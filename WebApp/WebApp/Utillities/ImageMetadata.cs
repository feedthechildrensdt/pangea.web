﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using MetadataExtractor;
using FTCWebApp.DataModels;
using System.Globalization;
using log4net;

namespace FTCWebApp.Utillities
{
    public class ImageMetadata
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ImageMetadata));

        //Summary
        //This method extracts the image metadata from the image file.
        public FileMetaDataModel ExtractImageMetadata(HttpPostedFileBase file)
        {
            try
            {
                FileMetaDataModel filemetadata = new FileMetaDataModel();
                Dictionary<string, object> dict = new Dictionary<string, object>();
                var filedata = file.InputStream;
                var directories = ImageMetadataReader.ReadMetadata(filedata);

                var EXIFdirectory = directories.FirstOrDefault(m => m.Name == "Exif IFD0");
                var EXIFSubdirectory = directories.FirstOrDefault(m => m.Name == "Exif SubIFD");
                var GPSdirectory = directories.FirstOrDefault(m => m.Name == "GPS");
                if (EXIFdirectory != null)
                {
                    filemetadata.Make = EXIFdirectory.Tags.FirstOrDefault(m => m.Name == "Make") != null ? EXIFdirectory.Tags.FirstOrDefault(m => m.Name == "Make").Description.ToString() : null;
                    filemetadata.Model = EXIFdirectory.Tags.FirstOrDefault(m => m.Name == "Model") != null ? EXIFdirectory.Tags.FirstOrDefault(m => m.Name == "Model").Description.ToString() : null;
                    filemetadata.Software = EXIFdirectory.Tags.FirstOrDefault(m => m.Name == "Software") != null ? EXIFdirectory.Tags.FirstOrDefault(m => m.Name == "Software").Description.ToString() : null;
                }
                if (EXIFSubdirectory != null)
                {
                    filemetadata.DateTaken = EXIFSubdirectory.Tags.FirstOrDefault(m => m.Name == "Date/Time Original") != null ? (DateTime.ParseExact(EXIFSubdirectory.Tags.FirstOrDefault(m => m.Name == "Date/Time Original").Description, "yyyy:MM:dd HH:mm:ss", CultureInfo.InvariantCulture).ToUniversalTime()).ToString() : null;
                    filemetadata.Original = EXIFSubdirectory.Tags.FirstOrDefault(m => m.Name == "Date/Time Original") != null ? (DateTime.ParseExact(EXIFSubdirectory.Tags.FirstOrDefault(m => m.Name == "Date/Time Original").Description, "yyyy:MM:dd HH:mm:ss", CultureInfo.InvariantCulture).ToUniversalTime()).ToString() : null;
                    filemetadata.Digitized = EXIFSubdirectory.Tags.FirstOrDefault(m => m.Name == "Date/Time Digitized") != null ? (DateTime.ParseExact(EXIFSubdirectory.Tags.FirstOrDefault(m => m.Name == "Date/Time Digitized").Description, "yyyy:MM:dd HH:mm:ss", CultureInfo.InvariantCulture).ToUniversalTime()).ToString() : null;
                    filemetadata.GPSVersionID = EXIFSubdirectory.Tags.FirstOrDefault(m => m.Name == "Exif Version") != null ? EXIFSubdirectory.Tags.FirstOrDefault(m => m.Name == "Exif Version").Description.ToString() : null;
                }
                if (GPSdirectory != null)
                {
                    filemetadata.GPSAltitude = GPSdirectory.Tags.FirstOrDefault(m => m.Name == "GPS Altitude") != null ? GPSdirectory.Tags.FirstOrDefault(m => m.Name == "GPS Altitude").Description.ToString() : null;
                    filemetadata.GPSAltitudeRef = GPSdirectory.Tags.FirstOrDefault(m => m.Name == "GPS Altitude Ref") != null ? GPSdirectory.Tags.FirstOrDefault(m => m.Name == "GPS Altitude Ref").Description.ToString() : null;
                    filemetadata.GPSLatitude = GPSdirectory.Tags.FirstOrDefault(m => m.Name == "GPS Latitude") != null ? GPSdirectory.Tags.FirstOrDefault(m => m.Name == "GPS Latitude").Description.ToString() : null;
                    filemetadata.GPSLatitudeRef = GPSdirectory.Tags.FirstOrDefault(m => m.Name == "GPS Latitude Ref") != null ? GPSdirectory.Tags.FirstOrDefault(m => m.Name == "GPS Latitude Ref").Description.ToString() : null;
                    filemetadata.GPSLongitude = GPSdirectory.Tags.FirstOrDefault(m => m.Name == "GPS Longitude") != null ? GPSdirectory.Tags.FirstOrDefault(m => m.Name == "GPS Longitude").Description.ToString() : null;
                    filemetadata.GPSLongitudeRef = GPSdirectory.Tags.FirstOrDefault(m => m.Name == "GPS Longitude Ref") != null ? GPSdirectory.Tags.FirstOrDefault(m => m.Name == "GPS Longitude Ref").Description.ToString() : null;
                    filemetadata.GPSTimeStamp = GPSdirectory.Tags.FirstOrDefault(m => m.Name == "GPS Time-Stamp") != null ? GPSdirectory.Tags.FirstOrDefault(m => m.Name == "GPS Time-Stamp").Description.ToString() : null;
                    filemetadata.GPSDateStamp = GPSdirectory.Tags.FirstOrDefault(m => m.Name == "GPS Date-Stamp") != null ? GPSdirectory.Tags.FirstOrDefault(m => m.Name == "GPS Date-Stamp").Description.ToString() : null;
                    filemetadata.GPSImgDirectionRef = GPSdirectory.Tags.FirstOrDefault(m => m.Name == "GPS Img Direction Ref") != null ? GPSdirectory.Tags.FirstOrDefault(m => m.Name == "GPS Img Direction Ref").Description.ToString() : null;
                }

                return filemetadata;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to extract the metadata from the uploaded image !", ex);
                return null;
            }
        }
    }
}