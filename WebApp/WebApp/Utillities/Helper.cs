﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace FTCWebApp.Utillities
{
    //Summary
    //Helper class for miscellaneous string conversions
    public static class Helper
    {
        //Summary
        // Helper method for non-nullable string to Long integer conversion
        public static long ParseLong(this string value, int defaultLongValue = 0)
        {
            long parsedLong;
            if (long.TryParse(value, out parsedLong))
            {
                return parsedLong;
            }

            return defaultLongValue;
        }

        //Summary
        // Helper method for nullable string to Long integer conversion
        public static long? ParseNullableLong(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return null;

            return value.ParseLong();
        }

        //Summary
        // Helper method for non-nullable string to integer conversion
        public static int ParseInt(this string value, int defaultIntValue = -1)
        {
            int parsedInt;
            if (int.TryParse(value, out parsedInt))
            {
                return parsedInt;
            }

            return defaultIntValue;
        }

        //Summary
        // Helper method for nullable string to integer conversion
        public static int? ParseNullableInt(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return null;

            return value.ParseInt();
        }

        public static string MessageWithDateTimeAdded(string Message)
        {
            return String.Format("{0}: {1}", DateTime.Now, Message);
        }



        public static string Session_Key_Data_Entry_Default_Country = "Data_Entry_Default_Country";

        public static string Session_Key_Data_Entry_Default_Location_Code = "Data_Entry_Default_Location_Code";

        public static string Session_Key_Data_Entry_Default_QualityControl_Action_Type = "Data_Entry_Default_QualityControl_Action_Type";

        public static string Session_Key_Data_Entry_Default_Enrollment_Action_Type = "Data_Entry_Default_Enrollment_Action_Type";

        public static string Session_Key_Data_Entry_Default_Mark_Location_For_Update_Action_Type = "Data_Entry_Default_Mark_Location_For_Update_Action_Type";

        public static string Session_Key_Data_Entry_Default_Update_Action_Type = "Data_Entry_Default_Update_Action_Type";

        public static string Session_Key_Data_Entry_Default_Translation_Action_Type = "Data_Entry_Default_Translation_Action_Type";


        public static string Session_Key_Data_Entry_Default_ApplicationManager_Action_Type = "Data_Entry_Default_ApplicationManager_Action_Type";


    }
}