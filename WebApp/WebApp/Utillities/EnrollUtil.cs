﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using FTCWebApp.ServiceReference1;
using FTCWebApp.DataModels;
using System.Web;
using FTCWebApp.Caching;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using FTCWebApp.ViewModels;
using System.Drawing.Drawing2D;
using System.Web.Mvc;
using log4net;
using System.Web.Configuration;
using System.Security.Principal;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using Newtonsoft.Json;
using FTCWebApp.DAL;


using FTCWebApp.ViewModels.GUI_Pieces;



namespace FTCWebApp.Utillities
{
    public class EnrollUtil
    {
        private FieldServicesClient _svc;
        private static readonly ILog log = LogManager.GetLogger(typeof(EnrollUtil));

        public EnrollUtil()
        {
            _svc = new FieldServicesClient("BasicHttpsBinding_IFieldServices");
        }

        //Summary
        //This method gets the drop down options for the specified Update fields based on age dynamically set by the user or system.
        public List<SelectListItem>[] GetFieldData(int? Age)
        {
            try
            {
                List<SelectListItem>[] FieldDataArray = new List<SelectListItem>[4];
                FieldDataArray[0] = new List<SelectListItem>();
                FieldDataArray[1] = new List<SelectListItem>();
                FieldDataArray[2] = new List<SelectListItem>();
                FieldDataArray[3] = new List<SelectListItem>();
                var Grades_Cache = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Grade_Level"));
                var Chores_Cache = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Chore"));
                var FavActs_Cache = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Favorite_Activity"));
                var FavLearn_Cache = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Favorite_Learning"));
                foreach (var item in Grades_Cache)
                {
                    if (Age >= Convert.ToInt32(item.Item3) / 365)
                    {
                        FieldDataArray[0].Add(new SelectListItem { Text = item.Item2, Value = item.Item1 });
                    }
                }
                foreach (var item in Chores_Cache)
                {
                    if (Age >= Convert.ToInt32(item.Item3) / 365)
                    {
                        FieldDataArray[1].Add(new SelectListItem { Text = item.Item2, Value = item.Item1 });
                    }

                }
                foreach (var item in FavActs_Cache)
                {
                    if (Age >= Convert.ToInt32(item.Item3) / 365)
                    {
                        FieldDataArray[2].Add(new SelectListItem { Text = item.Item2, Value = item.Item1 });
                    }

                }
                foreach (var item in FavLearn_Cache)
                {
                    if (Age >= Convert.ToInt32(item.Item3) / 365)
                    {
                        FieldDataArray[3].Add(new SelectListItem { Text = item.Item2, Value = item.Item1 });
                    }

                }

                return FieldDataArray;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to set the Field data as per the child's age or the age value is null !", ex);
                return null;
            }
        }

        //Summary
        //Get the Location Names and Location Codes upon selection of Country by the user
        public List<SelectListItem>[] GetLocationsByCountry(int Country_ID)
        {
            try
            {

                List<SelectListItem>[] LocationListArray = new List<SelectListItem>[2];

                UserInfo uInfo = SessionManager.Get<UserInfo>("UserInfo");

                string user_id = uInfo.ID.ToString();

                var the_locations = GUI_Pieces_Helper.Get_Country_Location_Entry_Drop_Down_List_Items(user_id, Country_ID);
               
                LocationListArray[0] = the_locations;

                List<SelectListItem> x_Location_Codes = new List<SelectListItem>();

                the_locations
                    .ForEach
                    (
                    xx =>
                    {

                        SelectListItem x_temp = new SelectListItem();

                        x_temp.Text = xx.Value;

                        x_temp.Value = xx.Value;

                        x_Location_Codes.Add(x_temp);

                    }
                    );
                
                LocationListArray[1] = x_Location_Codes;  

                return LocationListArray;

            }
            catch (Exception ex)
            {
                log.Debug("Failed to get the locations by country !", ex);
                return null;
            }
        }

        //Summary
        //Calculate the age based on the date of birth selected by the user
        public int? CalculateAge(DateTime dateOfBirth)
        {
            try
            {
                int age = 0;
                age = DateTime.Now.Year - dateOfBirth.Year;
                if (DateTime.Now.DayOfYear < dateOfBirth.DayOfYear)
                    age = age - 1;

                return age;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to calculate the child's age !", ex);
                return null;
            }
        }

        ////Summary
        ////This method populates the child object which is to be sent to the server for storage
        public EnrollChild AddChildObject(Dictionary<string, string> childEnrollInfo)
        {
            try
            {
                EnrollChild child = new EnrollChild();
                child.ChildID = childEnrollInfo["ChildID"];
                child.FirstName = childEnrollInfo["FirstName"];
                child.LastName = childEnrollInfo["LastName"];
                child.MiddleName = childEnrollInfo["MiddleName"];
                child.OtherNameGoesBy = childEnrollInfo["OtherNameGoesBy"];
                if (childEnrollInfo["DateOfBirth"].Length == 4)
                {
                    child.DateOfBirth = new DateTime(Convert.ToInt32(childEnrollInfo["DateOfBirth"]), 1, 1).ToUniversalTime();
                }
                else
                {
                    child.DateOfBirth = Convert.ToDateTime(childEnrollInfo["DateOfBirth"], CultureInfo.InvariantCulture).ToUniversalTime();
                }

                child.GenderCodeID = childEnrollInfo["GenderCodeID"] == "" ? -1 : int.Parse(childEnrollInfo["GenderCodeID"]);

                child.LocationCodeID = int.Parse(childEnrollInfo["LocationCodeID"]);

                if (childEnrollInfo["DisabilityStatusCodeID"] == "0")
                {
                    child.DisabilityStatus = false;
                }
                else if (childEnrollInfo["DisabilityStatusCodeID"] == "1")
                {
                    child.DisabilityStatus = true;
                }

                child.GradeLevelCodeID = childEnrollInfo["GradeLevelCodeID"] == "" ? -1 : int.Parse(childEnrollInfo["GradeLevelCodeID"]);

                var chorearray = (childEnrollInfo.Where(m => m.Key == "ChoreCodeID").ToList());
                string[] choretokens = null;
                if (!String.IsNullOrEmpty(chorearray[0].Value))
                {
                    choretokens = chorearray[0].Value.Split(',').ToArray();
                }

                child.ChoreIDs = choretokens != null ? Array.ConvertAll<string, int>(choretokens, int.Parse) : null;

                child.NumberOfBrothers = String.IsNullOrWhiteSpace(childEnrollInfo["NumberOfBrothers"]) ? 0 : int.Parse(childEnrollInfo["NumberOfBrothers"]);
                child.NumberOfSisters = String.IsNullOrWhiteSpace(childEnrollInfo["NumberOfSisters"]) ? 0 : int.Parse(childEnrollInfo["NumberOfSisters"]);

                child.PersonalityTypeID = childEnrollInfo["PersonalityTypeCodeID"] == "" ? -1 : int.Parse(childEnrollInfo["PersonalityTypeCodeID"]);

                var favActarray = (childEnrollInfo.Where(m => m.Key == "FavoriteActivityCodeID").ToList());
                string[] favActtokens = null;
                if (!String.IsNullOrEmpty(favActarray[0].Value))
                {
                    favActtokens = favActarray[0].Value.Split(',').ToArray();
                }
                child.FavoriteActivitieIDs = favActtokens != null ? Array.ConvertAll<string, int>(favActtokens, int.Parse) : null;

                child.FavoriteLearningCodeID = childEnrollInfo["FavoriteLearningCodeID"] == "" ? -1 : int.Parse(childEnrollInfo["FavoriteLearningCodeID"]);

                if (!String.IsNullOrWhiteSpace(childEnrollInfo["LivesWithCodeID"]))
                {
                    child.LivesWithCodeID = childEnrollInfo["LivesWithCodeID"] == "" ? -1 : int.Parse(childEnrollInfo["LivesWithCodeID"]);
                }

                if (childEnrollInfo.Keys.Contains("NonDupchildArray"))
                {
                    var NonDupArray = (childEnrollInfo.Where(m => m.Key == "NonDupchildArray").ToList());
                    child.ConfirmedNonDups = NonDupArray[0].Value.Split(',').ToArray();
                }

                return child;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to create the child object from the child enrollment form input !", ex);
                return null;
            }
        }

        ////Summary
        ////This method populates the child object which is to be sent to the server for storage
        public AdminChild AdminChildObject(Dictionary<string, string> childEnrollInfo)
        {
            try
            {
                AdminChild child = new AdminChild();
                child.ChildID = childEnrollInfo["ChildID"];
                child.ChildNum = childEnrollInfo["ChildNumber"];
                child.FirstName = childEnrollInfo["FirstName"];
                child.LastName = childEnrollInfo["LastName"];
                child.MiddleName = childEnrollInfo["MiddleName"];
                child.OtherNameGoesBy = childEnrollInfo["OtherNameGoesBy"];
                if (childEnrollInfo["DateOfBirth"].Length == 4)
                {
                    child.DateOfBirth = new DateTime(Convert.ToInt32(childEnrollInfo["DateOfBirth"]), 1, 1).ToUniversalTime();
                }
                else
                {
                    child.DateOfBirth = Convert.ToDateTime(childEnrollInfo["DateOfBirth"], CultureInfo.InvariantCulture).ToUniversalTime();
                }

                child.GenderCodeID = childEnrollInfo["GenderCodeID"] == "" ? -1 : int.Parse(childEnrollInfo["GenderCodeID"]);

                child.LocationCodeID = int.Parse(childEnrollInfo["LocationCodeID"]);

                if (childEnrollInfo["DisabilityStatusCodeID"] == "0")
                {
                    child.DisabilityStatus = false;
                }
                else if (childEnrollInfo["DisabilityStatusCodeID"] == "1")
                {
                    child.DisabilityStatus = true;
                }

                child.GradeLevelCodeID = childEnrollInfo["GradeLevelCodeID"] == "" ? -1 : int.Parse(childEnrollInfo["GradeLevelCodeID"]);

                var chorearray = (childEnrollInfo.Where(m => m.Key == "ChoreCodeID").ToList());
                string[] choretokens = null;
                if (!String.IsNullOrEmpty(chorearray[0].Value))
                {
                    choretokens = chorearray[0].Value.Split(',').ToArray();
                }

                child.ChoreIDs = choretokens != null ? Array.ConvertAll<string, int>(choretokens, int.Parse) : null;

                child.NumberOfBrothers = String.IsNullOrWhiteSpace(childEnrollInfo["NumberOfBrothers"]) ? 0 : int.Parse(childEnrollInfo["NumberOfBrothers"]);
                child.NumberOfSisters = String.IsNullOrWhiteSpace(childEnrollInfo["NumberOfSisters"]) ? 0 : int.Parse(childEnrollInfo["NumberOfSisters"]);

                //child.NumberOfSisters = Convert.ToInt32((childEnrollInfo.Where(m => m.Key == "NumberOfSisters").SingleOrDefault().Value == "" ? "0" : childEnrollInfo.Where(m => m.Key == "NumberOfSisters").SingleOrDefault().Value).ToString());

                child.PersonalityTypeID = childEnrollInfo["PersonalityTypeCodeID"] == "" ? -1 : int.Parse(childEnrollInfo["PersonalityTypeCodeID"]);

                //child.PersonalityTypeID = Convert.ToInt32((childEnrollInfo.Where(m => m.Key == "PersonalityTypeCodeID").SingleOrDefault().Value == "" ? "-1" : childEnrollInfo.Where(m => m.Key == "PersonalityTypeCodeID").SingleOrDefault().Value).ToString());

                var favActarray = (childEnrollInfo.Where(m => m.Key == "FavoriteActivityCodeID").ToList());
                string[] favActtokens = null;
                if (!String.IsNullOrEmpty(favActarray[0].Value))
                {
                    favActtokens = favActarray[0].Value.Split(',').ToArray();
                }
                child.FavoriteActivitieIDs = favActtokens != null ? Array.ConvertAll<string, int>(favActtokens, int.Parse) : null;

                child.FavoriteLearningCodeID = childEnrollInfo["FavoriteLearningCodeID"] == "" ? -1 : int.Parse(childEnrollInfo["FavoriteLearningCodeID"]);

                if (!String.IsNullOrWhiteSpace(childEnrollInfo["LivesWithCodeID"]))
                {
                    child.LivesWithCodeID = childEnrollInfo["LivesWithCodeID"] == "" ? -1 : int.Parse(childEnrollInfo["LivesWithCodeID"]);
                }

                if (childEnrollInfo.Keys.Contains("NonDupchildArray"))
                {
                    var NonDupArray = (childEnrollInfo.Where(m => m.Key == "NonDupchildArray").ToList());
                    child.ConfirmedNonDups = NonDupArray[0].Value.Split(',').ToArray();
                }

                return child;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to create the child object from the child enrollment form input !", ex);
                return null;
            }
        }

        //Summary
        //This method populates the update child object which is to be sent to the server for update
        public UpdateChild AddUpdateChildObject(Dictionary<string, string> childUpdateInfo, string currentProcess = "Update")
        {
            try
            {
                UpdateChild child = new UpdateChild();
                child.LocationCodeID = Convert.ToInt32(childUpdateInfo.SingleOrDefault(m => m.Key == "LocationCodeID").Value);
                child.ChildID = childUpdateInfo.SingleOrDefault(m => m.Key == "ChildID").Value.ToString();
                if (childUpdateInfo.SingleOrDefault(m => m.Key == "DisabilityStatusCodeID").Value.ToString() == "0")
                    child.DisabilityStatus = false;
                else if (childUpdateInfo.SingleOrDefault(m => m.Key == "DisabilityStatusCodeID").Value.ToString() == "1")
                    child.DisabilityStatus = true;
                child.GradeLevelCodeID = Convert.ToInt32((childUpdateInfo.SingleOrDefault(m => m.Key == "GradeLevelCodeID").Value == "" ? "-1" : childUpdateInfo.SingleOrDefault(m => m.Key == "GradeLevelCodeID").Value).ToString());
                var chorearray = (childUpdateInfo.Where(m => m.Key == "ChoreCodeID").ToList());
                string[] choretokens = null;
                if (!String.IsNullOrEmpty(chorearray[0].Value))
                { choretokens = chorearray[0].Value.Split(',').ToArray(); }
                if (choretokens != null)
                {
                    child.ChoreIDs = Array.ConvertAll<string, int>(choretokens, int.Parse);
                }
                else { child.ChoreIDs = null; }

                //child.ChoreIDs = Array.ConvertAll(charArray, s => int.Parse(s));
                child.NumberOfBrothers = Convert.ToInt32((childUpdateInfo.SingleOrDefault(m => m.Key == "NumberOfBrothers").Value == "" ? "0" : childUpdateInfo.SingleOrDefault(m => m.Key == "NumberOfBrothers").Value).ToString());
                child.NumberOfSisters = Convert.ToInt32((childUpdateInfo.SingleOrDefault(m => m.Key == "NumberOfSisters").Value == "" ? "0" : childUpdateInfo.SingleOrDefault(m => m.Key == "NumberOfSisters").Value).ToString());
                child.PersonalityTypeID = Convert.ToInt32((childUpdateInfo.SingleOrDefault(m => m.Key == "PersonalityTypeCodeID").Value == "" ? "-1" : childUpdateInfo.SingleOrDefault(m => m.Key == "PersonalityTypeCodeID").Value).ToString());
                var favActarray = (childUpdateInfo.Where(m => m.Key == "FavoriteActivityCodeID").ToList());
                string[] favActtokens = null;
                if (!String.IsNullOrEmpty(favActarray[0].Value))
                { favActtokens = favActarray[0].Value.Split(',').ToArray(); }
                if (favActtokens != null)
                {
                    child.FavoriteActivitieIDs = Array.ConvertAll<string, int>(favActtokens, int.Parse);
                }
                else { child.FavoriteActivitieIDs = null; }

                child.FavoriteLearningCodeID = Convert.ToInt32((childUpdateInfo.SingleOrDefault(m => m.Key == "FavoriteLearningCodeID").Value == "" ? "-1" : childUpdateInfo.SingleOrDefault(m => m.Key == "FavoriteLearningCodeID").Value).ToString());
                child.LivesWithCodeID = Convert.ToInt32((childUpdateInfo.SingleOrDefault(m => m.Key == "LivesWithCodeID").Value == "" ? "-1" : childUpdateInfo.SingleOrDefault(m => m.Key == "LivesWithCodeID").Value).ToString());
                //This method is also called when saving a declined enrollemnt record with declined enrollment information, in that case do not pass in MLE 
                if (currentProcess == "Update")
                {
                    child.MajorLifeEvent = childUpdateInfo.SingleOrDefault(m => m.Key == "MajorLifeEvent").Value.ToString();
                }

                return child;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to create the child object from the child update form input !", ex);
                return null;
            }
        }

        public AdminChild AddDeclinedChildInformationObject(Dictionary<string, string> childDeclinedEnrollInfo)
        {
            try
            {
                AdminChild child = new AdminChild();
                child.ChildID = childDeclinedEnrollInfo["ChildID"];
                child.ChildNum = childDeclinedEnrollInfo["ChildNumber"];
                child.FirstName = childDeclinedEnrollInfo["FirstName"];
                child.LastName = childDeclinedEnrollInfo["LastName"];
                child.MiddleName = childDeclinedEnrollInfo["MiddleName"];
                child.OtherNameGoesBy = childDeclinedEnrollInfo["OtherNameGoesBy"];
                if (childDeclinedEnrollInfo["DateOfBirth"].Length == 4)
                {
                    child.DateOfBirth = new DateTime(Convert.ToInt32(childDeclinedEnrollInfo["DateOfBirth"]), 1, 1).ToUniversalTime();
                }
                else
                {
                    child.DateOfBirth = Convert.ToDateTime(childDeclinedEnrollInfo["DateOfBirth"], CultureInfo.InvariantCulture).ToUniversalTime();
                }

                child.GenderCodeID = childDeclinedEnrollInfo["GenderCodeID"] == "" ? -1 : int.Parse(childDeclinedEnrollInfo["GenderCodeID"]);

                child.LocationCodeID = int.Parse(childDeclinedEnrollInfo["LocationCodeID"]);

                if (childDeclinedEnrollInfo.Keys.Contains("NonDupchildArray"))
                {
                    var NonDupArray = (childDeclinedEnrollInfo.Where(m => m.Key == "NonDupchildArray").ToList());
                    child.ConfirmedNonDups = NonDupArray[0].Value.Split(',').ToArray();
                }

                return child;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to create the child object from the child enrollment form input !", ex);
                return null;
            }
        }

        public AdminChild AddDeclinedEnrollmentInformationObject(Dictionary<string, string> childDeclinedEnrollInfo)
        {
            try
            {
                AdminChild child = new AdminChild();
                //child.LocationCodeID = Convert.ToInt32(childDeclinedEnrollInfo.SingleOrDefault(m => m.Key == "LocationCodeID").Value);
                child.ChildID = childDeclinedEnrollInfo["ChildID"];
                child.ChildNum = childDeclinedEnrollInfo["ChildNumber"];
                if (childDeclinedEnrollInfo.SingleOrDefault(m => m.Key == "DisabilityStatusCodeID").Value.ToString() == "0")
                    child.DisabilityStatus = false;
                else if (childDeclinedEnrollInfo.SingleOrDefault(m => m.Key == "DisabilityStatusCodeID").Value.ToString() == "1")
                    child.DisabilityStatus = true;
                child.GradeLevelCodeID = Convert.ToInt32((childDeclinedEnrollInfo.SingleOrDefault(m => m.Key == "GradeLevelCodeID").Value == "" ? "-1" : childDeclinedEnrollInfo.SingleOrDefault(m => m.Key == "GradeLevelCodeID").Value).ToString());
                var chorearray = (childDeclinedEnrollInfo.Where(m => m.Key == "ChoreCodeID").ToList());
                string[] choretokens = null;
                if (!String.IsNullOrEmpty(chorearray[0].Value))
                { choretokens = chorearray[0].Value.Split(',').ToArray(); }
                if (choretokens != null)
                {
                    child.ChoreIDs = Array.ConvertAll<string, int>(choretokens, int.Parse);
                }
                else { child.ChoreIDs = null; }

                //child.ChoreIDs = Array.ConvertAll(charArray, s => int.Parse(s));
                child.NumberOfBrothers = Convert.ToInt32((childDeclinedEnrollInfo.SingleOrDefault(m => m.Key == "NumberOfBrothers").Value == "" ? "0" : childDeclinedEnrollInfo.SingleOrDefault(m => m.Key == "NumberOfBrothers").Value).ToString());
                child.NumberOfSisters = Convert.ToInt32((childDeclinedEnrollInfo.SingleOrDefault(m => m.Key == "NumberOfSisters").Value == "" ? "0" : childDeclinedEnrollInfo.SingleOrDefault(m => m.Key == "NumberOfSisters").Value).ToString());
                child.PersonalityTypeID = Convert.ToInt32((childDeclinedEnrollInfo.SingleOrDefault(m => m.Key == "PersonalityTypeCodeID").Value == "" ? "-1" : childDeclinedEnrollInfo.SingleOrDefault(m => m.Key == "PersonalityTypeCodeID").Value).ToString());
                var favActarray = (childDeclinedEnrollInfo.Where(m => m.Key == "FavoriteActivityCodeID").ToList());
                string[] favActtokens = null;
                if (!String.IsNullOrEmpty(favActarray[0].Value))
                { favActtokens = favActarray[0].Value.Split(',').ToArray(); }
                if (favActtokens != null)
                {
                    child.FavoriteActivitieIDs = Array.ConvertAll<string, int>(favActtokens, int.Parse);
                }
                else { child.FavoriteActivitieIDs = null; }

                child.FavoriteLearningCodeID = Convert.ToInt32((childDeclinedEnrollInfo.SingleOrDefault(m => m.Key == "FavoriteLearningCodeID").Value == "" ? "-1" : childDeclinedEnrollInfo.SingleOrDefault(m => m.Key == "FavoriteLearningCodeID").Value).ToString());
                child.LivesWithCodeID = Convert.ToInt32((childDeclinedEnrollInfo.SingleOrDefault(m => m.Key == "LivesWithCodeID").Value == "" ? "-1" : childDeclinedEnrollInfo.SingleOrDefault(m => m.Key == "LivesWithCodeID").Value).ToString());
                return child;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to create the child object from the child enrollment form input !", ex);
                return null;
            }
        }

        //Summary
        //This method adds the metadata as well as additional data which needs to be sent to the API server for file upload
        public UploadFile AddFileData(HttpPostedFileBase file, string ChildID, string ContentTypeID, string ImgCropArea_X1, string ImgCropArea_Y1, string ImgCropArea_Width, string ImgCropArea_Height)
        {
            try
            {
                var ContentType = (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).ToList();

                var contentName = String.Empty;

                if (!ContentTypeID.Contains("Translation"))
                {
                    contentName = ContentType.FirstOrDefault(a => a.Content_Type_Code_ID == int.Parse(ContentTypeID)).Content_Type;
                }

                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                var uploadFile = new UploadFile();

                if (file != null && file.ContentLength > 0 && !string.IsNullOrEmpty(ChildID) && !string.IsNullOrEmpty(ContentTypeID))
                {
                    byte[] filebytes = null;
                    if (Path.GetExtension(file.FileName).ToLower() == ".jpg")
                    {
                        var Metaobj = new ImageMetadata();
                        var fileMeta = new FileMetaData();
                        var MetaData = new FileMetaDataModel();
                        MetaData = Metaobj.ExtractImageMetadata(file);
                        var filestream = file.InputStream;
                        var image = Image.FromStream(filestream);
                        if (contentName.Contains("Profile") || contentName.Contains("Action"))
                        {
                            //Do not allow the system to accept images with resolution less than 72 dpi
                            if (image.VerticalResolution < 72 || image.HorizontalResolution < 72)
                            {
                                return null;
                            }
                        }
                        Image _Image = null;
                        if (contentName.Contains("Profile") || contentName.Contains("Action"))
                        {
                            if ((image.Height >= 1600) && (image.Width >= 1200))
                            {
                                if (!string.IsNullOrEmpty(ImgCropArea_Height) && !string.IsNullOrEmpty(ImgCropArea_Width))
                                {
                                    var Cord_X = ImgCropArea_X1.Split('.');
                                    var Cord_Y = ImgCropArea_Y1.Split('.');
                                    var Crop_Width = ImgCropArea_Width.Split('.');
                                    var Crop_Height = ImgCropArea_Height.Split('.');

                                    if ((Convert.ToInt32(Crop_Width[0]) >= 1200) && (Convert.ToInt32(Crop_Height[0]) >= 1600))
                                    {
                                        _Image = FormatImage(image, Convert.ToInt32(Cord_X[0]), Convert.ToInt32(Cord_Y[0]), Convert.ToInt32(Crop_Width[0]), Convert.ToInt32(Crop_Height[0]));
                                    }
                                    else
                                    {
                                        return null;
                                    }
                                }
                                else
                                {
                                    _Image = FormatImage(image, 0, 0, 0, 0);
                                }
                                uploadFile.FileTypeID = (CacheManager.GetFromCache<List<FileTypeModel>>("File_Type")).FirstOrDefault(m => m.File_Extension == ".jpg").File_Type_Code_ID;
                                fileMeta.DateTaken = MetaData.DateTaken;
                                fileMeta.Original = MetaData.Original;
                                fileMeta.Digitized = MetaData.Digitized;
                                fileMeta.Make = MetaData.Make;
                                fileMeta.Model = MetaData.Model;
                                fileMeta.Software = MetaData.Software;
                                fileMeta.GPSAltitude = MetaData.GPSAltitude;
                                fileMeta.GPSAltitudeRef = MetaData.GPSAltitudeRef;
                                fileMeta.GPSImgDirectionRef = MetaData.GPSImgDirectionRef;
                                fileMeta.GPSLatitude = MetaData.GPSLatitude;
                                fileMeta.GPSLatitudeRef = MetaData.GPSLatitudeRef;
                                fileMeta.GPSLongitude = MetaData.GPSLongitude;
                                fileMeta.GPSLongitudeRef = MetaData.GPSLongitudeRef;
                                fileMeta.GPSTimeStamp = MetaData.GPSTimeStamp;
                                fileMeta.GPSVersionID = MetaData.GPSVersionID;
                                fileMeta.GPSDateStamp = MetaData.GPSDateStamp;
                                uploadFile.MetaData = fileMeta;
                                var tmp_image = new Bitmap(_Image);
                                filebytes = (byte[])(new ImageConverter()).ConvertTo(tmp_image.Clone(), typeof(byte[]));
                            }
                            else
                            {
                                return null;
                            }
                        }

                        else
                        {
                            _Image = FormatImage(image, 0, 0, 0, 0);
                            uploadFile.FileTypeID = (CacheManager.GetFromCache<List<FileTypeModel>>("File_Type")).FirstOrDefault(m => m.File_Extension == ".jpg").File_Type_Code_ID;
                            fileMeta.DateTaken = MetaData.DateTaken;
                            fileMeta.Original = MetaData.Original;
                            fileMeta.Digitized = MetaData.Digitized;
                            fileMeta.Make = MetaData.Make;
                            fileMeta.Model = MetaData.Model;
                            fileMeta.Software = MetaData.Software;
                            fileMeta.GPSAltitude = MetaData.GPSAltitude;
                            fileMeta.GPSAltitudeRef = MetaData.GPSAltitudeRef;
                            fileMeta.GPSImgDirectionRef = MetaData.GPSImgDirectionRef;
                            fileMeta.GPSLatitude = MetaData.GPSLatitude;
                            fileMeta.GPSLatitudeRef = MetaData.GPSLatitudeRef;
                            fileMeta.GPSLongitude = MetaData.GPSLongitude;
                            fileMeta.GPSLongitudeRef = MetaData.GPSLongitudeRef;
                            fileMeta.GPSTimeStamp = MetaData.GPSTimeStamp;
                            fileMeta.GPSVersionID = MetaData.GPSVersionID;
                            fileMeta.GPSDateStamp = MetaData.GPSDateStamp;
                            uploadFile.MetaData = fileMeta;
                            var tmp_image = new Bitmap(_Image);
                            filebytes = (byte[])(new ImageConverter()).ConvertTo(tmp_image.Clone(), typeof(byte[]));
                        }
                    }
                    else if (Path.GetExtension(file.FileName).ToLower() == ".pdf")
                    {
                        uploadFile.FileTypeID = (CacheManager.GetFromCache<List<FileTypeModel>>("File_Type")).FirstOrDefault(m => m.File_Extension == ".pdf").File_Type_Code_ID;
                        using (MemoryStream memorystream = new MemoryStream())
                        {
                            file.InputStream.CopyTo(memorystream);
                            filebytes = memorystream.ToArray();
                        }
                    }
                    else if (Path.GetExtension(file.FileName).ToLower() == ".doc" || Path.GetExtension(file.FileName).ToLower() == ".docx")
                    {
                        //In case of letter translation 
                        //uploadFile.FileTypeID = (CacheManager.GetFromCache<List<FileTypeModel>>("File_Type")).FirstOrDefault(m => m.File_Extension == ".doc" || m.File_Extension == ".docx").File_Type_Code_ID;
                        using (MemoryStream memorystream = new MemoryStream())
                        {
                            file.InputStream.CopyTo(memorystream);
                            filebytes = memorystream.ToArray();
                        }
                    }
                    uploadFile.FileName = file.FileName;
                    uploadFile.FileBytes = filebytes;
                    if (!ContentTypeID.Contains("Translation"))
                    {
                        uploadFile.ContentTypeID = Convert.ToInt32(ContentTypeID);
                    }
                    else
                    {
                        //TODO add uplaod fiel content type id for translation of letter
                        // will be the contnet type id for the translated file 
                        uploadFile.ContentTypeID = 0;
                    }
                    uploadFile.ChildID = ChildID;
                    uploadFile.UserID = uInfo.ToString();
                    return uploadFile;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                log.Debug("Failed to create a file object from the uploaded file for saving it to the server :" + file.FileName, ex);
                return null;
            }
        }

        //Summary
        //This method crops an image as per the coordinates provided by the user and resizes the image to the standard Pangea size of 1200 X 1600
        public Image FormatImage(Image image, int ImgCropArea_X1, int ImgCropArea_Y1, int ImgCropArea_Width, int ImgCropArea_Height)
        {
            try
            {
                Bitmap target = new Bitmap(1200, 1600, PixelFormat.Format24bppRgb);
                target.SetResolution(image.HorizontalResolution, image.VerticalResolution);
                Image formatted_Image = null;
                using (Graphics g = Graphics.FromImage(target))
                {
                    g.SmoothingMode = SmoothingMode.AntiAlias;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;

                    if (ImgCropArea_Width != 0 && ImgCropArea_Height != 0)
                    {
                        g.DrawImage(image, new Rectangle(0, 0, target.Width, target.Height), ImgCropArea_X1, ImgCropArea_Y1, ImgCropArea_Width, ImgCropArea_Height, GraphicsUnit.Pixel);
                    }
                    else
                    {
                        g.DrawImage(image, 0, 0, 1200, 1600);
                    }

                }
                using (MemoryStream mm = new MemoryStream())
                {
                    target.Save(mm, ImageFormat.Jpeg);
                    formatted_Image = Image.FromStream(mm);
                }
                return formatted_Image;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to format the uploaded image as per the system requirements !", ex);
                return null;
            }
        }

        //Summary
        //This method creates a thumbnail from an image input
        public string createThumbnail(Image image)
        {
            try
            {
                int newHeight = 100;
                int newWidth = (int)((newHeight * image.Width) / image.Height);
                Image thumbnail = image.GetThumbnailImage(newWidth, newHeight, null, IntPtr.Zero);
                MemoryStream imageStream = new MemoryStream();
                thumbnail.Save(imageStream, ImageFormat.Png);
                byte[] imgbytes = imageStream.ToArray();
                var bytestring = Convert.ToBase64String(imgbytes);
                return bytestring;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to create the thumbnail from the uploaded image !", ex);
                return null;
            }
        }

        public object processReplaceFile(HttpPostedFileBase file, string ChildID, string ContentTypeID, UploadFile fileUpload_ready)
        {
            try
            {
                if (fileUpload_ready != null)
                {
                    string replaceSessionName = "ContentType_" + ContentTypeID;
                    if (SessionManager.Get<UploadFile>(replaceSessionName) != null)
                        SessionManager.Remove<UploadFile>(replaceSessionName);

                    SessionManager.Set(replaceSessionName, fileUpload_ready);
                }
                return null;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to replace the uploaded file to the Session !", ex);
                return null;
            }
        }

        //Summary
        //This method stores the uploaded file in the session and returns file thumbnail object
        public object processUploadFile(HttpPostedFileBase file, string ChildID, string ContentTypeID, UploadFile fileUpload_ready)
        {
            try
            {
                if (fileUpload_ready != null && Path.GetExtension(file.FileName.ToLower()) == ".jpg")
                {
                    SessionManager.Set("ContentType_" + ContentTypeID, fileUpload_ready);
                    var key = "ContentType_" + ContentTypeID;
                    Image image = null;
                    using (MemoryStream mStream = new MemoryStream(fileUpload_ready.FileBytes))
                    {
                        image = Image.FromStream(mStream);
                    }
                    var bytestring = createThumbnail(image);
                    var data = new { Success = true, Image = bytestring, Id = ContentTypeID, Key = key, Name = file.FileName, Message = Helper.MessageWithDateTimeAdded("The file has been uploaded !") };
                    return data;
                }
                else if (fileUpload_ready != null && Path.GetExtension(file.FileName.ToLower()) == ".pdf")
                {
                    SessionManager.Set("ContentType_" + ContentTypeID, fileUpload_ready);
                    var key = "ContentType_" + ContentTypeID;
                    var FilePath = HttpContext.Current.Server.MapPath("~/Content/pdf-thumbnail.jpg");
                    Image image = Image.FromFile(FilePath);
                    var bytestring = createThumbnail(image);
                    var data = new { Success = true, Image = bytestring, Id = ContentTypeID, Key = key, Name = file.FileName, Message = Helper.MessageWithDateTimeAdded("The file has been uploaded !") };
                    return data;
                }
                else if (fileUpload_ready != null && Path.GetExtension(file.FileName.ToLower()) == ".doc" || fileUpload_ready != null && Path.GetExtension(file.FileName.ToLower()) == ".docx")
                {
                    //if the contnet Name has translation then add this to the session, replace later with the content type id for letter translation
                    SessionManager.Set("ContentType_TranslatedLetter", fileUpload_ready);
                    // TODO replace by session key once we have a content type of translation letter
                    var key = "ContentType_TranslatedLetter";
                    var FilePath = HttpContext.Current.Server.MapPath("~/Content/doc.jpg");
                    Image image = Image.FromFile(FilePath);
                    var bytestring = createThumbnail(image);
                    var data = new { Success = true, Image = bytestring, Id = ContentTypeID, Key = key, Name = file.FileName, Message = Helper.MessageWithDateTimeAdded("The file has been uploaded !") };
                    return data;
                }
                else if (file.ContentLength == 0 && fileUpload_ready == null)
                {
                    var data = new { Success = false, Message = Helper.MessageWithDateTimeAdded("The uploaded file has no content !") };
                    return data;
                }
                else if (file.ContentLength != 0 && fileUpload_ready == null && Path.GetExtension(file.FileName.ToLower()).ToLower() == ".jpg")
                {
                    var data = new { Success = false, Message = Helper.MessageWithDateTimeAdded("The uploaded image size is either lesser than the standard 1200 X 1600 image size allowed or has resolution lesser than 72 dpi !") };
                    return data;
                }
                else
                {
                    var data = new { Success = false, Message = Helper.MessageWithDateTimeAdded("The uploaded file is currently not supported !") };
                    return data;
                }
            }
            catch (Exception ex)
            {
                log.Debug("Failed to store the uploaded file to the Session !", ex);
                return null;
            }
        }

        //Summary
        // This method setups the view model for the child update page
        public CommonVM setupEnrollmentView(string id)
        {
            try
            {
                var EnrollModel = new CommonVM();
                EnrollModel.currentUser = SessionManager.Get<UserInfo>("UserInfo");
                EnrollModel.Declines = new List<DeclineDetails>();

                // populate the dropdowns will need to be revisited to move to code to a helper method called by several modules
                PopulateDatalists(EnrollModel);
                //2 child objects are returned from the API object at index 0 is from pending and object at 1 is from dbo. for enrollment always populate data from pending so index 0
                var IncompleteChildUpdates = _svc.GetIncompleteChildUpdate(id, EnrollModel.currentUser.ToString());
                // Populate the fields in the child information tab from pending
                EnrollModel.child = new Child();
                EnrollModel.child.ChildID = IncompleteChildUpdates[0].ChildID;
                EnrollModel.child.ChildNumber = IncompleteChildUpdates[0].ChildNum;
                EnrollModel.child.LocationCodeID = IncompleteChildUpdates[0].LocationCodeID.ToString();
                EnrollModel.child.CountryId = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Location")).Where(c => c.Item1 == EnrollModel.child.LocationCodeID).Select(m => m.Item3).SingleOrDefault();
                EnrollModel.child.LocationName = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Location")).Where(c => c.Item1 == EnrollModel.child.LocationCodeID).Select(m => m.Item2).SingleOrDefault();
                EnrollModel.child.FirstName = IncompleteChildUpdates[0].FirstName;
                EnrollModel.child.MiddleName = IncompleteChildUpdates[0].MiddleName;
                EnrollModel.child.LastName = IncompleteChildUpdates[0].LastName;
                EnrollModel.child.OtherNameGoesBy = IncompleteChildUpdates[0].OtherNameGoesBy;
                EnrollModel.child.DateOfBirth = IncompleteChildUpdates[0].DateOfBirth.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                EnrollModel.child.GenderCodeID = IncompleteChildUpdates[0].GenderCodeID;
                // Populate the fields in the Additional Information tab from pending
                EnrollModel.child.DisabilityStatusCodeID = IncompleteChildUpdates[0].DisabilityStatus == true ? 1 : 0;

                EnrollModel.child.GradeLevelCodeID = IncompleteChildUpdates[0].GradeLevelCodeID;
                EnrollModel.child.Chores = IncompleteChildUpdates[0].ChoreIDs.Select(s => new SelectListItem { Text = s.ToString(), Value = s.ToString() });
                EnrollModel.child.NumberOfBrothers = IncompleteChildUpdates[0].NumberOfBrothers;
                EnrollModel.child.NumberOfSisters = IncompleteChildUpdates[0].NumberOfSisters;
                EnrollModel.child.PersonalityTypeCodeID = IncompleteChildUpdates[0].PersonalityTypeID;
                EnrollModel.child.FavActs = IncompleteChildUpdates[0].FavoriteActivitieIDs.Select(s => new SelectListItem { Text = s.ToString(), Value = s.ToString() });
                EnrollModel.child.FavoriteLearningCodeID = IncompleteChildUpdates[0].FavoriteLearningCodeID;
                EnrollModel.child.LivesWithCodeID = IncompleteChildUpdates[0].LivesWithCodeID;
                return EnrollModel;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to setup the view for the child enrollment !", ex);
                return null;
            }
        }

        //Summary
        // This method setups the view model for the child update page
        public CommonVM setupUpdateView(string id)
        {
            try
            {
                var updateModel = new CommonVM();
                updateModel.currentUser = SessionManager.Get<UserInfo>("UserInfo");
                updateModel.Declines = new List<DeclineDetails>();

                var IncompleteChildUpdates = new AdminChild[2];
                IncompleteChildUpdates[0] = new AdminChild();
                IncompleteChildUpdates[1] = new AdminChild();
                //From the service child at index 0 is from pending and index 1 is dbo
                var data = _svc.GetIncompleteChildUpdate(id, updateModel.currentUser.ToString());

                if (data[0].FirstName == null && data[1].FirstName != null)
                {
                    // data[0] contains pending child update info.
                    IncompleteChildUpdates[0] = data[0];
                    //data[1] contains child info. 
                    IncompleteChildUpdates[1] = data[1];
                }
                else if (data[0].FirstName != null && data[1].FirstName == null)
                {
                    //data[1] contains pending child update info.
                    IncompleteChildUpdates[0] = data[1];
                    // data[0] contains child info.
                    IncompleteChildUpdates[1] = data[0];
                }

                // Populate the pending child update object
                // Populate the fields in the child information tab
                updateModel.child = new Child();
                // PopulateDatalists(updateModel);
                updateModel.child.ChildID = IncompleteChildUpdates[0].ChildID;
                updateModel.child.ChildNumber = IncompleteChildUpdates[1].ChildNum;
                updateModel.child.LocationCodeID = IncompleteChildUpdates[1].LocationCodeID.ToString();
                updateModel.child.CountryId = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Location")).Where(c => c.Item1 == updateModel.child.LocationCodeID).Select(m => m.Item3).SingleOrDefault();
                updateModel.child.LocationName = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Location")).Where(c => c.Item1 == updateModel.child.LocationCodeID).Select(m => m.Item2).SingleOrDefault();
                updateModel.child.FirstName = IncompleteChildUpdates[1].FirstName;
                updateModel.child.MiddleName = IncompleteChildUpdates[1].MiddleName;
                updateModel.child.LastName = IncompleteChildUpdates[1].LastName;
                updateModel.child.OtherNameGoesBy = IncompleteChildUpdates[1].OtherNameGoesBy;
                updateModel.child.DateOfBirth = IncompleteChildUpdates[1].DateOfBirth.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                updateModel.child.GenderCodeID = IncompleteChildUpdates[1].GenderCodeID;
                // Populate the fields in the Additional Information tab
                updateModel.child.DisabilityStatusCodeID = IncompleteChildUpdates[0].DisabilityStatus == true ? 1 : 0;
                updateModel.child.GradeLevelCodeID = IncompleteChildUpdates[0].GradeLevelCodeID;
                updateModel.child.Chores = IncompleteChildUpdates[0].ChoreIDs.Select(s => new SelectListItem { Text = s.ToString(), Value = s.ToString() });
                updateModel.child.NumberOfBrothers = IncompleteChildUpdates[0].NumberOfBrothers;
                updateModel.child.NumberOfSisters = IncompleteChildUpdates[0].NumberOfSisters;
                updateModel.child.PersonalityTypeCodeID = IncompleteChildUpdates[0].PersonalityTypeID;
                updateModel.child.FavActs = IncompleteChildUpdates[0].FavoriteActivitieIDs.Select(s => new SelectListItem { Text = s.ToString(), Value = s.ToString() });
                updateModel.child.FavoriteLearningCodeID = IncompleteChildUpdates[0].FavoriteLearningCodeID;
                updateModel.child.LivesWithCodeID = IncompleteChildUpdates[0].LivesWithCodeID;
                updateModel.child.MajorLifeEvent = IncompleteChildUpdates[0].MajorLifeEvent;

                //Grades, Chores, Favorite Activities, Favorite Learning drop downs are populated based on child's age.
                if (!string.IsNullOrEmpty(updateModel.child.DateOfBirth))
                {
                    var Age = CalculateAge(Convert.ToDateTime(updateModel.child.DateOfBirth));
                    var FieldData = GetFieldData(Age);
                    updateModel.Grades = FieldData[0].AsEnumerable();
                    updateModel.ChoresatHome = FieldData[1].AsEnumerable();
                    updateModel.FavActivities = FieldData[2].AsEnumerable();
                    updateModel.FavLearning = FieldData[3].AsEnumerable();
                }

                // Populate the child history object 
                updateModel.ChildHistory = new CommonVM();
                updateModel.Declines = new List<DeclineDetails>();
                //updateModel.ChildHistory.User = SessionManager.Get<UserInfo>("UserInfo"); // TODO : Probably not needed.

                updateModel.ChildHistory.Disabalitites = IncompleteChildUpdates[1].DisabilityStatus == true ? updateModel.Disabalitites.Where(m => m.Value == "1") : updateModel.Disabalitites.Where(m => m.Value == "0");
                updateModel.ChildHistory.Grades = updateModel.Grades.Where(m => m.Value == IncompleteChildUpdates[1].GradeLevelCodeID.ToString());
                updateModel.ChildHistory.ChoresatHome = updateModel.ChoresatHome.Where(m => IncompleteChildUpdates[1].ChoreIDs.ToList().Contains(Convert.ToInt32(m.Value)));
                updateModel.ChildHistory.Brothers = IncompleteChildUpdates[1].NumberOfBrothers;
                updateModel.ChildHistory.Sisters = IncompleteChildUpdates[1].NumberOfSisters;
                updateModel.ChildHistory.Personalities = updateModel.Personalities.Where(m => m.Value == IncompleteChildUpdates[1].PersonalityTypeID.ToString());
                updateModel.ChildHistory.FavActivities = updateModel.FavActivities.Where(m => IncompleteChildUpdates[1].FavoriteActivitieIDs.ToList().Contains(Convert.ToInt32(m.Value)));
                updateModel.ChildHistory.FavLearning = updateModel.FavLearning.Where(m => m.Value == IncompleteChildUpdates[1].FavoriteLearningCodeID.ToString());
                updateModel.ChildHistory.LivesWith = updateModel.LivesWith.Where(m => m.Value == IncompleteChildUpdates[1].LivesWithCodeID.ToString());
                updateModel.ChildHistory.MajorLifeEvent = IncompleteChildUpdates[1].MajorLifeEvent;

                // Get already saved documents from the DB add it to the session, create thumbnails for display
                updateModel.AlreadyUploadedDocuments = _svc.GetUpdateFile(new DownLoadFile() { ChildID = id, UserID = updateModel.currentUser.ToString() }).ToList();

                if (updateModel.AlreadyUploadedDocuments != null && updateModel.AlreadyUploadedDocuments.Count() > 0)
                {
                    ProcessAlreadyUplaodedDocuments(updateModel.AlreadyUploadedDocuments);
                }

                //Get the child profile photo and store it in model
                ChildProfilePhoto photo = new ChildProfilePhoto();
                photo.ChildID = id;
                photo.UserID = updateModel.currentUser.ToString();
                var child_ProfilePhoto = _svc.GetChildProfilePhoto(photo);
                updateModel.child.ProfilePhoto = new FileModel();
                updateModel.child.ProfilePhoto.FileBytes = child_ProfilePhoto.FileBytes;
                updateModel.child.ProfilePhoto.FileName = child_ProfilePhoto.FileName;

                return updateModel;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to setup the view for the child update !", ex);
                return null;
            }
        }

        ///Summary
        /// This method setups the view model for the child update page
        ///Summary
        public CommonVM setupIncompleteEnrollmentView(string id)
        {
            try
            {
                var EnrollModel = new CommonVM();

                EnrollModel.currentUser = SessionManager.Get<UserInfo>("UserInfo");
                EnrollModel.Declines = new List<DeclineDetails>();

                //2 child objects are returned from the API object at index 0 is from pending and object at 1 is from dbo. for enrollment always populate data from pending so index 0
                var IncompleteChildEnrollment = _svc.GetIncompleteChildEnrollment(id, EnrollModel.currentUser.ToString());

                //var data = _svc.GetEnrollmentFile(new DownLoadFile());
                // Populate the fields in the child information tab from pending
                EnrollModel.child = new Child();
                EnrollModel.child.ChildID = IncompleteChildEnrollment.ChildID;
                EnrollModel.child.LocationCodeID = IncompleteChildEnrollment.LocationCodeID.ToString();
                EnrollModel.child.CountryId = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Location")).Where(c => c.Item1 == EnrollModel.child.LocationCodeID).Select(m => m.Item3).SingleOrDefault();
                EnrollModel.child.LocationName = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Location")).Where(c => c.Item1 == EnrollModel.child.LocationCodeID).Select(m => m.Item2).SingleOrDefault();
                EnrollModel.child.FirstName = IncompleteChildEnrollment.FirstName;
                EnrollModel.child.MiddleName = IncompleteChildEnrollment.MiddleName;
                EnrollModel.child.LastName = IncompleteChildEnrollment.LastName;
                EnrollModel.child.OtherNameGoesBy = IncompleteChildEnrollment.OtherNameGoesBy;
                EnrollModel.child.DateOfBirth = IncompleteChildEnrollment.DateOfBirth.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);

                EnrollModel.child.GenderCodeID = IncompleteChildEnrollment.GenderCodeID;

                //Populate the fields in the Additional Information
                EnrollModel.child.DisabilityStatusCodeID = IncompleteChildEnrollment.DisabilityStatus == true ? 1 : 0;
                EnrollModel.child.GradeLevelCodeID = IncompleteChildEnrollment.GradeLevelCodeID;
                EnrollModel.child.Chores = IncompleteChildEnrollment.ChoreIDs.Select(s => new SelectListItem { Text = s.ToString(), Value = s.ToString() });
                EnrollModel.child.NumberOfBrothers = IncompleteChildEnrollment.NumberOfBrothers;
                EnrollModel.child.NumberOfSisters = IncompleteChildEnrollment.NumberOfSisters;
                EnrollModel.child.PersonalityTypeCodeID = IncompleteChildEnrollment.PersonalityTypeID;
                EnrollModel.child.FavActs = IncompleteChildEnrollment.FavoriteActivitieIDs.Select(s => new SelectListItem { Text = s.ToString(), Value = s.ToString() });
                EnrollModel.child.FavoriteLearningCodeID = IncompleteChildEnrollment.FavoriteLearningCodeID;
                EnrollModel.child.LivesWithCodeID = IncompleteChildEnrollment.LivesWithCodeID;

                //Grades, Chores, Favorite Activities, Favorite Learning drop downs are populated based on child's age.
                if (!string.IsNullOrEmpty(EnrollModel.child.DateOfBirth))
                {
                    var Age = CalculateAge(Convert.ToDateTime(EnrollModel.child.DateOfBirth));
                    var FieldData = GetFieldData(Age);
                    EnrollModel.Grades = FieldData[0].AsEnumerable();
                    EnrollModel.ChoresatHome = FieldData[1].AsEnumerable();
                    EnrollModel.FavActivities = FieldData[2].AsEnumerable();
                    EnrollModel.FavLearning = FieldData[3].AsEnumerable();
                }

                EnrollModel.AlreadyUploadedDocuments = _svc.GetEnrollmentFile(new DownLoadFile() { ChildID = id, UserID = EnrollModel.currentUser.ToString() }).ToList();

                var contnetTypes = CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type");
                if (EnrollModel.AlreadyUploadedDocuments != null && EnrollModel.AlreadyUploadedDocuments.Count() > 0)
                {
                    ProcessAlreadyUplaodedDocuments(EnrollModel.AlreadyUploadedDocuments);
                    var profileContnetTypeID = contnetTypes.FirstOrDefault(a => a.Content_Type.Contains("Profile")).Content_Type_Code_ID;
                    var profile = EnrollModel.AlreadyUploadedDocuments.FirstOrDefault(a => a.ContentTypeID == profileContnetTypeID);
                    if (profile != null)
                    {
                        EnrollModel.child.ProfilePhoto = new FileModel();
                        EnrollModel.child.ProfilePhoto.FileBytes = profile.FileBytes;
                        EnrollModel.child.ProfilePhoto.FileName = profile.FileName;
                    }
                }
                //Get the child profile photo and store it in model
                //DownLoadFile photo = new DownLoadFile();
                // photo.ChildID = id;
                //photo.UserID = EnrollModel.currentUser.ToString();
                //TODO: Profile Photo has a content typr id = 4, later get it from the code tables cached data
                // photo.ContentTypeID = '4';
                // var child_ProfilePhoto = _svc.GetChildProfilePhoto(photo);
                //var child_ProfilePhoto = _svc.GetUpdateFile(photo);
                //EnrollModel.child.ProfilePhoto.FileBytes = child_ProfilePhoto.FileBytes;
                //EnrollModel.child.ProfilePhoto.FileName = child_ProfilePhoto.FileName;

                return EnrollModel;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to setup the view for the child enrollment !", ex);
                return null;
            }
        }


        public CommonVM setupDeclinedEnrollmentView(string id)
        {
            try
            {
                var declineEnrollmentModel = new CommonVM();
                declineEnrollmentModel.Declines = _svc.GetChildDeclines(id, 0, false, declineEnrollmentModel.currentUser.ToString(), true).ToList();
                declineEnrollmentModel.currentUser = SessionManager.Get<UserInfo>("UserInfo");

                //2 child objects are returned from the API object at index 0 is from pending and object at 1 is from dbo. for enrollment always populate data from pending so index 0
                //calling this because the child is in pending table and not in enrollment anymore
                var childDetails = _svc.GetIncompleteChildUpdate(id, declineEnrollmentModel.currentUser.ToString());
                var child = childDetails[0];
                var dboChild = childDetails[1];

                //// Populate the fields in the child information tab from pending
                declineEnrollmentModel.child = new Child();
                declineEnrollmentModel.child.ChildID = child.ChildID;
                declineEnrollmentModel.child.ChildNumber = child.ChildNum;
                declineEnrollmentModel.child.LocationCodeID = child.LocationCodeID.ToString();
                declineEnrollmentModel.child.CountryId = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Location")).Where(c => c.Item1 == declineEnrollmentModel.child.LocationCodeID).Select(m => m.Item3).SingleOrDefault();
                declineEnrollmentModel.child.LocationName = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Location")).Where(c => c.Item1 == declineEnrollmentModel.child.LocationCodeID).Select(m => m.Item2).SingleOrDefault();
                declineEnrollmentModel.child.FirstName = child.FirstName;
                declineEnrollmentModel.child.MiddleName = child.MiddleName;
                declineEnrollmentModel.child.LastName = child.LastName;
                declineEnrollmentModel.child.OtherNameGoesBy = child.OtherNameGoesBy;
                declineEnrollmentModel.child.DateOfBirth = child.DateOfBirth.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                declineEnrollmentModel.child.GenderCodeID = child.GenderCodeID;

                //Populate the fields in the Additional Information
                declineEnrollmentModel.child.DisabilityStatusCodeID = child.DisabilityStatus == true ? 1 : 0;
                declineEnrollmentModel.child.GradeLevelCodeID = child.GradeLevelCodeID;
                declineEnrollmentModel.child.Chores = child.ChoreIDs.Select(s => new SelectListItem { Text = s.ToString(), Value = s.ToString() });
                declineEnrollmentModel.child.NumberOfBrothers = child.NumberOfBrothers;
                declineEnrollmentModel.child.NumberOfSisters = child.NumberOfSisters;
                declineEnrollmentModel.child.PersonalityTypeCodeID = child.PersonalityTypeID;
                declineEnrollmentModel.child.FavActs = child.FavoriteActivitieIDs.Select(s => new SelectListItem { Text = s.ToString(), Value = s.ToString() });
                declineEnrollmentModel.child.FavoriteLearningCodeID = child.FavoriteLearningCodeID;
                declineEnrollmentModel.child.LivesWithCodeID = child.LivesWithCodeID;

                //Grades, Chores, Favorite Activities, Favorite Learning drop downs are populated based on child's age.
                if (!string.IsNullOrEmpty(declineEnrollmentModel.child.DateOfBirth))
                {
                    var Age = CalculateAge(Convert.ToDateTime(declineEnrollmentModel.child.DateOfBirth));
                    var FieldData = GetFieldData(Age);
                    declineEnrollmentModel.Grades = FieldData[0].AsEnumerable();
                    declineEnrollmentModel.ChoresatHome = FieldData[1].AsEnumerable();
                    declineEnrollmentModel.FavActivities = FieldData[2].AsEnumerable();
                    declineEnrollmentModel.FavLearning = FieldData[3].AsEnumerable();
                }
                var contnetTypes = CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type");

                List<SelectListItem> declinedContnetTypes = new List<SelectListItem>();
                var profileContnetTypeID = contnetTypes.FirstOrDefault(a => a.Content_Type.Contains("Profile")).Content_Type_Code_ID;

                //if (workflowType == "Enrollment")
                //{
                var files = _svc.GetUpdateFile(new DownLoadFile() { ChildID = id, UserID = declineEnrollmentModel.currentUser.ToString(), ContentTypeID = profileContnetTypeID }).ToList();

                if (files != null && files.Count() == 1)
                {
                    declineEnrollmentModel.child.ProfilePhoto = new FileModel();
                    declineEnrollmentModel.child.ProfilePhoto.FileBytes = files[0].FileBytes;
                    declineEnrollmentModel.child.ProfilePhoto.FileName = files[0].FileName;
                }
                // }
                //else if(workflowType == "Update")
                //{
                //    //Get the child profile photo and store it in model
                //    ChildProfilePhoto photo = new ChildProfilePhoto();
                //    photo.ChildID = id;
                //    photo.UserID = declineEnrollmentModel.currentUser.ToString();
                //    var child_ProfilePhoto = _svc.GetChildProfilePhoto(photo);
                //    declineEnrollmentModel.child.ProfilePhoto = new FileModel();
                //    declineEnrollmentModel.child.ProfilePhoto.FileBytes = child_ProfilePhoto.FileBytes;
                //    declineEnrollmentModel.child.ProfilePhoto.FileName = child_ProfilePhoto.FileName;
                //    declineEnrollmentModel.child.MajorLifeEvent = child.MajorLifeEvent;

                //    // Populate the child history object 
                //    declineEnrollmentModel.ChildHistory = new CommonVM();
                //    declineEnrollmentModel.ChildHistory.Disabalitites = dboChild.DisabilityStatus == true ? declineEnrollmentModel.Disabalitites.Where(m => m.Value == "1") : declineEnrollmentModel.Disabalitites.Where(m => m.Value == "0");
                //    declineEnrollmentModel.ChildHistory.Grades = declineEnrollmentModel.Grades.Where(m => m.Value == dboChild.GradeLevelCodeID.ToString());
                //    declineEnrollmentModel.ChildHistory.ChoresatHome = declineEnrollmentModel.ChoresatHome.Where(m => dboChild.ChoreIDs.ToList().Contains(Convert.ToInt32(m.Value)));
                //    declineEnrollmentModel.ChildHistory.Brothers = dboChild.NumberOfBrothers;
                //    declineEnrollmentModel.ChildHistory.Sisters = dboChild.NumberOfSisters;
                //    declineEnrollmentModel.ChildHistory.Personalities = declineEnrollmentModel.Personalities.Where(m => m.Value == dboChild.PersonalityTypeID.ToString());
                //    declineEnrollmentModel.ChildHistory.FavActivities = declineEnrollmentModel.FavActivities.Where(m => dboChild.FavoriteActivitieIDs.ToList().Contains(Convert.ToInt32(m.Value)));
                //    declineEnrollmentModel.ChildHistory.FavLearning = declineEnrollmentModel.FavLearning.Where(m => m.Value == dboChild.FavoriteLearningCodeID.ToString());
                //    declineEnrollmentModel.ChildHistory.LivesWith = declineEnrollmentModel.LivesWith.Where(m => m.Value == dboChild.LivesWithCodeID.ToString());
                //    declineEnrollmentModel.ChildHistory.MajorLifeEvent = dboChild.MajorLifeEvent;
                //}
                return declineEnrollmentModel;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to setup the view for the child enrollment !", ex);
                return null;
            }
        }

        public CommonVM setupTranslationView(string id)
        {
            try
            {
                var model = new CommonVM();
                model.currentUser = SessionManager.Get<UserInfo>("UserInfo");
                model.Declines = new List<DeclineDetails>();
                var IncompleteChildUpdates = new AdminChild[2];
                IncompleteChildUpdates[0] = new AdminChild();
                IncompleteChildUpdates[1] = new AdminChild();
                var data = _svc.GetIncompleteChildUpdate(id, model.currentUser.ToString());

                //if the translated document is for enrollment then the child object needs to be pulled in from pending
                //if the translated document is related to update then the child object in pending will not have first name, last name etc as it is coming from dbo and does not change on update
                //if the record at index o /pending has base child info display ot from there if not then display it from dbo

                // This will happen in case of update translations
                if (data[0].FirstName == null && data[1].FirstName != null)
                {
                    // data[0] contains pending child update info.
                    IncompleteChildUpdates[0] = data[0];
                    //data[1] contains child info. 
                    IncompleteChildUpdates[1] = data[1];
                }
                // This happens in case of enrollment 
                else if (data[0].FirstName != null && data[1].FirstName == null)
                {
                    //data[1] contains pending child update info. data[0] contains child info. map both tabs to pending info
                    IncompleteChildUpdates[0] = data[0];

                    IncompleteChildUpdates[1] = data[0];
                }

                // Populate the pending child update object
                // Populate the fields in the child information tab
                model.child = new Child();
                // PopulateDatalists(updateModel);
                model.child.ChildID = IncompleteChildUpdates[0].ChildID;
                model.child.ChildNumber = IncompleteChildUpdates[1].ChildNum;
                model.child.LocationCodeID = IncompleteChildUpdates[1].LocationCodeID.ToString();
                model.child.CountryId = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Location")).Where(c => c.Item1 == model.child.LocationCodeID).Select(m => m.Item3).SingleOrDefault();
                model.child.LocationName = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Location")).Where(c => c.Item1 == model.child.LocationCodeID).Select(m => m.Item2).SingleOrDefault();
                model.child.FirstName = IncompleteChildUpdates[1].FirstName;
                model.child.MiddleName = IncompleteChildUpdates[1].MiddleName;
                model.child.LastName = IncompleteChildUpdates[1].LastName;
                model.child.OtherNameGoesBy = IncompleteChildUpdates[1].OtherNameGoesBy;
                model.child.DateOfBirth = IncompleteChildUpdates[1].DateOfBirth.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                model.child.GenderCodeID = IncompleteChildUpdates[1].GenderCodeID;

                // Populate the fields in the Additional Information tab
                model.child.DisabilityStatusCodeID = IncompleteChildUpdates[0].DisabilityStatus == true ? 1 : 0;
                model.child.GradeLevelCodeID = IncompleteChildUpdates[0].GradeLevelCodeID;
                model.child.Chores = IncompleteChildUpdates[0].ChoreIDs.Select(s => new SelectListItem { Text = s.ToString(), Value = s.ToString() });
                model.child.NumberOfBrothers = IncompleteChildUpdates[0].NumberOfBrothers;
                model.child.NumberOfSisters = IncompleteChildUpdates[0].NumberOfSisters;
                model.child.PersonalityTypeCodeID = IncompleteChildUpdates[0].PersonalityTypeID;
                model.child.FavActs = IncompleteChildUpdates[0].FavoriteActivitieIDs.Select(s => new SelectListItem { Text = s.ToString(), Value = s.ToString() });
                model.child.FavoriteLearningCodeID = IncompleteChildUpdates[0].FavoriteLearningCodeID;
                model.child.LivesWithCodeID = IncompleteChildUpdates[0].LivesWithCodeID;
                model.child.MajorLifeEvent = IncompleteChildUpdates[0].MajorLifeEvent;

                //Grades, Chores, Favorite Activities, Favorite Learning drop downs are populated based on child's age.
                if (!string.IsNullOrEmpty(model.child.DateOfBirth))
                {
                    var Age = CalculateAge(Convert.ToDateTime(model.child.DateOfBirth));
                    var FieldData = GetFieldData(Age);
                    model.Grades = FieldData[0].AsEnumerable();
                    model.ChoresatHome = FieldData[1].AsEnumerable();
                    model.FavActivities = FieldData[2].AsEnumerable();
                    model.FavLearning = FieldData[3].AsEnumerable();
                }


                //Get the child profile photo and store it in model
                ChildProfilePhoto photo = new ChildProfilePhoto();
                photo.ChildID = id;
                photo.UserID = model.currentUser.ToString();
                var child_ProfilePhoto = _svc.GetChildProfilePhoto(photo);
                model.child.ProfilePhoto = new FileModel();
                model.child.ProfilePhoto.FileBytes = child_ProfilePhoto.FileBytes;
                model.child.ProfilePhoto.FileName = child_ProfilePhoto.FileName;

                return model;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to setup the view for the child update !", ex);
                return null;
            }
        }

        public string UploadNewDocumentForDeclined(string documentType)
        {
            var ContentType = (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).ToList();
            ContentTypeModel contentDatails = ContentType.FirstOrDefault(a => a.Content_Type.Contains(documentType));
            var file = SessionManager.Get<UploadFile>("ContentType_" + contentDatails.Content_Type_Code_ID);
            // upload File only when it is not already uploaded and was retrieved from DB when working on incomplete saved enrollments
            if (file != null && file.FileBytes.Length > 0)
            {
                file.ContentTypeID = contentDatails.Content_Type_Code_ID;
                var files_uploaded = _svc.UploadAnUpdateFile(file);
                if (files_uploaded[0] == null || files_uploaded[0].Code == "-2")
                {
                    var data = new { Success = false, Message = "There was error uploading " + contentDatails.Description };
                    return JsonConvert.SerializeObject(data);
                }
                else
                {
                    file.FileID = Helper.ParseInt(files_uploaded[0].Code);
                    var data = new { Success = true, Message = "Successfully uploaded " + contentDatails.Description };
                    return JsonConvert.SerializeObject(data);
                }
            }
            else
            {
                var data = new { Success = false, Message = "File not Founnd please upload the  " + contentDatails.Description + " before completing the declined step" };
                return JsonConvert.SerializeObject(data);
            }
        }

        private void PopulateDatalists(CommonVM model)
        {
            var FavLearn_Cache = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Favorite_Learning"));
            var Grades_Cache = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Grade_Level"));
            var Chores_Cache = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Chore"));
            var FavActs_Cache = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Favorite_Activity"));

            var FavLearningList = new List<SelectListItem>();
            var GradesList = new List<SelectListItem>();
            var ChoresList = new List<SelectListItem>();
            var FavActList = new List<SelectListItem>();

            foreach (var item in FavLearn_Cache)
            {
                FavLearningList.Add(new SelectListItem { Text = item.Item2, Value = item.Item1 });
            }
            model.FavLearning = FavLearningList;

            foreach (var item in Grades_Cache)
            {
                GradesList.Add(new SelectListItem { Text = item.Item2, Value = item.Item1 });
            }
            model.Grades = GradesList;

            foreach (var item in Chores_Cache)
            {
                ChoresList.Add(new SelectListItem { Text = item.Item2, Value = item.Item1 });
            }
            model.ChoresatHome = ChoresList;

            foreach (var item in FavActs_Cache)
            {
                FavActList.Add(new SelectListItem { Text = item.Item2, Value = item.Item1 });
            }
            model.FavActivities = FavActList;
        }

        // create thumbnails and add the already uploaded documents to the session for displaying then when the thumbnail is clicked on
        private void ProcessAlreadyUplaodedDocuments(List<DownLoadFile> AlreadyUploadedDocuments)
        {
            var contnetTypes = CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type");
            if (AlreadyUploadedDocuments != null && AlreadyUploadedDocuments.Count() > 0)
            {
                foreach (var file in AlreadyUploadedDocuments)
                {
                    Image image = null;
                    if (Path.GetExtension(file.FileName).ToLower() == ".pdf")
                    {
                        var FilePath = HttpContext.Current.Server.MapPath("~/Content/pdf-thumbnail.jpg");
                        image = Image.FromFile(FilePath);
                    }
                    if (Path.GetExtension(file.FileName).ToLower() == ".doc" || Path.GetExtension(file.FileName).ToLower() == ".docx")
                    {
                        var FilePath = HttpContext.Current.Server.MapPath("~/Content/doc.jpg");
                        image = Image.FromFile(FilePath);
                    }
                    if (Path.GetExtension(file.FileName).ToLower() == ".jpg")
                    {
                        using (MemoryStream mStream = new MemoryStream(file.FileBytes))
                        {
                            image = Image.FromStream(mStream);
                        }
                    }

                    // use the userid to store the bytes converted to string for storing purposes
                    file.UserID = createThumbnail(image);
                    var contnetName = "TranslatedDocument";
                    if (file.ContentTypeID != 0)
                    {
                        contnetName = contnetTypes.FirstOrDefault(a => a.Content_Type_Code_ID == file.ContentTypeID).Description;
                    }
                    file.FileName = contnetName + "_" + file.FileName;
                    UploadFile uploadedFile = new UploadFile() { ChildID = file.ChildID, ContentTypeID = file.ContentTypeID, ExtensionData = file.ExtensionData, FileBytes = file.FileBytes, FileName = file.FileName, FileTypeID = file.FileTypeID, FileStreamID = file.FileStreamID, PrimaryStreamID = file.PrimaryStreamID };
                    SessionManager.Set("ContentType_" + file.ContentTypeID + "_AlreadyUploaded", uploadedFile);
                }
            }
        }

        public CommonVM SetupChildRecordView(string id)
        {
            try
            {
                var childRecordModel = new CommonVM();

                childRecordModel.currentUser = SessionManager.Get<UserInfo>("UserInfo");
                childRecordModel.Declines = new List<DeclineDetails>();
                //2 child objects are returned from the API object at index 0 is from pending and object at 1 is from dbo. for enrollment always populate data from pending so index 0
                var adminChild = _svc.GeteChild(id, childRecordModel.currentUser.ToString());

                //var data = _svc.GetEnrollmentFile(new DownLoadFile());
                // Populate the fields in the child information tab from pending
                childRecordModel.child = new Child();
                childRecordModel.child.ChildID = adminChild.ChildID;
                childRecordModel.child.ChildNumber = adminChild.ChildNum;
                childRecordModel.child.LocationCodeID = adminChild.LocationCodeID.ToString();
                childRecordModel.child.CountryId = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Location")).Where(c => c.Item1 == childRecordModel.child.LocationCodeID).Select(m => m.Item3).SingleOrDefault();
                childRecordModel.child.LocationName = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Location")).Where(c => c.Item1 == childRecordModel.child.LocationCodeID).Select(m => m.Item2).SingleOrDefault();
                childRecordModel.child.FirstName = adminChild.FirstName;
                childRecordModel.child.MiddleName = adminChild.MiddleName;
                childRecordModel.child.LastName = adminChild.LastName;
                childRecordModel.child.OtherNameGoesBy = adminChild.OtherNameGoesBy;
                childRecordModel.child.DateOfBirth = adminChild.DateOfBirth.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                childRecordModel.child.GenderCodeID = adminChild.GenderCodeID;

                //Populate the fields in the Additional Information
                childRecordModel.child.DisabilityStatusCodeID = adminChild.DisabilityStatus == true ? 1 : 0;
                childRecordModel.child.GradeLevelCodeID = adminChild.GradeLevelCodeID;
                childRecordModel.child.Chores = adminChild.ChoreIDs.Select(s => new SelectListItem { Text = s.ToString(), Value = s.ToString() });
                childRecordModel.child.NumberOfBrothers = adminChild.NumberOfBrothers;
                childRecordModel.child.NumberOfSisters = adminChild.NumberOfSisters;
                childRecordModel.child.PersonalityTypeCodeID = adminChild.PersonalityTypeID;
                childRecordModel.child.FavActs = adminChild.FavoriteActivitieIDs.Select(s => new SelectListItem { Text = s.ToString(), Value = s.ToString() });
                childRecordModel.child.FavoriteLearningCodeID = adminChild.FavoriteLearningCodeID;
                childRecordModel.child.LivesWithCodeID = adminChild.LivesWithCodeID;
                childRecordModel.child.MajorLifeEvent = adminChild.MajorLifeEvent;

                //Grades, Chores, Favorite Activities, Favorite Learning drop downs are populated based on child's age.
                if (!string.IsNullOrEmpty(childRecordModel.child.DateOfBirth))
                {
                    var Age = CalculateAge(Convert.ToDateTime(childRecordModel.child.DateOfBirth));
                    var FieldData = GetFieldData(Age);
                    childRecordModel.Grades = FieldData[0].AsEnumerable();
                    childRecordModel.ChoresatHome = FieldData[1].AsEnumerable();
                    childRecordModel.FavActivities = FieldData[2].AsEnumerable();
                    childRecordModel.FavLearning = FieldData[3].AsEnumerable();
                }

                var contentTypes = CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type");
                foreach (var contentType in contentTypes)
                {
                    List<DownLoadFile> _downloadedFiles = _svc.GetChildRecordFile(new DownLoadFile() { ChildID = id, UserID = childRecordModel.currentUser.ToString(), ContentTypeID = contentType.Content_Type_Code_ID }).ToList();
                    foreach (DownLoadFile downloadedFile in _downloadedFiles)
                    {
                        childRecordModel.AlreadyUploadedDocuments.Add(downloadedFile);
                    }
                }

                if (childRecordModel.AlreadyUploadedDocuments != null && childRecordModel.AlreadyUploadedDocuments.Count() > 0)
                {
                    ProcessAlreadyUplaodedDocuments(childRecordModel.AlreadyUploadedDocuments);
                    var profileContnetTypeID = contentTypes.FirstOrDefault(a => a.Content_Type.Contains("Profile")).Content_Type_Code_ID;
                    var profile = childRecordModel.AlreadyUploadedDocuments.FirstOrDefault(a => a.ContentTypeID == profileContnetTypeID);
                    if (profile != null)
                    {
                        childRecordModel.child.ProfilePhoto = new FileModel();
                        childRecordModel.child.ProfilePhoto.ChildID = id;
                        childRecordModel.child.ProfilePhoto.FileBytes = profile.FileBytes;
                        childRecordModel.child.ProfilePhoto.FileName = profile.FileName;
                    }
                }

                return childRecordModel;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to setup the view for the child update !", ex);
                return null;
            }
        }

    }
}