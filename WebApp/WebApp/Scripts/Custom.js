﻿//This function sets the class to 'current' of menu items when they are clicked
$(function () {
    $('#nav li a').each(function () {
        if ($(this).prop('href') == window.location.href) { $(this).addClass('current'); }
    });
});

//This function selects the location ID based on the location name selected.
$(function () {
    $('#child_LocationName').change(function () {
        id1 = $("#child_LocationName option:selected").val();
        $('#child_LocationCodeID > option').each(function () {
            id2 = this.value;
            if (id1 == id2) {
                $(this).prop('selected', true);
            }
        });
    });
});
//This function selects the location name based on the location ID selected.
$(function () {
    $('#child_LocationCodeID').change(function () {
        id1 = $("#child_LocationCodeID option:selected").val();
        $('#child_LocationName > option').each(function () {
            id2 = this.value;
            if (id1 == id2) {
                $(this).prop('selected', true);
            }
        });
    });
});
// This function sets the default values for the contents in Required Files modal popup when it is closed.
$(function () {
    $('#rModal').on('hidden.bs.modal', function () {
        $(this).removeData('bs.modal');
        $('#req_org_image').prop('src', 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==');
        $('#req_uploadfiles').val('');
        $('#req_ok').prop('disabled', true);
        $('#image_valMsg').text('');
        enable_Crop.cancelSelection();
        try
        {
            var canvas = $("#crop_preview")[0];
            context = canvas.getContext("2d");
            context.clearRect(0, 0, canvas.width, canvas.height);
            $('#crop_x1').val('');
            $('#crop_y1').val('');
            $('#crop_width').val('');
            $('#crop_height').val('');
        }
        catch (err)
        {}
    });
    $('#oModal').on('hidden.bs.modal', function () {
        $(this).removeData('bs.modal');
        $('#opt_org_image').prop('src', 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==');
        $('#opt_uploadfiles').val('');
        $('#opt_ok').prop('disabled', true);
    });
});
//This function is triggered when a Required file is uploaded. Sets the profile picture and also validates the image dimensions. 
$(function () {
    $("#req_uploadfiles").on("change", function () {
        var files = !!this.files ? this.files : [];
        var optionName = $('#req_fileSelect option:selected').text();        
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
        var oImage = $("#req_org_image");
        var profileImage = $('#child_profileImage');
        $('#image_valMsg').text('');
        var reader = new FileReader(); // instance of the FileReader
        reader.readAsDataURL(files[0]); // read the local file
        reader.onload = function (e) {
            oImage.attr({ 'src': e.target.result });
            //may need to move it after ok button
            if ($('#module_name').val() == "Enrollment" && optionName.indexOf("Profile") != -1) {
                profileImage.attr({ 'src': e.target.result });
            }

            if (optionName.indexOf("Profile") != -1 || optionName.indexOf("Action") != -1) {
                var image = new Image();
                image.src = e.target.result;
                image.onload = function () {
                    if (image.width < 1200 || image.height < 1600) {
                        $('#image_valMsg').text(new Date().toLocaleString() + ": Image dimensions do not comply with the system requirements (1200 X 1600) !");
                        $('#req_ok').prop('disabled', true);
                    }
                    else {
                        $('#req_ok').prop('disabled', false);
                    }
                }
            }
            else {
                $('#req_ok').prop('disabled', false);
            }
        }
    });
});
//This function is triggered when a Optional file is uploaded.
$(function () {
    $("#opt_uploadfiles").on("change", function () {
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support               

        var oImage = $("#opt_org_image");
        var reader = new FileReader(); // instance of the FileReader
        reader.readAsDataURL(files[0]); // read the local file
        reader.onload = function (e) {
            oImage.attr({ 'src': e.target.result });
        }
        $('#opt_ok').prop('disabled', false);
    });
});

$(function () {
    req_contentTypeSelect();
    opt_contentTypeSelect();
    translate_contentTypeSelect();
    $('#req_fileSelect').change(req_contentTypeSelect);
    $('#opt_fileSelect').change(opt_contentTypeSelect);
    $('#TranslateDropDown').change(translate_contentTypeSelect);
})

//This function is triggered when a file type is selected from the Required file popup drop down. It sets the acceptance criteria for choosing a file.
function req_contentTypeSelect() {
    selected_option = $("#req_fileSelect option:selected").html();
    $('#req_uploadfiles').prop('disabled', false);
    $('#req_uploadfiles').val('');
    $('#req_ok').prop('disabled', true);
    $('#req_org_image').prop('src', 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==');
    try
    {
        var canvas = $("#crop_preview")[0];
        context = canvas.getContext("2d");
        if (selected_option == "Profile") {
            $("#req_uploadfiles").prop('accept', 'image/jpeg');
            context.clearRect(0, 0, canvas.width, canvas.height);
            enable_Crop = $('#req_org_image').imgAreaSelect({ handles: true, aspectRatio: '3:4', onSelectEnd: preview, instance: true });
        }
        else if (selected_option == "Consent") {
            $("#req_uploadfiles").prop('accept', 'application/pdf');
            context.clearRect(0, 0, canvas.width, canvas.height);
        }
        else if (selected_option == "Action") {
            $("#req_uploadfiles").prop('accept', 'image/jpeg');
            context.clearRect(0, 0, canvas.width, canvas.height);
            enable_Crop = $('#req_org_image').imgAreaSelect({ handles: true, aspectRatio: '3:4', onSelectEnd: preview, instance: true });
        }
    }
    catch (ex)
    {

    }
}
//This function is triggered when a file type is selected from the Optional file popup drop down. It sets the acceptance criteria for choosing a file.
function opt_contentTypeSelect() {
    selected_option = $("#opt_fileSelect option:selected").html();
    $('#opt_uploadfiles').prop('disabled', false);
    $('#opt_uploadfiles').val('');
    $('#opt_ok').prop('disabled', true);
    $('#opt_org_image').prop('src', 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==');
    if (selected_option == "PDF") {
        $("#opt_uploadfiles").prop('accept', 'application/pdf');
    }
    else if (selected_option == "Signature") {
        $("#opt_uploadfiles").prop('accept', 'image/jpeg');
    }
    else if (selected_option == "Document") {
        $("#opt_uploadfiles").prop('accept', '.doc,.docx');
    }
    else if (selected_option == "Web") {
        $("#opt_uploadfiles").prop('accept', '.html');
    }
}

//This function is triggered when a file type is selected from the Required file popup drop down. It sets the acceptance criteria for choosing a file.
function translate_contentTypeSelect() {
    selected_option = $('#TranslateDropDown option:selected').html();
    $('#req_uploadfiles').prop('disabled', false);
    $('#req_uploadfiles').val('');
    $('#req_ok').prop('disabled', true);
    $('#req_org_image').prop('src', 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==');
    try{
        var canvas = $("#crop_preview")[0];
        context = canvas.getContext("2d");
        if (selected_option == "Letter") {
            $("#req_uploadfiles").prop('accept', 'application/msword, .doc,.docx');
            context.clearRect(0, 0, canvas.width, canvas.height);
        }
    }
    catch (err)
    {}
}

//This function is triggered when a page is loaded and sets default values based on the module loaded.
$(function () {
    if ($('#module_name').val() == "Enrollment") {
        //Do not disable controls for inprogress child enrollment
        if ($('#child-ID').text().length == 0) {
            $('#AdditionalInformationTab').not('.active').addClass('disabled');
            $('#AdditionalInformationTab').click(function (event) {
                if ($(this).hasClass('disabled')) {
                    return false;
                }
            });
            $('#req_attachbtn').prop('disabled', true);
            $('#opt_attachbtn').prop('disabled', true);
            $('#req_ok').prop('disabled', true);
            $('#opt_ok').prop('disabled', true);
        }
        $('#Menu_Enrollment').addClass('current');
    }
    else if ($('#module_name').val() == "Update") {
        $('#Menu_Update').addClass('current');
        childform_disableState();
        $('#req_ok').prop('disabled', true);
        $('#opt_ok').prop('disabled', true);
    }
    else if ($('#module_name').val() == "QCEnrollment") {
        $('#Menu_QualityControl').addClass('current');
        childform_disableState()
        enrollform_disableState();
        $('#chkDOB').prop('disabled', true);
    }
    else if ($('#module_name').val() == "QCUpdate") {
        $('#Menu_QualityControl').addClass('current');
        childform_disableState()
        enrollform_disableState();

        $('#chkDOB').prop('disabled', true);
    }
    else if ($('#module_name').val() == "Translate") {
        $('#Menu_Translation').addClass('current')
    }

    $('#child_updatescreen').click(function () {
        $('#AdditionalInformationTab').trigger('click');
    })
});
function childform_disableState() {
    $('#child_CountryId').prop('disabled', true);
    $('#child_LocationName').prop('disabled', true);
    $('#child_LocationCodeID').prop('disabled', true);
    $('#child_FirstName').prop('disabled', true);
    $('#child_LastName').prop('disabled', true);
    $('#child_MiddleName').prop('disabled', true);
    $('#child_GenderCodeID').prop('disabled', true);
    $('#child_OtherNameGoesBy').prop('disabled', true);
    $('#child_DateOfBirth').prop('disabled', true);
}

function enrollform_disableState() {
    $('#child_DisabilityStatusCodeID').prop('disabled', true);
    $('#child_GradeLevelCodeID').prop('disabled', true);
    $('#child_ChoreCodeID').prop('disabled', true);
    $('#child_NumberOfBrothers').prop('disabled', true);
    $('#child_NumberOfSisters').prop('disabled', true);
    $('#child_PersonalityTypeCodeID').prop('disabled', true);
    $('#child_FavoriteActivityCodeID').prop('disabled', true);
    $('#child_FavoriteLearningCodeID').prop('disabled', true);
    $('#child_LivesWithCodeID').prop('disabled', true);
    $('#child_MajorLifeEvent').prop('disabled', true);
}
//This function is used to setup jquery Grid for different view sizes.
$(window).on("resize", function () {
    var $grid = $("#table_Grid"),
        newWidth = $grid.closest(".ui-jqgrid").parent().width();
    $grid.jqGrid("setGridWidth", newWidth, true);
});
//This function populates the chores and favorite activities fields in Update, QC Enrollment and QC Update module with the values received from the server and stored in hidden fields.
$(function () {
    if ($('#module_name').val() == "Update" || $('#module_name').val() == "QCEnrollment" || $('#module_name').val() == "QCUpdate" || $('#module_name').val() == "ChildRecord" || ($('#module_name').val() == "Enrollment" && $('#child-ID').text() != "")) { 
        $('#child_Chores option').each(function () {
            var self = this;
            $('#child_ChoreCodeID option').filter(function () {
                return $(this).val() == self.value;
            }).prop('selected', true);
            $('#child_ChoreCodeID').trigger("chosen:updated");
        });

        $('#child_FavActs option').each(function () {
            var self = this;
            $('#child_FavoriteActivityCodeID option').filter(function () {
                return $(this).val() == self.value;
            }).prop('selected', true);          
            $('#child_FavoriteActivityCodeID').trigger("chosen:updated");
        });
    }
});

//This function copies the contents of 'child-ID' element into the 'child_ChildID' element
$(function () {
    if ($('#module_name').val() == "Update") {
        var childId = $('#child-ID').text();
        $('#child_ChildID').prop('value', childId);
    }
});
//This function is used to preview the cropped image and take cropped image coordinates and store in hidden fields.
function preview(img, selection) {
    try{
        var canvas = $("#crop_preview")[0];
        context = canvas.getContext("2d");
        context.clearRect(0, 0, canvas.width, canvas.height);

        var scaleX = 100 / (selection.width || 1);
        var scaleY = 100 / (selection.height || 1);

        var naturalClickPosX = (img.naturalWidth / img.width) * selection.x1;
        var naturalClickPosY = (img.naturalHeight / img.height) * selection.y1;

        var ActualselectedWidth = (img.naturalWidth / img.width) * selection.width;
        var ActualselectedHeight = (img.naturalHeight / img.height) * selection.height;

        context.drawImage(img, naturalClickPosX, naturalClickPosY, ActualselectedWidth, ActualselectedHeight, 0, 0, canvas.width, canvas.height);

        $('#crop_x1').val(naturalClickPosX);
        $('#crop_y1').val(naturalClickPosY);
        $('#crop_width').val(ActualselectedWidth);
        $('#crop_height').val(ActualselectedHeight);
    }
    catch (err)
    {}
}
//This function opens the profile picture when user clicks on the profile picture.
$(function () {
    $('#child_profileImage').on('dblclick', function () {
        var src = $(this).prop('src');
        if ($('#module_name').val() == "Update") {
            var doc;
            doc = $("<div style = 'width: 600px; height:800px' >")
            .append($("<img>", {
                "src": src,
                "float": "left",
                "margin-right": "10px",
                "height": "100%",
                "width": "100%"
            }));
            var w = window.open("", "new window", "width=1000, height=900");
            w.document.write(doc[0].outerHTML);
        }
    });
});
//This function is used to remove duplicate child enrollment
$(function () {
    $('#removeduplicateChild_yes').click(function () {
        var childID = $('#child_ChildID').prop('value');
        var formData = new FormData();
        formData.append('childID', childID);

        $.ajax({
            url: $(this).attr('data-path'),
            type: 'POST',
            data: formData,
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            },
            success: function (data) {
                var Data = JSON.parse(data);
                if (Data.Success == true) {
                    $("#duplicateRecordsModal").modal("hide");
                    $('#validationMsgs').html("");
                    $('#Msgs').html(Data.Message);
                    setTimeout(function () { $('#Menu_Enrollment')[0].click(); }, 3000);
                }
                else {
                    $('#validationMsgs').html(Data.Message);
                }
            },
            processData: false,
            contentType: false
        });
    });
});
//This function is used to remove duplicate child enrollment
$(function () {
    $('#removeduplicateChild_no').click(function () {
        var childID = $('#child_ChildID').prop('value');
        var formData = new FormData();
        var childformDataArray = $("#childform").serializeArray();
        var enrollformDataArray = $("#enrollform").serializeArray();
        for (var i = 0; i < childformDataArray.length; i++) {
            var formDataItem = childformDataArray[i];
            console.log(formDataItem);
            formData.append(formDataItem.name, formDataItem.value);
        }
        for (var i = 0; i < enrollformDataArray.length; i++) {
            var formDataItem = enrollformDataArray[i];
            console.log(formDataItem);
            formData.append(formDataItem.name, formDataItem.value);
        }
        var chosenData = $("#child_ChoreCodeID").chosen().val();
        if (chosenData.length == 0)
        { formData.append('child.ChoreCodeID', ""); }
        var favActData = $("#child_FavoriteActivityCodeID").chosen().val();
        if (favActData.length == 0)
        { formData.append('child.FavoriteActivityCodeID', ""); }
        var NonDupchildArray = [];
        var table = $('#duplicateChilds_body');
        table.find('tr').each(function (i, el) {
            var $tds = $(this).find('td');
            NonDupchildArray.push($tds.eq(0).text());
        });
        formData.append('child.NonDupchildArray', NonDupchildArray);

        $.ajax({
            url: $(this).attr('data-path'),
            type: 'POST',
            data: formData,
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            },
            success: function (data) {
                if (data.Success == true) {
                    $("#duplicateRecordsModal").modal("hide");
                    $('#Msgs').html(data.Message);
                    var validmsgs = '<ul style="color:red"></ul>';
                    $.each(data.validationResult, function (k, v) {
                        validmsgs += "<li>" + v + "</li>";
                    });
                    $.each(data.FileValidation, function (k, v) {
                        validmsgs += "<li>" + v + "</li>";
                    });
                    $('#validationMsgs').html(validmsgs);
                    $('#duplicateChilds_body').empty();
                    $('#Confirm_NonDups').prop('value','true')
                }
                else {
                    $('#validationMsgs').html(Data.Message);
                }
            },
            processData: false,
            contentType: false
        });
    });
});
// This function clears the table body in Possible Duplicate Child modal popup when it is closed.
$(function () {
    $('#duplicateRecordsModal').on('hidden.bs.modal', function () {
        $('#duplicateChilds_body').empty();
    });
});
