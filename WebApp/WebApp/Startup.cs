﻿using Microsoft.Owin;
using Owin;


[assembly: OwinStartupAttribute(typeof(FTCWebApp.Startup))]
namespace FTCWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
