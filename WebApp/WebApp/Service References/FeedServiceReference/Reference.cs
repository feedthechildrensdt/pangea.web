﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FTCWebApp.FeedServiceReference {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ConnectivityTestObject", Namespace="http://schemas.datacontract.org/2004/07/FeedServices.Data")]
    [System.SerializableAttribute()]
    public partial class ConnectivityTestObject : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private byte[] FileBytesField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool GoodFileField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime OriginSentField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime ServerReciveField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime ServerSentField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public byte[] FileBytes {
            get {
                return this.FileBytesField;
            }
            set {
                if ((object.ReferenceEquals(this.FileBytesField, value) != true)) {
                    this.FileBytesField = value;
                    this.RaisePropertyChanged("FileBytes");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool GoodFile {
            get {
                return this.GoodFileField;
            }
            set {
                if ((this.GoodFileField.Equals(value) != true)) {
                    this.GoodFileField = value;
                    this.RaisePropertyChanged("GoodFile");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime OriginSent {
            get {
                return this.OriginSentField;
            }
            set {
                if ((this.OriginSentField.Equals(value) != true)) {
                    this.OriginSentField = value;
                    this.RaisePropertyChanged("OriginSent");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime ServerRecive {
            get {
                return this.ServerReciveField;
            }
            set {
                if ((this.ServerReciveField.Equals(value) != true)) {
                    this.ServerReciveField = value;
                    this.RaisePropertyChanged("ServerRecive");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime ServerSent {
            get {
                return this.ServerSentField;
            }
            set {
                if ((this.ServerSentField.Equals(value) != true)) {
                    this.ServerSentField = value;
                    this.RaisePropertyChanged("ServerSent");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="UserPermissions", Namespace="http://schemas.datacontract.org/2004/07/FeedServices.Data")]
    [System.SerializableAttribute()]
    public partial class UserPermissions : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string AppNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string AppShortNameField;
        
        private FTCWebApp.FeedServiceReference.Permission[] PermissionsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string UserIDField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AppName {
            get {
                return this.AppNameField;
            }
            set {
                if ((object.ReferenceEquals(this.AppNameField, value) != true)) {
                    this.AppNameField = value;
                    this.RaisePropertyChanged("AppName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AppShortName {
            get {
                return this.AppShortNameField;
            }
            set {
                if ((object.ReferenceEquals(this.AppShortNameField, value) != true)) {
                    this.AppShortNameField = value;
                    this.RaisePropertyChanged("AppShortName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public FTCWebApp.FeedServiceReference.Permission[] Permissions {
            get {
                return this.PermissionsField;
            }
            set {
                if ((object.ReferenceEquals(this.PermissionsField, value) != true)) {
                    this.PermissionsField = value;
                    this.RaisePropertyChanged("Permissions");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string UserID {
            get {
                return this.UserIDField;
            }
            set {
                if ((object.ReferenceEquals(this.UserIDField, value) != true)) {
                    this.UserIDField = value;
                    this.RaisePropertyChanged("UserID");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Permission", Namespace="http://schemas.datacontract.org/2004/07/FeedServices.Data")]
    [System.SerializableAttribute()]
    public partial class Permission : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private string DescriptionField;
        
        private string NameField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string Description {
            get {
                return this.DescriptionField;
            }
            set {
                if ((object.ReferenceEquals(this.DescriptionField, value) != true)) {
                    this.DescriptionField = value;
                    this.RaisePropertyChanged("Description");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="FeedServiceReference.IFeedService")]
    public interface IFeedService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IFeedService/TestConnectivity", ReplyAction="http://tempuri.org/IFeedService/TestConnectivityResponse")]
        FTCWebApp.FeedServiceReference.ConnectivityTestObject TestConnectivity(FTCWebApp.FeedServiceReference.ConnectivityTestObject test);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IFeedService/TestConnectivity", ReplyAction="http://tempuri.org/IFeedService/TestConnectivityResponse")]
        System.Threading.Tasks.Task<FTCWebApp.FeedServiceReference.ConnectivityTestObject> TestConnectivityAsync(FTCWebApp.FeedServiceReference.ConnectivityTestObject test);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IFeedService/GetUserPermissions", ReplyAction="http://tempuri.org/IFeedService/GetUserPermissionsResponse")]
        FTCWebApp.FeedServiceReference.UserPermissions GetUserPermissions(FTCWebApp.FeedServiceReference.UserPermissions user);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IFeedService/GetUserPermissions", ReplyAction="http://tempuri.org/IFeedService/GetUserPermissionsResponse")]
        System.Threading.Tasks.Task<FTCWebApp.FeedServiceReference.UserPermissions> GetUserPermissionsAsync(FTCWebApp.FeedServiceReference.UserPermissions user);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IFeedServiceChannel : FTCWebApp.FeedServiceReference.IFeedService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class FeedServiceClient : System.ServiceModel.ClientBase<FTCWebApp.FeedServiceReference.IFeedService>, FTCWebApp.FeedServiceReference.IFeedService {
        
        public FeedServiceClient() {
        }
        
        public FeedServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public FeedServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public FeedServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public FeedServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public FTCWebApp.FeedServiceReference.ConnectivityTestObject TestConnectivity(FTCWebApp.FeedServiceReference.ConnectivityTestObject test) {
            return base.Channel.TestConnectivity(test);
        }
        
        public System.Threading.Tasks.Task<FTCWebApp.FeedServiceReference.ConnectivityTestObject> TestConnectivityAsync(FTCWebApp.FeedServiceReference.ConnectivityTestObject test) {
            return base.Channel.TestConnectivityAsync(test);
        }
        
        public FTCWebApp.FeedServiceReference.UserPermissions GetUserPermissions(FTCWebApp.FeedServiceReference.UserPermissions user) {
            return base.Channel.GetUserPermissions(user);
        }
        
        public System.Threading.Tasks.Task<FTCWebApp.FeedServiceReference.UserPermissions> GetUserPermissionsAsync(FTCWebApp.FeedServiceReference.UserPermissions user) {
            return base.Channel.GetUserPermissionsAsync(user);
        }
    }
}
