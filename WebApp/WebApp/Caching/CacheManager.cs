﻿using System;
using System.Configuration;
using System.Runtime.Caching;
using log4net;

namespace FTCWebApp.Caching
{
    //Summary
    //This class takes care of storing and retrieving Field or Column Values to/from the cache.
    public static class CacheManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CacheManager));
        private static MemoryCache _cache = MemoryCache.Default;

        static CacheManager()
        {
            SetDataCache.SetCache();
        }

        //Add a value to the cache
        public static void AddToCache<T>(string key, T value)
        {
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes(Convert.ToDouble(ConfigurationManager.AppSettings["MemCacheTime"]))
            };
            _cache.Set(key, value, cacheItemPolicy);
        }

        //Get a value from the cache
        public static T GetFromCache<T>(string key)
        {
            try
            {
                if (_cache.Contains(key))
                {
                    return (T)_cache[key];
                }
                SetDataCache.SetCache();
                return (T)_cache[key];
            }
            catch (Exception ex)
            {
                log.Debug(ex.Message);
                return default(T);
            }
        }
        //Removes a value from the cache
        public static void RemoveFromCache(string key)
        {
            _cache.Remove(key);
        }
    }
}