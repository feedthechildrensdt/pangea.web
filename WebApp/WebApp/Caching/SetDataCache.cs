﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using FTCWebApp.DataModels;
using FTCWebApp.ServiceReference1;
using log4net;

namespace FTCWebApp.Caching
{
    public static class SetDataCache
    {
        private static FieldServicesClient _service = new FieldServicesClient("BasicHttpsBinding_IFieldServices");
        private static DataSet _dataset;
        private static readonly ILog log = LogManager.GetLogger(typeof(SetDataCache));

        //Summary
        //Sets the cache with dataset containing Field tables, Field rules
        public static void SetCache()
        {
            UserInfo _userInfo = SessionManager.Get<UserInfo>("UserInfo");

            try
            {
                _dataset = _service.GetCodeTables(_userInfo.ID.ToString());

                AddAllCodeTableNames(_dataset, "ListofCodeTables");
                AddTupleofThree(_dataset, "Country", "Country_Code_ID", "Country_Name", "Min_School_Age");
                AddLocationFieldData(_dataset, "Location", "Location_Code_ID", "Location_Name", "Country_Code_ID");
                AddFieldData(_dataset, "Gender", "Gender_Code_ID", "Description");
                AddFieldData(_dataset, "Personality_Type", "Personality_Type_Code_ID", "Description");
                AddFieldData(_dataset, "Lives_With", "Lives_With_Code_ID", "Description");
                AddFieldData(_dataset, "Child_Remove_Reason", "Child_Remove_Reason_Code_ID", "Description");
                AddTupleofFive(_dataset, "Action", "Action_ID", "Action_Name", "Enrollment_Eligible", "Update_Eligible", "Decline_Eligible");
                //AddFieldData(_dataset, "Action", "Action_ID", "Action_Name");
                AddFieldData(_dataset, "Action_Reason", "Action_Reason_ID", "Reason_Description");
                //AddFieldData(_dataset, "Workflow", "Workflow_Code_ID", "Description");
                AddTupleofThree(_dataset, "Workflow", "Workflow_Code_ID", "Description", "QC_Available");
                AddTupleofThree(_dataset, "Favorite_Learning", "Favorite_Learning_Code_ID", "Description", "Min_Age");
                AddTupleofThree(_dataset, "Grade_Level", "Grade_Level_Code_ID", "Description", "Min_Age");
                AddTupleofThree(_dataset, "Chore", "Chore_Code_ID", "Description", "Min_Age");
                AddTupleofThree(_dataset, "Favorite_Activity", "Favorite_Activity_Code_ID", "Description", "Min_Age");
                AddContentType(_dataset, "Content_Type", "Content_Type_Code_ID", "Content_Type", "Description", "Required");
                AddFileType(_dataset, "File_Type", "File_Type_Code_ID", "File_Type", "File_Description", "File_Extension");
                AddFieldRules(_dataset, "Column_Rules", "Column_Rules_Name", "Module_Name", "Table_Name", "Column_Name", "Default_String_Value", "Default_int_Value", "Min_int_Allowed_Value", "Max_int_Allowed_Value", "Required", "Validation_Type");
                AddFieldData(_dataset, "Decline_Reason", "Decline_Reason_Code_ID", "Decline_Reason");
                AddDeclineReasonSubCategoryMapping(_dataset);
                AddFieldData(_dataset, "Status", "Status_Code_ID", "Description");
                AddWorkflowStepInformation(_dataset, "Step", "Description", "Translation_Required", "Step_Code_ID");
            }
            catch (Exception ex)
            {
                log.Debug("Failed to load the dataset from the remote service !", ex);
            }
        }

        /// <summary>
        /// Create a list of all code tables that will be managed from the web app.
        /// </summary>
        public static void AddAllCodeTableNames(DataSet dataset, string key)
        {
            List<SelectListItem> FieldList = new List<SelectListItem>();
            foreach (DataRow row in dataset.Tables[0].Rows)
            {

                string tableName = row.ItemArray.GetValue(0).ToString();
                
                ;

                if (
                    tableName == "Child_Remove_Reason" || 
                    tableName == "Gender" || 
                    tableName == "Lives_With" || 
                    tableName == "Personality_Type" || 
                    tableName == "Chore" ||
                    tableName == "Favorite_Activity" || 
                    tableName == "Favorite_Learning" || 
                    tableName == "Grade_Level" || 
                    tableName == "Content_Type" || 
                    tableName == "Country" ||
                    tableName == "Location" || 
                    tableName == "Region" ||
                    tableName == "Decline_Reason"
                    )
                {
                    FieldList
                        .Add
                        (
                        new SelectListItem 
                        { 
                            Value = tableName, 
                            Text = tableName 
                        }
                    );
                }
            }

            CacheManager.AddToCache(key, FieldList);
        }

        //Summary
        //This method extracts form fields data (dropdown values) from the dataset and stores it in the cache with the same name as the table name. 
        public static void AddFieldData(DataSet dataset, string TableName, string FieldValue, string FieldText)
        {
            try
            {
                List<SelectListItem> FieldList = new List<SelectListItem>();

                var tableIndex = Indexof(TableName);
                var tableName = IndexName(tableIndex);

                if (tableName != null && (tableName == _dataset.Tables[tableIndex].TableName))
                {
                    foreach (DataRow row in _dataset.Tables[tableIndex].Rows)
                    {
                        FieldList.Add(new SelectListItem { Value = Convert.ToString(row[FieldValue]), Text = Convert.ToString(row[FieldText]) });
                    }
                    CacheManager.AddToCache(TableName, FieldList);
                }
                else { CacheManager.AddToCache(TableName, Enumerable.Empty<SelectListItem>().ToList()); }
            }
            catch (Exception ex)
            {
                log.Debug("Failed to add the " + TableName + " Field from the dataset to the cache !", ex);
            }

        }

        //Summary
        //This method extracts Content types data from the dataset and stores it in the cache with the same name as the table name. 
        public static void AddContentType(DataSet dataset, string TableName, string ContentTypeID, string ContentType, string Description, string Required)
        {
            try
            {
                List<ContentTypeModel> FieldList = new List<ContentTypeModel>();

                var tableIndex = Indexof(TableName);
                var tableName = IndexName(tableIndex);

                if (tableName != null && (tableName == _dataset.Tables[tableIndex].TableName))
                {
                    foreach (DataRow row in _dataset.Tables[tableIndex].Rows)
                    {
                        FieldList.Add(new ContentTypeModel { Content_Type_Code_ID = Convert.ToInt32(row[ContentTypeID]), Content_Type = Convert.ToString(row[ContentType]), Description = Convert.ToString(row[Description]), Required = (bool)(row[Required]) });
                    }
                    CacheManager.AddToCache(TableName, FieldList);
                }
                else { CacheManager.AddToCache(TableName, Enumerable.Empty<SelectListItem>().ToList()); }
            }
            catch (Exception ex)
            {
                log.Debug("Failed to add the Content Types from the dataset to the cache !", ex);
            }
        }

        //Summary
        //This method extracts File types data from the dataset and stores it in the cache with the same name as the table name. 
        public static void AddFileType(DataSet dataset, string TableName, string FileTypeID, string FileType, string FileDescription, string FileExtension)
        {
            try
            {
                List<FileTypeModel> FieldList = new List<FileTypeModel>();

                var tableIndex = Indexof(TableName);
                var tableName = IndexName(tableIndex);

                if (tableName != null && (tableName == _dataset.Tables[tableIndex].TableName))
                {
                    foreach (DataRow row in _dataset.Tables[tableIndex].Rows)
                    {
                        FieldList.Add(new FileTypeModel { File_Type_Code_ID = Convert.ToInt32(row[FileTypeID]), File_Type = Convert.ToString(row[FileType]), File_Description = Convert.ToString(row[FileDescription]), File_Extension = Convert.ToString(row[FileExtension]) });
                    }
                    CacheManager.AddToCache(TableName, FieldList);
                }
                else { CacheManager.AddToCache(TableName, Enumerable.Empty<SelectListItem>().ToList()); }
            }
            catch (Exception ex)
            {
                log.Debug("Failed to add the File Types from the dataset to the cache !", ex);
            }
        }


        //Summary
        //This method gets the index of the table from the dataset 
        private static int Indexof(string TableName)
        {
            DataRow tableName = _dataset.Tables[0].AsEnumerable().FirstOrDefault(row => row["Table_Name"].ToString() == TableName);
            var index = _dataset.Tables[0].Rows.IndexOf(tableName);
            if (index >= 0)
            {
                return index + 1;
            }

            return -1;
        }

        //Summary
        //This method sets the actual name of the table (datatable name in dataset) 
        //by concatinating the string 'table' and the index of the table derived from "Indexof" method 
        private static string IndexName(int tableIndex)
        {
            var index = tableIndex;
            if (index > 0)
            {
                return "Table" + Convert.ToString(index);
            }
            return null;
        }

        //Summary
        //This method adds the List of Tuples consisting of Location Name, Location Code, Country Id and stores in the cache
        private static void AddLocationFieldData(DataSet dataset, string TableName, string LocationFieldValue, string LocationFieldText, string LocationCountryCode)
        {
            try
            {
                List<Tuple<string, string, string>> LocationList = new List<Tuple<string, string, string>>();
                var tableIndex = Indexof(TableName);
                var tableName = IndexName(tableIndex);

                if (tableName != null && (tableName == _dataset.Tables[tableIndex].TableName))
                {
                    foreach (DataRow row in _dataset.Tables[tableIndex].Rows)
                    {
                        LocationList.Add(new Tuple<string, string, string>(Convert.ToString(row[LocationFieldValue]), Convert.ToString(row[LocationFieldText]), Convert.ToString(row[LocationCountryCode])));
                    }
                    CacheManager.AddToCache(TableName, LocationList);
                }
                else { CacheManager.AddToCache(TableName, Enumerable.Empty<SelectListItem>().ToList()); }
            }
            catch (Exception ex)
            {
                log.Debug("Failed to add the Location Field data from the dataset to the cache !", ex);
            }
        }

        //Summary
        //This method adds the column rules extracted from the dataset to the cache
        private static void AddFieldRules(DataSet dataset, string TableName, string Column_Rules_Name, string Module_Name, string Database_Table_Name, string Column_Name, string Default_String_Value, string Default_int_Value, string Min_int_Allowed_Value, string Max_int_Allowed_Value, string Required, string Validation_Type)
        {
            try
            {
                List<FieldRuleModel> FieldRuleList = new List<FieldRuleModel>();
                var tableIndex = Indexof(TableName);
                var tableName = IndexName(tableIndex);

                if (tableName != null && (tableName == _dataset.Tables[tableIndex].TableName))
                {
                    foreach (DataRow row in _dataset.Tables[tableIndex].Rows)
                    {
                        if (FieldRuleList.Exists(m => m.FieldName == row["Column_Name"].ToString()))
                        {
                            var item = FieldRuleList.Where(m => m.FieldName == row["Column_Name"].ToString()).FirstOrDefault();
                            item.ColumnRules.Add(new ColumnRuleModel() { ColumnRuleName = Convert.ToString(row[Column_Rules_Name]), ModuleName = Convert.ToString(row[Module_Name]), TableName = Convert.ToString(row[Database_Table_Name]), ColumnName = Convert.ToString(row[Column_Name]), defaultString = Convert.ToString(row[Default_String_Value]), defaultValue = Convert.ToString(row[Default_int_Value]), minValue = Convert.ToString(row[Min_int_Allowed_Value]), maxValue = Convert.ToString(row[Max_int_Allowed_Value]), isRequired = (bool)(row[Required]), validationType = Convert.ToString(row[Validation_Type]) });
                        }
                        else
                        {
                            FieldRuleList.Add(new FieldRuleModel() { FieldName = Convert.ToString(row[Column_Name]), ColumnRules = new List<ColumnRuleModel> { new ColumnRuleModel { ColumnRuleName = Convert.ToString(row[Column_Rules_Name]), ModuleName = Convert.ToString(row[Module_Name]), TableName = Convert.ToString(row[Database_Table_Name]), ColumnName = Convert.ToString(row[Column_Name]), defaultString = Convert.ToString(row[Default_String_Value]), defaultValue = Convert.ToString(row[Default_int_Value]), minValue = Convert.ToString(row[Min_int_Allowed_Value]), maxValue = Convert.ToString(row[Max_int_Allowed_Value]), isRequired = (bool)(row[Required]), validationType = Convert.ToString(row[Validation_Type]) } } });
                        }
                    }
                    CacheManager.AddToCache(TableName, FieldRuleList);
                }
                else { CacheManager.AddToCache(TableName, Enumerable.Empty<SelectListItem>().ToList()); }
            }
            catch (Exception ex)
            {
                log.Debug("Failed to add the " + TableName + " Field from the dataset to the cache !", ex);
            }

        }

        //Summary
        //This method adds the List of Tuples
        private static void AddTupleofThree(DataSet dataset, string TableName, string item1, string item2, string item3)
        {
            try
            {
                List<Tuple<string, string, string>> List = new List<Tuple<string, string, string>>();
                var tableIndex = Indexof(TableName);
                var tableName = IndexName(tableIndex);

                if (tableName != null && (tableName == _dataset.Tables[tableIndex].TableName))
                {
                    foreach (DataRow row in _dataset.Tables[tableIndex].Rows)
                    {
                        List.Add(new Tuple<string, string, string>(Convert.ToString(row[item1]), Convert.ToString(row[item2]), Convert.ToString(row[item3])));
                    }
                    CacheManager.AddToCache(TableName, List);
                }
                else { CacheManager.AddToCache(TableName, Enumerable.Empty<SelectListItem>().ToList()); }
            }
            catch (Exception ex)
            {
                log.Debug("Failed to add the " + TableName + " Field from the dataset to the cache !", ex);
            }
        }

        //Summary
        //This method adds the List of Tuples
        private static void AddTupleofFive(DataSet dataset, string TableName, string item1, string item2, string item3, string item4, string item5)
        {
            try
            {
                List<Tuple<string, string, string, string, string>> List = new List<Tuple<string, string, string, string, string>>();
                var tableIndex = Indexof(TableName);
                var tableName = IndexName(tableIndex);

                if (tableName != null && (tableName == _dataset.Tables[tableIndex].TableName))
                {
                    foreach (DataRow row in _dataset.Tables[tableIndex].Rows)
                    {
                        List.Add(new Tuple<string, string, string, string, string>(Convert.ToString(row[item1]), Convert.ToString(row[item2]), Convert.ToString(row[item3]), Convert.ToString(row[item4]), Convert.ToString(row[item5])));
                    }
                    CacheManager.AddToCache(TableName, List);
                }
                else { CacheManager.AddToCache(TableName, Enumerable.Empty<SelectListItem>().ToList()); }
            }
            catch (Exception ex)
            {
                log.Debug("Failed to add the " + TableName + " Field from the dataset to the cache !", ex);
            }
        }

        //Add a tuple to the cache to capture the  decline category and sub category relation
        private static void AddDeclineReasonSubCategoryMapping(DataSet dataset)
        {
            try
            {
                List<Tuple<string, string, string>> List = new List<Tuple<string, string, string>>();
                var tableIndex = Indexof("Decline_SubReason");
                var tableName = IndexName(tableIndex);

                if (tableName != null && (tableName == _dataset.Tables[tableIndex].TableName))
                {
                    foreach (DataRow row in _dataset.Tables[tableIndex].Rows)
                    {
                        var declineReasonCodeID = Convert.ToString(row["Decline_Reason_Code_ID"]);
                        var declineReasonsFromCache = CacheManager.GetFromCache<List<SelectListItem>>("Decline_Reason");
                        var data = declineReasonsFromCache.FirstOrDefault(a => a.Value == declineReasonCodeID).Text;

                        List.Add
                            (
                            new Tuple<string, string, string>
                            (
                            Convert.ToString(row["Decline_SubReason_Code_ID"]),
                            data,
                            Convert.ToString(row["Description"])
                            )
                            );
                    }

                    CacheManager.AddToCache("Decline_SubReason", List);
                }
                else { CacheManager.AddToCache("Decline_SubReason", Enumerable.Empty<SelectListItem>().ToList()); }
            }
            catch (Exception ex)
            {
                log.Debug("Failed to add the " + "Decline_SubReason" + " Field from the dataset to the cache !", ex);
            }
        }

        ///Summary
        ///This method extracts Content types data from the dataset and stores it in the cache with the same name as the table name. 
        public static void AddWorkflowStepInformation(DataSet dataset, string TableName, string Description, string Translation_Required, string code_id)
        {
            try
            {
                List<WorkflowStepModel> FieldList = new List<WorkflowStepModel>();

                var tableIndex = Indexof(TableName);
                var tableName = IndexName(tableIndex);

                if (tableName != null && (tableName == _dataset.Tables[tableIndex].TableName))
                {
                    foreach (DataRow row in _dataset.Tables[tableIndex].Rows)
                    {
                        FieldList.Add(new WorkflowStepModel { Description = Convert.ToString(row[Description]), TranslationRequired = (bool)(row[Translation_Required]), step_code_id = (int)(row[code_id]) });
                    }
                    CacheManager.AddToCache(TableName, FieldList);
                }
                else { CacheManager.AddToCache(TableName, Enumerable.Empty<SelectListItem>().ToList()); }
            }
            catch (Exception ex)
            {
                log.Debug("Failed to add the Content Types from the dataset to the cache !", ex);
            }
        }
    }
}