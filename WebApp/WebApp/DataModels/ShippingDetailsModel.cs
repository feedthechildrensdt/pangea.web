﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FTCWebApp.DataModels
{
    public class ShippingDetailsModel
    {
        public string ShipmentID { get; set; }

        [Display(Name = "Shipped on Date:")]
        public string ShippedOnDate { get; set; }

        [Display(Name = "Shipped VIA:")]
        public string ShippedVia { get; set; }

        [Display(Name ="Shipment Weight:")]
        public string ShipmentWeight { get; set; }

        public string Currency { get; set; }
        public string ShippingCost { get; set; }

        [Display(Name = "Tracking Number:")]
        public string TrackingNumber { get; set; }

        [Display(Name = "Shipped from Country:")]
        public string ShippedFromCountry { get; set; }

        [Display(Name = "Shipped By:")]
        public string ShippedBy { get; set; }

        [Display(Name = "Additional Notes:")]
        public string ShippingNotes { get; set; }

        public string RecievedOnDate { get; set; }

        public string RecievedBy { get; set; }

        //[Display(Name ="Children with documents included in shipment:")]
        //public string ChildNumbersIncludedInShipment { get; set; }
    }
}