﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Collections.Generic;
using System;

namespace FTCWebApp.DataModels
{
    //Child Entity Model 
    public class Child
    {
        #region ENROLLMENT
        [Display(Name = "Child ID")]
        public string ChildID { get; set; }

        [Display(Name = "Child Number")]
        public string ChildNumber { get; set; }

        [Display(Name = "Date of Birth")]
        public string DateOfBirth { get; set; }
        
        public DateTime Date_Of_Birth 
        { 
            get 
            {

                DateTime dt_temp = Convert.ToDateTime(DateOfBirth);

                return dt_temp;

            } 
        }
        
        public string Date_Of_Birth_Computed_Age 
        {
            get 
            {
                
                string stemp = "";

                try
                {
                    if (Date_Of_Birth.Year > 1901)
                    {
                        
                        int age = new DateTime(DateTime.Now.Subtract(Date_Of_Birth).Ticks).Year - 1;

                        stemp = age.ToString();

                    }
                }
                catch { }

                return stemp;

            } 
        }



        [Display(Name = "Disability")]
        public int? DisabilityStatusCodeID { get; set; }

        [Display(Name = "Favorite Activities")]
        public int?[] FavoriteActivityCodeID { get; set; }

        [Display(Name = "Favorite Learning")]
        public int? FavoriteLearningCodeID { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Other Name")]
        public string OtherNameGoesBy { get; set; }

        [Display(Name = "Sex")]
        public int? GenderCodeID { get; set; }

        [Display(Name = "School Grade")]
        public int? GradeLevelCodeID { get; set; }

        [Display(Name = "Chores at Home")]
        public int?[] ChoreCodeID { get; set; }

        public int?[] ConfirmedNonDups { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Lives With")]
        public int? LivesWithCodeID { get; set; }

        [Display(Name = "Country")]
        public string CountryId { get; set; }

        [Display(Name = "Workflow")]
        public string WorkflowId { get; set; }

        [Display(Name = "Location Code")]
        public string LocationCodeID { get; set; }

        [Display(Name = "Location Name")]
        public string LocationName { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Display(Name = "Brothers")]
        public int? NumberOfBrothers { get; set; }

        [Display(Name = "Sisters")]
        public int? NumberOfSisters { get; set; }

        [Display(Name = "Personality")]
        public int? PersonalityTypeCodeID { get; set; }
        #endregion

        #region UPDATE 
        [Display(Name = "Major Life Event")]
        public string MajorLifeEvent { get; set; }

        public FileModel[] ChildDocs { get; set; }

        public FileModel ProfilePhoto { get; set; }

        [Display(Name = "Remove Reason")]
        public string RemoveReasonCodeID { get; set; }

        public IEnumerable<SelectListItem> Chores { get; set; }

        public IEnumerable<SelectListItem> FavActs { get; set; }
        #endregion

        #region QC 
        public string Child_WorkflowID { get; set; }

        public string Child_StepCodeID { get; set; }

        public string Pending_ChildID { get; set; }

        [Display(Name = "Decline Reason")]
        public string QCRejectReason { get; set; }

        public string QCDeclineSubReason { get; set; }

        [Display(Name = "Notes")]
        public string QCRejectNotes { get; set; }

        [Display(Name = "Translate")]
        public string TranslatedMLE { get; set; }
        #endregion
    }
}