﻿using FTCWebApp.ServiceReference1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FTCWebApp.DataModels
{
    public class DeclineDetail
    {
        public Decline decline { get; set; }

        public string declineReason { get; set; }

        public string declineSubReason { get; set; }

        public string pendingChildID { get; set; }

        public string fileID { get; set; }
    }
}