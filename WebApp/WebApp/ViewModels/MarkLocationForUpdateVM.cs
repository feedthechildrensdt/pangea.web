﻿using FTCWebApp.Caching;
using FTCWebApp.Classes;
using FTCWebApp.DataModels;
using FTCWebApp.Utillities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;


using FTCWebApp.ViewModels.GUI_Pieces;

namespace FTCWebApp.ViewModels
{
    public class MarkLocationForUpdateVM
    {
        private IEnumerable<SelectListItem> locations;
        private IEnumerable<SelectListItem> locationCodes;

        public MarkLocationForUpdateVM()
        {
            MarkLocationForUpdateVM_Constructor();
        }


        public MarkLocationForUpdateVM
            (
            FTCWebApp.Classes.User_Session_Defaults.enum_Display_Page e_Display_Page,
            HttpSessionStateBase hsb
            )
        {

            MarkLocationForUpdateVM_Constructor();

            user_Session_Defaults = new User_Session_Defaults
                (
                e_Display_Page,
                hsb
                );

        }



        private void MarkLocationForUpdateVM_Constructor()
        {
            ActionType = string.Empty;
            DisplayMessage = string.Empty;

            MarkLocationForUpdateRecord = new MarkLocationForUpdateModel();

            EditMode = false;
        }

        /// <summary>
        /// The Different Action Selection Items possible.
        /// </summary>
        public IEnumerable<SelectListItem> ActionTypes
        {
            get
            {
                var _retVal = new List<SelectListItem>();
                if (EditMode)
                    _retVal.Add(new SelectListItem() { Value = "Edit Site", Text = "Update Location" });
                else
                    _retVal.Add(new SelectListItem() { Value = "Add Site", Text = "Add Location for Update" });

                _retVal.Add(new SelectListItem() { Value = "View Site", Text = "View/Edit/Cancel Location(s) Marked for Update" });
                return _retVal;
            }
        }

        public IEnumerable<SelectListItem> CompletionDaysList
        {
            get
            {
                var _retVal = new List<SelectListItem>();
                _retVal.Add(new SelectListItem() { Value = "90", Text = "90" });
                _retVal.Add(new SelectListItem() { Value = "120", Text = "120" });
                return _retVal;
            }
        }

        /// <summary>
        /// List of the countries
        /// </summary>
        public IEnumerable<SelectListItem> Countries
        {
            get
            {
               
                string the_user_id = currentUser.ID.ToString();

                List<SelectListItem> l_sli = GUI_Pieces_Helper.Get_Users_Country_Drop_Down_List_Items(the_user_id);

                return l_sli;

            }
        }

        /// <summary>
        /// List of the Location names
        /// </summary>
        public IEnumerable<SelectListItem> Locations
        {
            get
            {
                if (!String.IsNullOrEmpty(MarkLocationForUpdateRecord.CountryCodeID))
                {
                    EnrollUtil _util = new EnrollUtil();
                    locations = _util.GetLocationsByCountry(Convert.ToInt32(MarkLocationForUpdateRecord.CountryCodeID))[0].AsEnumerable();
                }
                else if (locations == null || String.IsNullOrEmpty(MarkLocationForUpdateRecord.CountryCodeID))
                    locations = new SelectList(Enumerable.Empty<SelectListItem>());

                return locations;
            }
        }

        /// <summary>
        /// List of the Location Codes
        /// </summary>
        public IEnumerable<SelectListItem> LocationCodes
        {
            get
            {
                if (!String.IsNullOrEmpty(MarkLocationForUpdateRecord.CountryCodeID))
                {
                    EnrollUtil _util = new EnrollUtil();
                    locationCodes = _util.GetLocationsByCountry(Convert.ToInt32(MarkLocationForUpdateRecord.CountryCodeID))[1].AsEnumerable();
                }
                else if (locations == null || String.IsNullOrEmpty(MarkLocationForUpdateRecord.CountryCodeID))
                    locationCodes = new SelectList(Enumerable.Empty<SelectListItem>());

                return locationCodes;
            }
        }

        /// <summary>
        /// The Action Selected By the User.
        /// </summary>
        [Display(Name = "Action:")]
        [Required]
        public String ActionType { get; set; }

        public String DisplayMessage { get; set; }

        public MarkLocationForUpdateModel MarkLocationForUpdateRecord { get; set; }

        public UserInfo currentUser { get; set; }

        public bool EditMode { get; set; }

        public List<DeclineDetail> Declines { get; set; }



        public User_Session_Defaults user_Session_Defaults;

    }
}