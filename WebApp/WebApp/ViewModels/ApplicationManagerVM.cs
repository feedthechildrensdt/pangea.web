﻿using FTCWebApp.Caching;
using FTCWebApp.Classes;
using FTCWebApp.DataModels;
using Microsoft.ApplicationInsights.Extensibility.Implementation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace FTCWebApp.ViewModels
{
    public class ApplicationManagerVM
    {

        
        public ApplicationManagerVM()
        {

            HttpSessionStateBase sessionBase = new HttpSessionStateWrapper(HttpContext.Current.Session);

            user_Session_Defaults = new User_Session_Defaults
                (
                User_Session_Defaults.enum_Display_Page.ApplicationManager,
                sessionBase
                );
        }
        

        public ApplicationManagerVM
            (
            FTCWebApp.Classes.User_Session_Defaults.enum_Display_Page e_Display_Page,
            HttpSessionStateBase hsb
            )
        {

            user_Session_Defaults = new User_Session_Defaults
                (
                e_Display_Page,
                hsb
                );

        }
        

        public IEnumerable<SelectListItem> TableNames
        {
            get
            {
                return CacheManager.GetFromCache<List<SelectListItem>>("ListofCodeTables");
            }
        }

        [Display(Name = "Name:")]
        [Required]
        public string TableName { get; set; }

        [Display(Name = "Action Type:")]
        [Required]
        public string ActionType { get; set; }

        public IEnumerable<SelectListItem> ActionTypes
        {
            get
            {
                var list = new List<SelectListItem>();
                list.Add(new SelectListItem() { Value = "Add New Code", Text = "Add New Data" });
                list.Add(new SelectListItem() { Value = "Edit Data", Text = "View/Edit Existing Data" });
                return list;
            }
        }

        public IEnumerable<SelectListItem> ActiveValues
        {
            get
            {
                var list = new List<SelectListItem>();
                list.Add(new SelectListItem() { Value = "true", Text = "Yes" });
                list.Add(new SelectListItem() { Value = "false", Text = "No" });
                return list;
            }
        }

        public string DisplayMessage { get; set; }

        public ApplicationManagerModel codeTableRecord { get; set; }

        public List<DeclineDetail> Declines { get; set; }

        public UserInfo currentUser { get; set; }

        
        public User_Session_Defaults user_Session_Defaults;

    }
}