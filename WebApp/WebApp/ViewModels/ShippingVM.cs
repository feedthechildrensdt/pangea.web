﻿using FTCWebApp.Caching;
using FTCWebApp.Classes;
using FTCWebApp.DataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using FTCWebApp.ViewModels.GUI_Pieces;

namespace FTCWebApp.ViewModels
{
    public class ShippingVM
    {

        public ShippingVM()
        {

        }

        public ShippingVM
            (
            FTCWebApp.Classes.User_Session_Defaults.enum_Display_Page e_Display_Page,
            HttpSessionStateBase hsb
            )
        {

            user_Session_Defaults = new User_Session_Defaults
                (
                e_Display_Page,
                hsb
                );

        }


        public UserInfo currentUser { get; set; }

        public List<DeclineDetail> Declines { get; set; }

        public Child child { get; set; }

        private SelectList locationCodes;
        private SelectList locations;

        public IEnumerable<SelectListItem> Countries
        {
            get
            {

                string the_user_id = currentUser.ID.ToString();

                List<SelectListItem> l_sli = GUI_Pieces_Helper.Get_Users_Country_Drop_Down_List_Items(the_user_id);

                return l_sli;

            }
        }

        public SelectList Locations
        {
            get
            {
                if (locations == null)
                {
                    locations = new SelectList(Enumerable.Empty<SelectListItem>());
                }

                return locations;
            }
        }

        public SelectList LocationCodes
        {
            get
            {
                if (locationCodes == null)
                {
                    locationCodes = new SelectList(Enumerable.Empty<SelectListItem>());
                }

                return locationCodes;
            }
        }

        public ShippingDetailsModel Details { get; set; }

        public List<String> childNumbers { get; set; }

        public IEnumerable<SelectListItem> ActionTypes // This method populates  data for Disability Field
        {
            get
            {
                List<SelectListItem> actions = new List<SelectListItem>();
                actions.Add(new SelectListItem { Value = "0", Text = "Recieved" });
                actions.Add(new SelectListItem { Value = "1", Text = "Lost in shippment" });
                actions.Add(new SelectListItem { Value = "2", Text = "Recieved missing documents" });
                return actions;
            }
        }

        [Display(Name = "Recieving Action Type:")]
        public String ActionType { get; set; }

        public String DisplayMessage { get; set; }



        public User_Session_Defaults user_Session_Defaults;

    }
}