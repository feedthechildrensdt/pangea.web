﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using FTCWebApp.DataModels;
using log4net;
using FTCWebApp.Utillities;

namespace FTCWebApp.Validation
{
    //Summary
    //This class is used to perform server side validation for child enrollment forms
    public class EnrollmentValidation
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(EnrollmentValidation));

        public EnrollmentValidation() { }

        public Tuple<string, string> validate(List<FieldRuleModel> fieldRules, string FieldName, string FieldValue, bool ignoreChoreAndFavValidation)
        {
            try
            {
                var matchItem = fieldRules.Where(m => m.FieldName == FieldName).FirstOrDefault();
                int parsedFieldValue;
                bool parsed = (int.TryParse(FieldValue, out parsedFieldValue));
                DateTime parsedDateTime = DateTime.MinValue;
                double AgeinDays = 0;
                if (FieldName == "DateOfBirth" && !string.IsNullOrEmpty(FieldValue))
                {
                    if (FieldValue.Length == 4)
                    {
                        parsedDateTime = new DateTime(Convert.ToInt32(FieldValue), 1, 1).ToUniversalTime();
                        AgeinDays = (DateTime.UtcNow.Date - parsedDateTime).TotalDays;
                    }
                    else
                    {
                        parsedDateTime = Convert.ToDateTime(FieldValue, CultureInfo.InvariantCulture).ToUniversalTime();
                        AgeinDays = (DateTime.UtcNow.Date - parsedDateTime).TotalDays;
                    }
                }
                if (matchItem != null && matchItem.ColumnRules.Count != 0)
                {
                    for (var i = 0; i < matchItem.ColumnRules.Count; i++)
                    {
                        if (matchItem.ColumnRules[i].isRequired == false)
                        {
                            continue;
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(FieldValue))
                            {
                                if (FieldName == "FirstName")
                                    return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("First Name is missing."));
                                if (FieldName == "LastName")
                                    return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Last Name is missing."));
                                if (FieldName == "MiddleName")
                                    return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Middle Name is missing."));
                                if (FieldName == "DateOfBirth")
                                    return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Date of Birth is missing."));
                                if (FieldName == "OtherNameGoesBy")
                                    return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Other Name is missing."));
                                if (FieldName == "GenderCodeID")
                                    return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Sex of the child is missing."));
                                if (FieldName == "LocationCodeID")
                                    return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Location is missing."));
                                if (FieldName == "DisabilityStatusCodeID")
                                    return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Disability of the child is missing."));
                                if (FieldName == "GradeLevelCodeID")
                                    return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Grade Level is missing."));
                                if (FieldName == "NumberOfBrothers")
                                    return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Number of Brothers is missing."));
                                if (FieldName == "NumberOfSisters")
                                    return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Number of Sisters is missing."));
                                if (FieldName == "PersonalityTypeCodeID")
                                    return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Personality type is missing."));
                                if (FieldName == "LivesWithCodeID")
                                    return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Lives With is missing."));
                                if (FieldName == "FavoriteLearningCodeID")
                                    return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Favorite Learning is missing."));
                                if (FieldName == "ChoreCodeID" && !ignoreChoreAndFavValidation)
                                    return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Must have at least 1 Chore to enroll a child."));
                                if (FieldName == "FavoriteActivityCodeID" && !ignoreChoreAndFavValidation)
                                    return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Must have at least 1 Favorite activity to enroll a child."));
                                if(FieldName == "MajorLifeEvent")
                                    return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Major Life Event is missing."));
                            }
                            else if (FieldValue != null && (parsedFieldValue != 0 || parsedDateTime != DateTime.MinValue))
                            {
                                if (matchItem.ColumnRules[i].validationType == "RangeValidation")
                                {
                                    if ((string.IsNullOrEmpty(matchItem.ColumnRules[i].minValue)) && (!string.IsNullOrEmpty(matchItem.ColumnRules[i].maxValue)))
                                    {
                                        if (FieldName == "DateOfBirth")
                                        {
                                            if (AgeinDays > Convert.ToDouble(matchItem.ColumnRules[i].maxValue))
                                            {
                                                return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Child is too old for Enrollment. Max age for Enrollment is " + (Convert.ToInt32(matchItem.ColumnRules[i].maxValue) / 365) + " years old"));
                                            }
                                        }
                                        else
                                        {
                                            if (parsedFieldValue > Convert.ToInt32(matchItem.ColumnRules[i].maxValue))
                                            {
                                                return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Entered value for " + FieldName + " is greater than the Maximum value"));
                                            }
                                        }
                                    }
                                    else if ((!string.IsNullOrEmpty(matchItem.ColumnRules[i].minValue)) && (string.IsNullOrEmpty(matchItem.ColumnRules[i].maxValue)))
                                    {
                                        if (FieldName == "DateOfBirth")
                                        {
                                            if (AgeinDays < Convert.ToDouble(matchItem.ColumnRules[i].minValue))
                                            {
                                                return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Child is too young for Enrollment. Min age for Enrollment is " + (Convert.ToInt32(matchItem.ColumnRules[i].minValue)) + " days old"));
                                            }
                                        }
                                        else
                                        {
                                            if (parsedFieldValue < Convert.ToInt32(matchItem.ColumnRules[i].minValue))
                                            {
                                                return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Entered value for " + FieldName + " is lesser than the Minimum value"));
                                            }
                                        }
                                    }
                                    else if ((!string.IsNullOrEmpty(matchItem.ColumnRules[i].minValue)) && (!string.IsNullOrEmpty(matchItem.ColumnRules[i].maxValue)))
                                    {
                                        if (FieldName == "DateOfBirth")
                                        {
                                            if (AgeinDays < Convert.ToDouble(matchItem.ColumnRules[i].minValue))
                                            {
                                                return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Child is too young for Enrollment. Minimum age for Enrollment is " + (Convert.ToInt32(matchItem.ColumnRules[i].minValue)) + " days old"));
                                            }
                                            else if (AgeinDays > Convert.ToDouble(matchItem.ColumnRules[i].maxValue))
                                            {
                                                return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Child is too old for Enrollment. Maximum age for Enrollment is " + (Convert.ToInt32(matchItem.ColumnRules[i].maxValue) / 365) + " years old"));
                                            }
                                        }
                                        else
                                        {
                                            if (parsedFieldValue > Convert.ToInt32(matchItem.ColumnRules[i].maxValue))
                                            {
                                                return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Entered value for " + FieldName + " is greater than the Maximum value: " + matchItem.ColumnRules[i].maxValue));
                                            }
                                            else if (parsedFieldValue < Convert.ToInt32(matchItem.ColumnRules[i].minValue))
                                            {
                                                return Tuple.Create(FieldName, Helper.MessageWithDateTimeAdded("Entered value for " + FieldName + " is lesser than the Minimum value: " + matchItem.ColumnRules[i].minValue));
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }
                }
                else
                {
                    return null;
                }
                return null;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to validate the form input", ex);
                return null;
            }
        }
    }
}