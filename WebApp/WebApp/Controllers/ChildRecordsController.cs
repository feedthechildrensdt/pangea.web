﻿using FTCWebApp.Caching;
using FTCWebApp.Classes;
using FTCWebApp.DataModels;
using FTCWebApp.ServiceReference1;
using FTCWebApp.ViewModels;
using FTCWebApp.ViewModels.GUI_Pieces;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FTCWebApp.Controllers
{
    public class ChildRecordsController : Controller
    {

        // private FieldServicesClient _svc = new FieldServicesClient("BasicHttpsBinding_IFieldServices");

        private DAL.API_EndPointer api_endpointer = new DAL.API_EndPointer();

        // private FieldServicesClient _svc = new FieldServicesClient("BasicHttpBinding_IFieldServices");
        private static readonly ILog log = LogManager.GetLogger(typeof(ChildRecordsController));

        // GET: ChildRecords
        public ActionResult Index()
        {
            UserInfo uInfo = SessionManager.Get<UserInfo>("UserInfo");
            if (uInfo == null)
            {
                return RedirectToAction("SessionReset", "Home");
            }

            ChildRecordsVM model = new ChildRecordsVM(Classes.User_Session_Defaults.enum_Display_Page.Child_Records, Session);
            model.currentUser = uInfo;

            return View("ChildRecords", model);
        }

        //Summary
        //Get the list of child records pending for Update 
        [HttpPost]
        public string getChildRecordData(string countryID, string locationID)
        {
            try
            {
                UserInfo _currentUser = SessionManager.Get<UserInfo>("UserInfo");

                if (countryID.ToLower() == "null")
                    countryID = string.Empty;

                if (locationID.ToLower() == "null")
                    locationID = string.Empty;


                User_Session_Defaults.Set_User_Preferred_Location_Code_ID_Session_Var(Session, locationID);

                User_Session_Defaults.Set_User_Preferred_Country_Session_Var(Session, countryID);



                int _countryID = String.IsNullOrEmpty(countryID) ? 0 : Convert.ToInt32(countryID);
                int _locationID = String.IsNullOrEmpty(locationID) ? 0 : Convert.ToInt32(locationID);

                var Incompleteupdates = new ListChild[2];
                Incompleteupdates = api_endpointer.svc.GetChildRecords(_currentUser.ID.ToString(), _countryID, _locationID);
                return JsonConvert.SerializeObject(Incompleteupdates);
            }
            catch (Exception ex)
            {
                log.Debug("Failed to get the Child Records!", ex);
                return null;
            }
        }


        /*
        [HttpPost]
        public string getUserCountryDatum(string UserID)
        {

            string UserCountryDatum = "";

            try
            {

                Country_DropDown_Item[] country_select_items = api_endpointer.svc.GetCountryRecords(UserID);

                UserCountryDatum = JsonConvert.SerializeObject(country_select_items);

            }
            catch (Exception ex)
            {

                UserCountryDatum = JsonConvert.SerializeObject(ex);

            }

            return UserCountryDatum;

        }
        */


    }
}