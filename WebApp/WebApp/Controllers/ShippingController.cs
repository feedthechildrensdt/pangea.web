﻿using FTCWebApp.Caching;
using FTCWebApp.Classes;
using FTCWebApp.DataModels;
using FTCWebApp.ServiceReference1;
using FTCWebApp.Utillities;
using FTCWebApp.ViewModels;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FTCWebApp.Controllers
{
    public class ShippingController : Controller
    {
        private FieldServicesClient _svc = new FieldServicesClient("BasicHttpsBinding_IFieldServices");
        private static readonly ILog log = LogManager.GetLogger(typeof(ShippingController));

        public ActionResult ShippingCreation()
        {

            var shippingCreation = new ShippingVM(Classes.User_Session_Defaults.enum_Display_Page.Shipping_Shipping_Creation, Session);

            shippingCreation.currentUser = SessionManager.Get<UserInfo>("UserInfo");
            shippingCreation.Declines = new List<DeclineDetail>();
            SessionManager.Set<UserInfo>("UserInfo", shippingCreation.currentUser);
            return View("ShippingCreation", shippingCreation);
        }

        public ActionResult ShippingCreationDetails(FormCollection data)
        {

            var shippingCreation = new ShippingVM(Classes.User_Session_Defaults.enum_Display_Page.Shipping_Shipping_Creation, Session);
            
            shippingCreation.currentUser = SessionManager.Get<UserInfo>("UserInfo");
            shippingCreation.Declines = new List<DeclineDetail>();
            SessionManager.Set<UserInfo>("UserInfo", shippingCreation.currentUser);
            return View("ShippingCreationDetails", shippingCreation);
        }

        public ActionResult ShippingRecieving()
        {

            var shippingRecieving = new ShippingVM(Classes.User_Session_Defaults.enum_Display_Page.Shipping_Shipping_Recieving, Session);

            shippingRecieving.currentUser = SessionManager.Get<UserInfo>("UserInfo");
            shippingRecieving.Declines = new List<DeclineDetail>();
            SessionManager.Set<UserInfo>("UserInfo", shippingRecieving.currentUser);
            ViewBag.Module = "ShippingRecieving";
            return View("ShippingRecieving", shippingRecieving);
        }

        //Summary
        //Get the list of child records with documents ready for shipment
        [HttpPost]
        public string getChildrenReadyForShipping(string countryID, string locationID)
        {
            try
            {  
                
                // SET USER DEFAULT VALUES
                User_Session_Defaults.Set_User_Preferred_Country_Session_Var(Session, countryID);

                User_Session_Defaults.Set_User_Preferred_Location_Code_ID_Session_Var(Session, locationID);

                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                var Incompleteupdates = new ListChild[2];
                Incompleteupdates = _svc.GetIncompleteUpdates(uInfo.ToString(), Helper.ParseInt(countryID), Helper.ParseInt(locationID), 2);
                return JsonConvert.SerializeObject(Incompleteupdates);
            }
            catch (Exception ex)
            {
                log.Debug("Failed to get the Incomplete child updates !", ex);
                return null;
            }
        }

        //Summary
        //Get the list of shipments from the DB
        [HttpPost]
        public string getShipments(string countryID, string trackingNumber)
        {
            try
            {

                // SET USER DEFAULT VALUE
                User_Session_Defaults.Set_User_Preferred_Country_Session_Var(Session, countryID);
                
                ShippingDetailsModel data1 = new ShippingDetailsModel { ShipmentID = "529D000B-68B7-E811-80EB-00155D651737", TrackingNumber = "195381-22-341", ShippedVia = "UPS", ShippedFromCountry ="Kenya", ShippedOnDate ="10/12/2018", ShippedBy = "Sandra Jones" , ShipmentWeight ="118Kgs", Currency = "Kenyan shilling", ShippingCost ="400" };
                ShippingDetailsModel data2 = new ShippingDetailsModel { ShipmentID = "0e5549e2-3b81-e811-8a6e-b8ca3abcf1fa", TrackingNumber = "172323", ShippedVia = "DHL", ShippedFromCountry ="Haiti", ShippedOnDate = "05/23/2018", ShippedBy ="Tanya", ShipmentWeight ="214 lbs", Currency= "Haitian gourde", ShippingCost ="234565.32"  };
                var ShippedList = new List<ShippingDetailsModel>() { data1, data2 };

                return JsonConvert.SerializeObject(ShippedList);
            }
            catch (Exception ex)
            {
                log.Debug("Failed to get the shipments !", ex);
                return null;
            }
        }

        public ActionResult ShippingRecievingDetails(FormCollection data)
        {
            var shippingCreation = new ShippingVM();
            shippingCreation.currentUser = SessionManager.Get<UserInfo>("UserInfo");
            shippingCreation.Declines = new List<DeclineDetail>();
            SessionManager.Set<UserInfo>("UserInfo", shippingCreation.currentUser);
            ViewBag.Module = "ShippingRecieving";
            return View("ShippingRecievingDetails", shippingCreation);
        }
    }
}