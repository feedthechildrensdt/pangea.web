﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Mvc;
using FTCWebApp.DataModels;
using FTCWebApp.ViewModels;
using FTCWebApp.Caching;
using System.Collections.Specialized;
using FTCWebApp.ServiceReference1;
using FTCWebApp.Validation;
using FTCWebApp.Utillities;
using Newtonsoft.Json;
using log4net;
using System.Drawing;


using FTCWebApp.Classes;


namespace FTCWebApp.Controllers
{
    public class PanMainController : Controller
    {
        private FieldServicesClient _svc;
        private EnrollmentValidation _val;
        private EnrollUtil _util;
        private static readonly ILog log = LogManager.GetLogger(typeof(PanMainController));

        public PanMainController()
        {
            _svc = new FieldServicesClient("BasicHttpsBinding_IFieldServices");
            _val = new EnrollmentValidation();
            _util = new EnrollUtil();
        }

        // Controller for Enrollment Page view 
        public ActionResult Enrollment(string user_info)
        {
            UserInfo uInfo = SessionManager.Get<UserInfo>("UserInfo");
            if (uInfo == null)
            {
                return RedirectToAction("SessionReset", "Home");
            }

            var enrollment = new CommonVM();
            enrollment.currentUser = uInfo;
            enrollment.Declines = new List<DeclineDetails>();

            //Remove all previous user files or data stored in Session
            Session.RemoveAll();
            SessionManager.Set<UserInfo>("UserInfo", enrollment.currentUser);

            ViewBag.Module = "Enrollment";

            enrollment.child = new Child();
            enrollment.Declines = new List<DeclineDetails>();
            enrollment.child.NumberOfBrothers = enrollment.child.NumberOfSisters = 0;

            log.Debug("Leaving Enrollment");
            return View(enrollment);
        }

        public ActionResult EnrollmentWorklist()
        {
            
            UserInfo uInfo = SessionManager.Get<UserInfo>("UserInfo");
            if (uInfo == null)
            {
                return RedirectToAction("SessionReset", "Home");
            }

            var model = new CommonVM(User_Session_Defaults.enum_Display_Page.Enrollment, Session);

            model.currentUser = uInfo;
            model.Declines = new List<DeclineDetails>();
            
            Dictionary<string, object> dct_User_Defaults = Classes.User_Session_Defaults.Get_User_Defaults(Session);
            
            //Remove all previous user files or data stored in Session
            Session.RemoveAll();
            SessionManager.Set<UserInfo>("UserInfo", model.currentUser);

            model.user_Session_Defaults.Add_Dictionary_Items_To_Session(Session, dct_User_Defaults);


            ViewBag.Module = "Enrollment";

            log.Debug("Leaving Enrollment Worklist");
            return View(model);
        }

        //Summary
        //Get the list of child records pending for Update 
        [HttpPost]
        public string getEnrollmentWorklist(string countryID, string locationID, string actionTypeID)
        {
            try
            {

                Set_Enrollment_Page_User_Defaults(countryID, locationID, actionTypeID);


                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                var IncompleteEnrollments = new ListChild[2];

                //if action id is create bring every record from the enrollment table
                IncompleteEnrollments = _svc.GetIncompleteEnrollments(uInfo.ToString(), Helper.ParseInt(countryID), Helper.ParseInt(locationID), 0);
                var GenderValues = CacheManager.GetFromCache<List<SelectListItem>>("Gender");
                foreach (var record in IncompleteEnrollments)
                {
                    var code = GenderValues.FirstOrDefault(a => a.Value == record.Gender);
                    if (code != null)
                    {
                        record.Gender = code.Text;
                        if (!String.IsNullOrEmpty(record.Gender))
                        {
                            record.Gender = record.Gender.Substring(0, 1);
                        }
                    }
                }
                return JsonConvert.SerializeObject(IncompleteEnrollments);
            }
            catch (Exception ex)
            {
                log.Debug("Failed to get the Incomplete child updates !", ex);
                return null;
            }
        }


        private void Set_Enrollment_Page_User_Defaults(string countryID, string locationID, string actionTypeID)
        {

            // SET USER DEFAULT VALUES
            Classes.User_Session_Defaults.Set_User_Preferred_Country_Session_Var(Session, countryID);

            Classes.User_Session_Defaults.Set_User_Preferred_Location_Code_ID_Session_Var(Session, locationID);

            Classes.User_Session_Defaults.Set_User_Preferred_Enrollment_Action_Type_Session_Var(Session, actionTypeID);

        }


        public ActionResult LoadEnrollmentChild(string id)
        {
            ViewBag.Module = "Enrollment";
            var model = _util.setupIncompleteEnrollmentView(id);
            return View("Enrollment", model);
        }

        //Summary
        //Get the Location Names and Location Codes upon selection of Country by the user
        public JsonResult GetLocationsByCountry(int id)
        {
            List<SelectListItem>[] LocationListArray = _util.GetLocationsByCountry(id);

            if (LocationListArray != null)
            {
                var LocationNames = LocationListArray.AsEnumerable();
                return Json(LocationNames, JsonRequestBehavior.AllowGet);
            }
            else
                return null;
        }

        //Get the decline sub reason for a selected workflow/ decline type and if translation is required for the step
        public string GetWorkflowStepDetails(string id)
        {
            try
            {
                var DeclineSubReasonList = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Decline_SubReason"));
                var WorkflowStepList = CacheManager.GetFromCache<List<WorkflowStepModel>>("Step").ToList();

                // list of subcategory for a given category of decline 
                var SubDeclines = DeclineSubReasonList.Where(a => a.Item2 == id);
                var DeclineSubReason = new List<SelectListItem>();
                var TranslationDefault = WorkflowStepList.FirstOrDefault(a => a.Description == id).TranslationRequired;

                foreach (var item in SubDeclines)
                {
                    DeclineSubReason.Add(new SelectListItem { Value = item.Item3, Text = item.Item3 });
                }

                var Declines = DeclineSubReason.AsEnumerable();
                var data = new { Success = true, Declines = Declines, TranslationDefault = TranslationDefault };
                return JsonConvert.SerializeObject(data);
            }
            catch (Exception ex)
            {
                log.Debug("Failed to get the Decline sub categories !", ex);
                return null;
            }
        }

        //Summary
        //Populate some of the Enrollment Information dropdown options dynamically depending on the data of birth set by the user
        public JsonResult GetFieldData(string age)
        {
            try
            {
                if (!string.IsNullOrEmpty(age))
                {
                    int Age = Convert.ToInt32(age);
                    var Fieldset = _util.GetFieldData(Age).AsEnumerable();
                    return Json(Fieldset, JsonRequestBehavior.AllowGet);
                }
                else
                { return null; }
            }
            catch (Exception ex)
            {
                log.Debug("Failed to get the field data for the selected age !", ex);
                return null;
            }
        }

        //Summary
        //This method takes an enrollment file and stores in the session
        [HttpPost]
        public string UploadAFile(HttpPostedFileBase file, string ChildID, string ContentTypeID, string ImgCropArea_X1, string ImgCropArea_Y1, string ImgCropArea_Width, string ImgCropArea_Height, string Module = null)
        {
            try
            {

                var fileUpload_ready = _util.AddFileData(file, ChildID, ContentTypeID, ImgCropArea_X1, ImgCropArea_Y1, ImgCropArea_Width, ImgCropArea_Height);

                object data = null;

                data = _util.processUploadFile(file, ChildID, ContentTypeID, fileUpload_ready);

                return JsonConvert.SerializeObject(data);
            }
            catch (Exception ex)
            {
                log.Debug("Failed to upload the file !", ex);
                return null;
            }
        }

        //This method is used to pass the Translated MLE to the API and DB
        public string GetMLETranslationImage(FormCollection formData)
        {
            try
            {
                UploadFile file = new UploadFile();

                var FilePath = System.Web.HttpContext.Current.Server.MapPath("~/Content/doc.jpg");
                Image image = Image.FromFile(FilePath);
                EnrollUtil util = new EnrollUtil();
                file.FileBytes = (byte[])(new ImageConverter()).ConvertTo(image.Clone(), typeof(byte[]));
                file.FileName = "Major Life Event";
                //Image image = null;
                //using (MemoryStream mStream = new MemoryStream(file.FileBytes))
                //{
                //    image = Image.FromStream(mStream);
                //}
                var bytestring = util.createThumbnail(image);
                var data = new { Success = true, Image = bytestring, contenttype = "MLE", Id = "MLETranslation", Name = file.FileName, Message = Helper.MessageWithDateTimeAdded("MLE Translation has been uploaded!") };

                return JsonConvert.SerializeObject(data);
            }
            catch (Exception ex)
            {
                log.Debug("Failed to submit the translated MLE !", ex);
                return null;
            }
        }

        //Summary
        //This method opens an attachment file from the session
        [HttpPost]
        public string OpenAFile(string childID, string ContentTypeID, string Key)
        {
            try
            {
                if (!string.IsNullOrEmpty(childID) && !string.IsNullOrEmpty(ContentTypeID) && !string.IsNullOrEmpty(Key))
                {
                    if (SessionManager.Get<UploadFile>(Key) != null)
                    {
                        var file = SessionManager.Get<UploadFile>(Key);
                        var data = new { Success = true, Name = file.FileName, File = file };
                        return JsonConvert.SerializeObject(data);
                    }
                    else
                    {
                        var data = new { Success = false, Message = Helper.MessageWithDateTimeAdded("Fail ! Could not retrieve the file from the system !") };
                        return JsonConvert.SerializeObject(data);
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to open the selected file ", ex);
                return null;
            }
        }

        public ActionResult DownloadLetter(string Key)
        {
            var file = SessionManager.Get<UploadFile>(Key);

            return File(file.FileBytes, "application/vnd.openxmlformats-officedocument.wordprocessingml.document", file.FileName);
        }

        //Summary
        //This method removes an enrollment file from the session and database using the Session Key provided and the module
        [HttpPost]
        public JsonResult RemoveAFile(string childID, string ContentTypeID, string Key, string Module = null)
        {
            try
            {
                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                List<ReturnMessage> message = new List<ReturnMessage>();

                if (!string.IsNullOrEmpty(childID) && !string.IsNullOrEmpty(ContentTypeID) && !string.IsNullOrEmpty(Key))
                {
                    UploadFile upload = new UploadFile();
                    upload.ChildID = childID;
                    if (!ContentTypeID.Contains("Translation"))
                    {
                        upload.ContentTypeID = Convert.ToInt32(ContentTypeID);
                    }
                    upload.UserID = uInfo.ToString();

                    if (SessionManager.Get<UploadFile>(Key) != null)
                    {
                        var file = SessionManager.Get<UploadFile>(Key);
                        upload.FileStreamID = file.FileStreamID;
                        SessionManager.Remove<UploadFile>(Key);
                        if (Module == "Enrollment")
                        {
                            message = _svc.DeleteAnEnrollmentFile(upload).ToList();
                        }
                        if (Module == "Update")
                        {
                            message = _svc.DeleteAnUpdateFile(upload).ToList();
                        }
                        if (message.Count() >= 1 && message[0].Code == "0")
                        {
                            var data = new { Success = true, Id = ContentTypeID, Message = Helper.MessageWithDateTimeAdded(message[0].Description) };
                            return Json(data);
                        }
                        else
                        {
                            var data = new { Success = true, Id = ContentTypeID, Message = Helper.MessageWithDateTimeAdded("File deleted from the session but could not be removed from the database !") };
                            return Json(data);
                        }
                    }
                    else
                    {

                        if (Module == "Enrollment")
                        {
                            message = _svc.DeleteAnEnrollmentFile(upload).ToList();
                        }
                        if (Module == "Update")
                        {
                            message = _svc.DeleteAnUpdateFile(upload).ToList();
                        }

                        if (message.Count() >= 1 && message[0].Code == "0")
                        {
                            var data = new { Success = true, Id = ContentTypeID, Message = Helper.MessageWithDateTimeAdded(message[0].Description) };
                            return Json(data);
                        }
                        else
                        {
                            var data = new { Success = false, Id = ContentTypeID, Message = Helper.MessageWithDateTimeAdded("Fail ! Could not remove the file from the database !") };
                            return Json(data);
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to remove the selected file !", ex);
                return null;
            }
        }

        // GET: PanMain/Create
        public ActionResult Create()
        {
            return View();
        }

        //Summary
        //This action creates a child record with basic information provided by the user
        // POST: PanMain/Create
        [HttpPost]
        public ActionResult Create(FormCollection formData)
        {
            var data = createAChild(formData);
            return Json(data);
        }

        //Summary
        //This action saves the progress made by the user on the enrollment tab which contains additional information of the child.
        // POST: PanMain/Save
        [HttpPost]
        public ActionResult Save(FormCollection formData)
        {
            try
            {
                dynamic CreateResponse = null;
                var files_uploaded = new ReturnMessage[10];
                if (formData["button.id"] == "child_saveDeclinedChildInfo" || formData["button.id"] == "enroll_saveDeclinedEnrollmentInfo")
                {
                    // validate all form fields 
                    //save only declined child info
                    //Save from child info screen on declined, 
                    //check and see if the enrollment info is also declined 
                    //check and see if only child info is declined and not enrollment 
                    //if (formData["declines.IsDeclinedChildInfo"] == "1")
                    //{
                    CreateResponse = saveADeclinedChild(formData);
                    // To DO add these results in a list to the session and make sure before completing decline this list is empty
                    var declineSaveErrors = CreateResponse.validationResult;
                    List<string> errorMessages = new List<string>();
                    foreach (var item in declineSaveErrors)
                    {
                        errorMessages.Add(item.Value);
                    }
                    SessionManager.Set<List<string>>("DeclineSaveErrors", errorMessages);

                    // }
                    //save only declined enrollment info
                    //if (formData["declines.IsDeclinedEnrollmentInfo"] == "1")
                    //{
                    //    CreateResponse = saveADeclinedChild(formData);
                    //}
                }
                else
                {
                    CreateResponse = createAChild(formData);
                }
                if (CreateResponse.Success == true)
                {
                    //if it is an enrollment in progress get the id from the form and after save and pass it back to the view 
                    if (!String.IsNullOrEmpty(formData["child.ChildID"]) && CreateResponse.ID == formData["child.ChildID"])
                    {
                        var data = new { Success = CreateResponse.Success, validationResult = CreateResponse.validationResult, ID = CreateResponse.ID, FileValidation = CreateResponse.fileValidationResult, Message = CreateResponse.Message, duplicates = CreateResponse.duplicates };
                        return Json(data);
                    }
                    else
                    {
                        var data = new { Success = CreateResponse.Success, validationResult = CreateResponse.validationResult, FileValidation = CreateResponse.fileValidationResult, Message = CreateResponse.Message, duplicates = CreateResponse.duplicates };
                        return Json(data);
                    }
                }
                else
                {
                    var declineSaveErrors = CreateResponse.Message;

                    SessionManager.Set<List<string>>("DeclineSaveErrors", new List<string> { declineSaveErrors });

                    var data = new { Success = CreateResponse.Success, validationResult = CreateResponse.validationResult, FileValidation = CreateResponse.fileValidationResult, Message = CreateResponse.Message };
                    return Json(data);
                }
            }
            catch (Exception ex)
            {
                log.Debug("Something went wrong in saving the child enrollment !", ex);
                return null;
            }
        }

        //Summary
        //This action saves all the child enrollment information and enables the 'Enrollment_Ready' flag.
        [HttpPost]
        public ActionResult Submit(FormCollection formData)
        {
            try
            {
                dynamic CreateResponse = null;
                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                var files_uploaded = new ReturnMessage[10];
                var count = 0;
                var req_docs = (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).Where(m => m.Required == true).Select(c => new { ContentType = "ContentType_" + c.Content_Type_Code_ID.ToString(), ContentName = c.Description }).ToList();
                var all_docs = (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).Select(c => new { ContentType = "ContentType_" + c.Content_Type_Code_ID.ToString(), ContentName = c.Description }).ToList();
                var req_Declined_docs = SessionManager.Get<List<SelectListItem>>("DeclinedContentTypes");

                //if (formData["button.id"] == "child_saveDeclinedChildInfo" || formData["id"] == "child_submitDeclinedChildInfo" || formData["id"] == "enroll_saveDeclinedEnrollmentInfo" || formData["id"] == "enroll_submitDeclinedEnrollmentInfo")
                //{
                if (formData["id"] == "enroll_submitDeclinedEnrollmentInfo")
                {
                    CreateResponse = saveADeclinedChild(formData);
                    if (CreateResponse.Success == true)
                    {
                        foreach (var item in req_Declined_docs)
                        {//Make sure all the required or declined documents are added before submiting

                            if (CreateResponse.fileValidationResult.ContainsKey(item.Text))
                            {
                                count += 1;
                            }
                            if (count == 0 && CreateResponse.validationResult.Count == 0)
                            {
                                string childID = CreateResponse.ID;
                                var submit_result = _svc.SubmitUpdate(childID, uInfo.ToString());
                                if (submit_result.Count() >= 1 && submit_result[0].Message == "Child_Number")
                                {
                                    //Clearing the current session keyvalue collection
                                    Session.RemoveAll();
                                    SessionManager.Set<UserInfo>("UserInfo", uInfo);
                                    var data = new { Success = true, ID = childID, Number = submit_result[0].Code, Message = Helper.MessageWithDateTimeAdded(submit_result[0].Description), validationResult = CreateResponse.fileValidationResult };
                                    return Json(data);
                                }
                                else
                                {
                                    var data = new { Success = false, validationResult = CreateResponse.fileValidationResult, Message = Helper.MessageWithDateTimeAdded("Something went wrong in submitting the enrollment !") };
                                    return Json(data);
                                }
                            }
                            else
                            {
                                var data = new { Success = false, validationResult = CreateResponse.validationResult, FileValidation = CreateResponse.fileValidationResult, Message = Helper.MessageWithDateTimeAdded("Child Enrollment progress has been saved !") };
                                return Json(data);
                            }
                        }
                    }
                    else
                    {
                        var data = new { Success = false, validationResult = CreateResponse.validationResult, FileValidation = CreateResponse.fileValidationResult };
                        return Json(data);
                    }
                }
                else
                {
                    CreateResponse = createAChild(formData);

                    if (CreateResponse.Success == true)
                    {
                        foreach (var item in all_docs)
                        {
                            if (req_docs.Contains(item))
                            {
                                if (CreateResponse.fileValidationResult.ContainsKey(item.ContentName))
                                {
                                    count += 1;
                                }
                            }
                        }
                        if (count == 0 && CreateResponse.validationResult.Count == 0)
                        {
                            string childID = CreateResponse.ID;
                            var submit_result = _svc.EnrollAChild(childID, uInfo.ToString());
                            if (submit_result.Count() >= 1 && submit_result[0].Message == "Child_Number")
                            {
                                // Need to update the workflow here to be QC_Approve_Action
                                int pendingChildID = 0;
                                var QC_List_Dataset = _svc.GetWorkFlows(0, 0, 0, uInfo.ToString(), "");
                                foreach (DataRow row in QC_List_Dataset.Tables[0].Rows)
                                {
                                    if (childID.Equals(Convert.ToString(row["Child_GUID"])))
                                    {
                                        pendingChildID = Convert.ToInt32(row["Pending_Child_ID"]);
                                        break;
                                    }
                                }

                                if (pendingChildID != 0)
                                {
                                    WorkFlowStep[] _chWrkFlowSteps = _svc.GetChildWorkFlow(pendingChildID, 1, uInfo.ToString(), false);
                                    foreach (WorkFlowStep _step in _chWrkFlowSteps)
                                    {
                                        _step.QCApprover = false;
                                        _step.Complete = true;
                                        _svc.SaveWorkFlow(_step, uInfo.ToString());
                                    }
                                }

                                //Clearing the current session keyvalue collection
                                Session.RemoveAll();
                                SessionManager.Set<UserInfo>("UserInfo", uInfo);

                                var data = new { Success = true, ID = childID, Number = submit_result[0].Code, Message = Helper.MessageWithDateTimeAdded(submit_result[0].Description), validationResult = CreateResponse.fileValidationResult };
                                return Json(data);
                            }
                            else
                            {
                                var data = new { Success = false, validationResult = CreateResponse.fileValidationResult, Message = Helper.MessageWithDateTimeAdded("Something went wrong in submitting the enrollment !") };
                                return Json(data);
                            }
                        }
                        else
                        {
                            var data = new { Success = false, validationResult = CreateResponse.validationResult, FileValidation = CreateResponse.fileValidationResult, Message = Helper.MessageWithDateTimeAdded("Child Enrollment progress has been saved !") };
                            return Json(data);
                        }
                    }
                    else
                    {
                        var data = new { Success = false, validationResult = CreateResponse.validationResult, FileValidation = CreateResponse.fileValidationResult };
                        return Json(data);
                    }
                }

                var data1 = new { Success = false, validationResult = CreateResponse.validationResult, FileValidation = CreateResponse.fileValidationResult };
                return Json(data1);
            }
            catch (Exception ex)
            {
                log.Debug("Something went wrong in submitting the child enrollment !", ex);
                return null;
            }
        }

        //Summary
        //This method extracts child information from formdata, validates the child information and returns the child information along with the validation results to the calling method.
        [NonAction]
        private Dictionary<string, string>[] validationResult(FormCollection formData, string module, bool ignoreChoreAndFavValidation = false)
        {
            try
            {
                Dictionary<string, string>[] resultSet = new Dictionary<string, string>[2];
                List<FieldRuleModel> FieldRules = CacheManager.GetFromCache<List<FieldRuleModel>>("Column_Rules").Select(c => new FieldRuleModel { FieldName = c.FieldName, ColumnRules = c.ColumnRules.FindAll(m => m.ModuleName == module) }).ToList();
                Dictionary<string, string> childEnrollInfo = new Dictionary<string, string>();
                Dictionary<string, string> validationResults = new Dictionary<string, string>();
                Tuple<string, string> result = null;
                char mychar = '.';
                NameValueCollection nvc = Request.Form;
                foreach (var item in Request.Form.AllKeys)
                {
                    var FieldName = item.Split(mychar)[1];
                    childEnrollInfo.Add(FieldName, nvc[item]);
                    if (FieldName != "ChildID")
                    {
                        result = _val.validate(FieldRules, FieldName, nvc[item], ignoreChoreAndFavValidation);
                        if (result != null)
                        {
                            validationResults.Add(result.Item1, result.Item2);
                        }
                    }
                }
                resultSet[0] = childEnrollInfo;
                resultSet[1] = validationResults;
                return resultSet;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to create the validation result !", ex);
                return null;
            }
        }

        //Summary
        //This method is creates a child record in the database when call upon.
        [NonAction]
        private object createAChild(FormCollection formData)
        {
            try
            {
                //var currentProcess = formData["button.id"];
                //var DeclinedChildInfo = formData["declines.IsDeclinedChildInfo"];
                //var DeclinedEnrollmentInfo = formData["declines.IsDeclinedEnrollmentInfo"];
                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                var resultSet = validationResult(formData, "Enrollment");
                var childEnrollInfo = resultSet[0];
                var validationResults = resultSet[1];
                int count = 0;
                var files_uploaded = new ReturnMessage[10];
                Dictionary<string, string> filevalidationResults = new Dictionary<string, string>();
                var req_docs = (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).Where(m => m.Required == true).Select(c => new { ContentType = "ContentType_" + c.Content_Type_Code_ID.ToString(), ContentName = c.Description }).ToList();
                var all_docs = (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).Select(c => new { ContentType = "ContentType_" + c.Content_Type_Code_ID.ToString(), ContentName = c.Description }).ToList();

                //TODo: Make sure this code is needed, if create child is never called on decline process then this could should be removed
                //var req_Declined_docs = SessionManager.Get<List<SelectListItem>>("DeclinedContentTypes");

                //if the current process is working on decline then validate for the documents that are needed to fix the decline
                //if (currentProcess == "child_saveDeclinedChildInfo" || currentProcess == "child_submitDeclinedChildInfo" || currentProcess == "enroll_saveDeclinedEnrollmentInfo" || currentProcess == "enroll_submitDeclinedEnrollmentInfo")
                //{
                //    foreach (var item in req_Declined_docs)
                //    {
                //        //do not show validation messages for already uploaded
                //        if ((SessionManager.Get<UploadFile>("ContentType_" + item.Value)) == null)
                //        {
                //            filevalidationResults.Add(item.Text, Helper.MessageWithDateTimeAdded(item.Text + " is missing"));
                //        }
                //    }
                //}
                //else
                //{
                foreach (var item in req_docs)
                {
                    // do not add missing file for already uploaded files and files currently saved
                    if ((SessionManager.Get<UploadFile>(item.ContentType)) == null && (SessionManager.Get<UploadFile>(item.ContentType + "_AlreadyUploaded")) == null)
                    {
                        filevalidationResults.Add(item.ContentName, Helper.MessageWithDateTimeAdded(item.ContentName + " is missing"));
                    }
                }
                //}
                //Summary
                //Since only the child form fields are submitted on child record creation, making sure that child form fields are not in the validation result list.
                foreach (var item in validationResults)
                {
                    if (item.Key == "FirstName" || item.Key == "LastName" || item.Key == "LocationCodeID" || item.Key == "GenderCodeID" || item.Key == "DateOfBirth" || item.Key == "MiddleName" || item.Key == "OtherNameGoesBy")
                    {
                        count += 1;
                    }
                }
                if (count == 0)
                {
                    var child = _util.AddChildObject(childEnrollInfo);
                    //if (currentProcess == "child_saveDeclinedChildInfo" || currentProcess == "child_submitDeclinedChildInfo" || currentProcess == "enroll_saveDeclinedEnrollmentInfo" || currentProcess == "enroll_submitDeclinedEnrollmentInfo")
                    //{
                    //    var message = _svc.SaveUpdate(child, uInfo.ToString());
                    //}
                    //else
                    //{
                    var message = _svc.SaveEnrollment(child, uInfo.ToString());
                    //}
                    if (message.Count() >= 1 && message[0].Message == "Child_ID")
                    {
                        var duplicates = new List<ReturnMessage>();
                        string childID = message[0].Code;
                        if (child.ChildID == message[0].Code)
                        {
                            foreach (var item in all_docs)
                            {
                                UploadFile file = SessionManager.Get<UploadFile>(item.ContentType);
                                // upload File only when it is not already uploaded and was retrieved from DB when working on incomplete saved enrollments
                                if (file != null && file.FileBytes.Length > 0 && !item.ContentType.Contains("AlreadyUploaded"))
                                {
                                    file.ContentTypeID = int.Parse(item.ContentType.Split('_')[1]);
                                    files_uploaded = _svc.UploadAnEnrollmentFile(file);
                                    if (files_uploaded[0] == null || files_uploaded[0].Code == "-2")
                                    { filevalidationResults.Add(item.ContentName, Helper.MessageWithDateTimeAdded(item.ContentName + " could not be uploaded !")); }
                                }
                                else { continue; }
                            }
                        }
                        for (var i = 1; i <= message.Count() - 1; i++)
                        {
                            duplicates.Add(message[i]);
                        }
                        var data = new { Success = true, validationResult = validationResults, fileValidationResult = filevalidationResults, ID = childID, Message = Helper.MessageWithDateTimeAdded(message[0].Description), duplicates = duplicates };
                        return data;
                    }
                    else
                    {
                        var data = new { Success = false, validationResult = validationResults, fileValidationResult = filevalidationResults, Message = Helper.MessageWithDateTimeAdded("Something went wrong in saving child Information !") };
                        return data;
                    }
                }
                else
                {
                    var data = new { Success = false, validationResult = validationResults, fileValidationResult = filevalidationResults };
                    return data;
                }
            }
            catch (Exception ex)
            {
                log.Debug("Something went wrong in creating the child enrollment !", ex);
                return null;
            }
        }

        //Summary
        //This method is save the child record in the case of a declined enrollment.
        [NonAction]
        private object saveADeclinedChild(FormCollection formData)
        {
            try
            {
                var currentProcess = formData["button.id"];
                var DeclinedChildInfo = formData["declines.IsDeclinedChildInfo"];
                var DeclinedEnrollmentInfo = formData["declines.IsDeclinedEnrollmentInfo"];
                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                bool ignoreChoreAndFavValidation = false;
                if (currentProcess == "child_saveDeclinedChildInfo")
                {
                    ignoreChoreAndFavValidation = true;
                }
                var resultSet = validationResult(formData, "Enrollment", ignoreChoreAndFavValidation);
                var childEnrollInfo = resultSet[0];
                var validationResults = resultSet[1];
                int count = 0;
                Dictionary<string, string> filevalidationResults = new Dictionary<string, string>();

                //Since only the child form fields are submitted on child record creation, making sure that child form fields are not in the validation result list.
                foreach (var item in validationResults)
                {
                    if (item.Key == "FirstName" || item.Key == "LastName" || item.Key == "LocationCodeID" || item.Key == "GenderCodeID" || item.Key == "DateOfBirth" || item.Key == "MiddleName" || item.Key == "OtherNameGoesBy")
                    {
                        count += 1;
                    }
                }
                if (count == 0)
                {
                    List<ReturnMessage> message = new List<ReturnMessage>();

                    //if (currentProcess == "child_saveDeclinedChildInfo" || currentProcess == "child_submitDeclinedChildInfo" || currentProcess == "enroll_saveDeclinedEnrollmentInfo" || currentProcess == "enroll_submitDeclinedEnrollmentInfo")
                    //{
                    if (currentProcess == "child_saveDeclinedChildInfo")
                    {
                        var child = _util.AddDeclinedChildInformationObject(childEnrollInfo);
                        message = _svc.UpdateDecline(child, uInfo.ToString()).ToList();
                    }
                    else if (currentProcess == "enroll_saveDeclinedEnrollmentInfo")
                    {
                        var child = _util.AddDeclinedEnrollmentInformationObject(childEnrollInfo);
                        message = _svc.UpdateDecline(child, uInfo.ToString()).ToList();
                        //var child = _util.AddUpdateChildObject(childEnrollInfo, currentProcess);
                        // message = _svc.SaveUpdate(child, uInfo.ToString()).ToList();
                    }

                    if (message.Count() >= 1 && message[0].Message == "Child_ID")
                    {
                        var duplicates = new List<ReturnMessage>();
                        string childID = message[0].Code;
                        //if (child.ChildID == message[0].Code)
                        //{
                        //    foreach (var item in all_docs)
                        //    {
                        //        var file = SessionManager.Get<UploadFile>(item.ContentType);
                        //        // upload File only when it is not already uploaded and was retrieved from DB when working on incomplete saved enrollments
                        //        if (file != null && file.FileBytes.Length > 0 && !item.ContentType.Contains("AlreadyUploaded"))
                        //        {
                        //            file.ContentTypeID = int.Parse(item.ContentType.Split('_')[1]);
                        //            files_uploaded = _svc.UploadAnUpdateFile(file);
                        //            if (files_uploaded[0] == null || files_uploaded[0].Code == "-2")
                        //            { filevalidationResults.Add(item.ContentName, Helper.MessageWithDateTimeAdded(item.ContentName + " could not be uploaded !")); }
                        //        }
                        //        else { continue; }
                        //    }
                        //}
                        for (var i = 1; i <= message.Count() - 1; i++)
                        {
                            duplicates.Add(message[i]);
                        }
                        var data = new { Success = true, validationResult = validationResults, fileValidationResult = filevalidationResults, ID = childID, Message = Helper.MessageWithDateTimeAdded(message[0].Description), duplicates = duplicates };
                        return data;
                    }
                    else
                    {
                        var data = new { Success = false, validationResult = validationResults, fileValidationResult = filevalidationResults, Message = Helper.MessageWithDateTimeAdded("Something went wrong in saving child Information !") };
                        return data;
                    }
                }
                else
                {
                    var data = new { Success = false, validationResult = validationResults, fileValidationResult = filevalidationResults };
                    return data;
                }
            }
            catch (Exception ex)
            {
                log.Debug("Something went wrong in saving the declined information!", ex);
                return null;
            }
        }

        // Summary
        // Returns the Child Update Index page
        public ActionResult Update()
        {
            UserInfo uInfo = SessionManager.Get<UserInfo>("UserInfo");
            if (uInfo == null)
            {
                return RedirectToAction("SessionReset", "Home");
            }

            Dictionary<string, object> dct_User_Defaults = Classes.User_Session_Defaults.Get_User_Defaults(Session);

            var updateModel = new CommonVM(User_Session_Defaults.enum_Display_Page.Update, Session);

            updateModel.currentUser = uInfo;
            updateModel.Declines = new List<DeclineDetails>();
            //Remove all previous user files or data stored in Session
            Session.RemoveAll();
            SessionManager.Set<UserInfo>("UserInfo", updateModel.currentUser);

            updateModel.user_Session_Defaults.Add_Dictionary_Items_To_Session(Session, dct_User_Defaults);

            return View(updateModel);
        }

        //Summary
        //Get the list of child records pending for Update 
        [HttpPost]
        public string getIncompleteupdates(string countryID, string locationID, string actionTypeID)
        {
            try
            {

                Classes.User_Session_Defaults.Set_User_Preferred_Country_Session_Var(Session, countryID);

                Classes.User_Session_Defaults.Set_User_Preferred_Location_Code_ID_Session_Var(Session, locationID);

                Classes.User_Session_Defaults.Set_User_Preferred_Update_Action_Session_Var(Session, actionTypeID);

                var uInfo = SessionManager.Get<UserInfo>("UserInfo");

                var Incompleteupdates = new ListChild[2];

                Incompleteupdates = _svc.GetIncompleteUpdates(uInfo.ToString(), Helper.ParseInt(countryID), Helper.ParseInt(locationID), Helper.ParseInt(actionTypeID));

                return JsonConvert.SerializeObject(Incompleteupdates);

            }
            catch (Exception ex)
            {
                log.Debug("Failed to get the Incomplete child updates !", ex);

                return null;
            }
        }


        //Summary 
        // This method loads/brings the child record for update
        // Populate some of the Update Information drop down options in the Update module depending on pre-populated date of birth field.
        public ActionResult LoadUpdateChild(string id)
        {
            ViewBag.Module = "Update";
            var updateModel = _util.setupUpdateView(id);
            updateModel.Declines = new List<DeclineDetails>();
            return View("Enrollment", updateModel);
        }

        #region declines  
        //Summary
        //Get the list of child records that have declined enrollments and updates 
        [HttpPost]
        public string getDeclinedWorklist
            (
            string countryID, 
            string locationID, 
            string actionTypeID, 
            string workflowType
            )
        {
            try
            {

                Classes.User_Session_Defaults.Set_User_Preferred_Country_Session_Var(Session, countryID);

                switch (workflowType)
                {
                    case "Enrollment":

                        Classes.User_Session_Defaults.Set_User_Preferred_Enrollment_Action_Type_Session_Var(Session, actionTypeID);

                        break;
                    case "Update":
                                                
                        Classes.User_Session_Defaults.Set_User_Preferred_Update_Action_Session_Var(Session, actionTypeID);
                        
                        break;
                }

                var workflowID = CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Workflow").FirstOrDefault(a => a.Item2 == workflowType).Item1;
                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                var DeclinedEnrollments = _svc.GetDeclineList(int.Parse(workflowID), 0, String.Empty, true, uInfo.ToString(), true, Helper.ParseInt(countryID), Helper.ParseInt(locationID));
                var Decline_List = new List<object>();

                foreach (DataRow row in DeclinedEnrollments.Tables[0].Rows)
                {
                    Decline_List.Add(new
                    {
                        ChildID = Convert.ToString(row["Child_ID"]),
                        ChildNumber = Convert.ToString(row["Child_Number"]),
                        FirstName = Convert.ToString(row["First_Name"]),
                        MiddleName = Convert.ToString(row["Middle_Name"]),
                        LastName = Convert.ToString(row["Last_Name"]),
                        OtherName = Convert.ToString(row["Nickname"]),
                        Location = Convert.ToString(row["Location_Code_ID"]),
                        Workflow = Convert.ToString(row["Workflow"]),
                        DateOfBirth = Convert.ToDateTime(row["Date_of_Birth"]).Date,
                        Gender = Convert.ToString(row["Gender"])
                    });
                }
                return JsonConvert.SerializeObject(Decline_List);
            }
            catch (Exception ex)
            {
                log.Debug("Failed to get the declined child enrollments !", ex);
                return null;
            }
        }

        public string CompleteDeclinedStep(FormCollection formData)
        {
            string retStr = string.Empty;

            try
            {
                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                var currentStep = formData["correctedStep"];

                if (!String.IsNullOrEmpty(currentStep))
                {
                    if (currentStep.Contains("Profile"))
                    {
                        retStr = _util.UploadNewDocumentForDeclined("Profile");
                    }
                    else if (currentStep.Contains("Action"))
                    {
                        retStr = _util.UploadNewDocumentForDeclined("Action");
                    }
                    else if (currentStep.Contains("Consent"))
                    {
                        retStr = _util.UploadNewDocumentForDeclined("Consent");
                    }
                    else if (currentStep.Contains("Art"))
                    {
                        retStr = _util.UploadNewDocumentForDeclined("Art");
                    }
                    else if (currentStep.Contains("Letter"))
                    {
                        retStr = _util.UploadNewDocumentForDeclined("Letter");
                    }
                    else
                    {
                        var saveErrors = SessionManager.Get<List<string>>("DeclineSaveErrors");
                        if (saveErrors == null || saveErrors.Count == 0)
                        {
                            retStr = "true";
                        }
                    }

                    if (retStr.ToLower().Contains("true"))
                    {
                        var workflowID = CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Workflow").FirstOrDefault(a => a.Item2 == formData["workflowID"]).Item1;
                        var statusCodes = CacheManager.GetFromCache<List<SelectListItem>>("Status");
                        var stepCodes = CacheManager.GetFromCache<List<WorkflowStepModel>>("Step");
                        var stepInfo = formData["correctedStep"].ToString().Split('&');
                        var stepDescription = stepInfo[0].Split(' ');

                        var ContentType = (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).ToList();
                        var currentContentType = ContentType.FirstOrDefault(a => a.Content_Type.ToLower() == stepDescription[0].ToLower());

                        UploadFile file = null;

                        if (currentContentType != null)
                        {
                            var contentTypeId = currentContentType.Content_Type_Code_ID;
                            file = SessionManager.Get<UploadFile>("ContentType_" + contentTypeId);
                        }

                        var wk_step = new WorkFlowStep();

                        wk_step.Pending_Child_ID = Helper.ParseInt(formData["PendingChildID"]);
                        wk_step.Workflow_Code_ID = Helper.ParseInt(workflowID);
                        wk_step.Step_Code_ID = stepCodes.FirstOrDefault(a => a.Description.ToLower().Contains(stepDescription[0].ToLower())).step_code_id;
                        wk_step.Status_Code_ID = 1;
                        if (file != null) wk_step.File_ID = file.FileID;
                        wk_step.QCApprover = false;
                        wk_step.Complete = true;
                        wk_step.Child_Workflow_Step_ID = int.Parse(stepInfo[1]);

                        var Message = _svc.SaveWorkFlow(wk_step, uInfo.ToString());
                        if (Message.Code == "1")
                        {
                            var data = new { Success = true, Message = Helper.MessageWithDateTimeAdded(Message.Message) };
                            return JsonConvert.SerializeObject(data);
                        }
                        else
                        {
                            var data = new { Success = false, Message = Helper.MessageWithDateTimeAdded(Message.Message) };
                            return JsonConvert.SerializeObject(data);
                        }
                    }
                    else
                    {
                        var data = new { Success = false, Message = "There are save errors that need to be fixed before the step is complete" };
                        return JsonConvert.SerializeObject(data);
                    }
                }
                else
                {
                    var data = new { Success = false, Message = "Please select a declined step to complete" };
                    return JsonConvert.SerializeObject(data);
                }
            }
            catch (Exception ex)
            {
                log.Debug("Failed to save the workflow !", ex);
                return null;
            }
        }

        //Process to Save information of a declined workflow step
        [HttpPost]
        public string DeclineWorkflowStep(string childWorkflowStepID, string pendingchildID, string declinedStep, string workflowID, string action, string StatusCodeID, string DeclineReason, string DeclineNotes)
        {
            try
            {
                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                if (!string.IsNullOrEmpty(pendingchildID) && !string.IsNullOrEmpty(declinedStep))
                {
                    var wk_step = new Decline();
                    wk_step.Child_Workflow_Step_ID = Helper.ParseInt(childWorkflowStepID);
                    wk_step.Decline_Note = DeclineNotes;
                    var declineReasonsFromCache = CacheManager.GetFromCache<List<SelectListItem>>("Decline_Reason");
                    var declineMajor = declineReasonsFromCache.FirstOrDefault(a => a.Text == declinedStep).Value;
                    wk_step.Decline_Reason_Code = Helper.ParseInt(declineMajor);
                    // wk_step.Workflow_Response = int.Parse(workflowID);
                    var DeclineSubReasonList = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Decline_SubReason"));
                    var SubDeclines = DeclineSubReasonList.Where(a => a.Item2 == declinedStep);
                    //var SubDeclines = DeclineSubReasonList.FirstOrDefault(a => a.Item3 == DeclineReason);
                    var SubReason = SubDeclines.FirstOrDefault(a => a.Item3 == DeclineReason);
                    wk_step.Decline_SubReason_Code = int.Parse(SubReason.Item1);
                    var Message = _svc.DeclineStep(wk_step, uInfo.ToString());
                    var data = new { Success = true, Message = Helper.MessageWithDateTimeAdded(Message.Message) };
                    return JsonConvert.SerializeObject(data);

                }
                else
                {
                    var data = new { Success = false, Message = Helper.MessageWithDateTimeAdded("Pending Child ID or Workflow Step ID is missing !") };
                    return JsonConvert.SerializeObject(data);
                }
            }
            catch (Exception ex)
            {
                log.Debug("Failed to save the workflow !", ex);
                return null;
            }
        }

        //Summary 
        // This method loads/brings the child record for declined update
        public ActionResult LoadDeclinedUpdateChild(string id)
        {
            ViewBag.Module = "Update";
            var updateModel = _util.setupUpdateView(id);
            var ContnetTypes = (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).Where(m => m.Required == true);
            updateModel.Declines = _svc.GetChildDeclines(id, 0, false, updateModel.currentUser.ToString(), true).ToList();
            List<SelectListItem> declinedContnetTypes = new List<SelectListItem>();

            foreach (var item in ContnetTypes)
            {
                var data = updateModel.Declines.FirstOrDefault(a => a.DeclineReason.Contains(item.Content_Type));
                if (data != null)
                {
                    SelectListItem item1 = new SelectListItem() { Text = item.Description, Value = item.Content_Type_Code_ID.ToString() };
                    declinedContnetTypes.Add(item1);
                }
            }
            SessionManager.Set<List<SelectListItem>>("DeclinedContentTypes", declinedContnetTypes);
            updateModel.Child_declinedSteps_ddl = updateModel.Declines.Select(x => new SelectListItem { Text = x.Step, Value = x.Step + "&" + x.ChildWorkflowStepID.ToString() });
            updateModel.declinedContnetTypes = declinedContnetTypes.AsEnumerable();
            var info = SessionManager.Get<List<SelectListItem>>("DeclinedContentTypes");
            updateModel.AlreadyUploadedDocuments = null;
            return View("Enrollment", updateModel);
        }


        //Summary 
        // This method loads/brings the child record for declined enrollment
        public ActionResult LoadDeclinedEnrollmentChild(string id)
        {
            ViewBag.Module = "Enrollment";
            var model = _util.setupDeclinedEnrollmentView(id);

            var ContnetTypes = (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).Where(m => m.Required == true);
            List<SelectListItem> declinedContnetTypes = new List<SelectListItem>();
            foreach (var item in ContnetTypes)
            {
                var data = model.Declines.FirstOrDefault(a => a.DeclineReason.Contains(item.Content_Type));
                if (data != null)
                {
                    SelectListItem item1 = new SelectListItem() { Text = item.Description, Value = item.Content_Type_Code_ID.ToString() };
                    declinedContnetTypes.Add(item1);
                }
            }
            SessionManager.Set<List<SelectListItem>>("DeclinedContentTypes", declinedContnetTypes);
            //model.Child_declinedSteps_ddl = model.Declines.Select(x => new SelectListItem { Text = x.Step, Value = x.ChildWorkflowStepID + "&" + x.FileID});
            model.Child_declinedSteps_ddl = model.Declines.Select(x => new SelectListItem { Text = x.Step, Value = x.Step + "&" + x.ChildWorkflowStepID.ToString() });
            model.declinedContnetTypes = declinedContnetTypes.AsEnumerable();
            var info = SessionManager.Get<List<SelectListItem>>("DeclinedContentTypes");
            model.AlreadyUploadedDocuments = null;
            return View("Enrollment", model);
        }

        #endregion
        //Summary 
        // GET: PanMain/RemoveChild/AC0671A7-36B0-4E96-8929-F64695793926
        public ActionResult RemoveChild(string id)
        {
            ViewBag.Module = "ChildRemove";
            var updateModel = _util.setupUpdateView(id);
            return View("RemoveChildUpdate", updateModel);
        }

        public ActionResult RemoveChildEnrollment(string id)
        {
            ViewBag.Module = "EnrollmentRemove";
            var updateModel = _util.setupIncompleteEnrollmentView(id);
            return View("RemoveChildEnrollment", updateModel);
        }

        //Summary 
        // This method removes a child update with the provided child ID from the system
        public string DeleteUpdate(string childID, string RemoveReasonID)
        {
            try
            {
                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                var data = new { Success = false, Message = "" };
                if (!string.IsNullOrEmpty(RemoveReasonID))
                {
                    var RemovalReasonID = Convert.ToInt32(RemoveReasonID);
                    var Response = _svc.DeleteUpdate(childID, uInfo.ToString(), RemovalReasonID);
                    if (Response[0].Code == "1")
                    {
                        data = new { Success = true, Message = Helper.MessageWithDateTimeAdded(Response[0].Message) };
                    }
                    else
                    {
                        data = new { Success = false, Message = Helper.MessageWithDateTimeAdded(Response[0].Message) };
                    }
                }
                else
                {
                    data = new { Success = false, Message = Helper.MessageWithDateTimeAdded("Please select a reason for removing the child !") };
                }
                return JsonConvert.SerializeObject(data);
            }
            catch (Exception ex)
            {
                log.Debug("Failed to get the delete the child update record !", ex);
                return null;
            }
        }

        //Summary 
        // This method removes a child enrollment with the provided child ID from the system
        public string DeleteChildEnrollment(string childID)
        {
            try
            {
                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                var filevalidationResults = new Dictionary<string, string>();
                var all_docs = (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).Select(c => new { ContentType = "ContentType_" + c.Content_Type_Code_ID.ToString(), ContentName = c.Description }).ToList();
                var data = new { Success = false, Message = "", FileValidation = filevalidationResults };
                if (!string.IsNullOrEmpty(childID))
                {
                    var DeleteEnrollment_Response = _svc.DeleteEnrollment(childID, uInfo.ToString());
                    if (DeleteEnrollment_Response[0].Code == "0")
                    {
                        foreach (var item in all_docs)
                        {
                            var file = SessionManager.Get<UploadFile>(item.ContentType);
                            if (file != null && file.FileBytes.Length > 0)
                            {
                                var filedelete_Response = _svc.DeleteAnEnrollmentFile(file);
                                if (filedelete_Response == null || filedelete_Response[0].Code == "-2")
                                { filevalidationResults.Add(item.ContentName, Helper.MessageWithDateTimeAdded(item.ContentName + " could not be deleted from the database !")); }
                            }
                            else { continue; }
                        }
                        data = new { Success = true, Message = Helper.MessageWithDateTimeAdded(DeleteEnrollment_Response[0].Message), FileValidation = filevalidationResults };
                    }
                    else
                    {
                        data = new { Success = false, Message = Helper.MessageWithDateTimeAdded(DeleteEnrollment_Response[0].Message), FileValidation = filevalidationResults };
                    }
                }
                else
                {
                    data = new { Success = false, Message = Helper.MessageWithDateTimeAdded("Please provide a valid child ID !"), FileValidation = filevalidationResults };
                }
                return JsonConvert.SerializeObject(data);
            }
            catch (Exception ex)
            {
                log.Debug("Failed to get the delete the child enrollment record !", ex);
                return null;
            }
        }


        // Summary
        //This method saves the child update information
        [HttpPost]
        public ActionResult SaveUpdate(FormCollection formData)
        {
            try
            {
                var currentProcess = formData["button.id"];
                var DeclinedUpdateInfo = formData["declines.IsDeclinedUpdateInfo"];
                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                var resultSet = validationResult(formData, "Update");
                var childEnrollInfo = resultSet[0];
                var validationResults = resultSet[1];
                Dictionary<string, string> filevalidationResults = new Dictionary<string, string>();
                var files_uploaded = new ReturnMessage[10];

                //made changes to look for required documents in the code column rules table 
                var rules = CacheManager.GetFromCache<List<FieldRuleModel>>("Column_Rules").SelectMany(c => c.ColumnRules).Where(m => (m.validationType == "ReqValidation" || m.validationType == "DocValidation") && m.ModuleName == "Update" && m.isRequired).ToList();
                var req_docs = new List<ContentTypeModel>();
                var req_Declined_docs = SessionManager.Get<List<SelectListItem>>("DeclinedContentTypes");
                var all_docs = (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).Select(c => new { ContentType = "ContentType_" + c.Content_Type_Code_ID.ToString(), ContentName = c.Description }).ToList();

                foreach (var item in all_docs)
                {
                    if (rules.FirstOrDefault(a => a.ColumnRuleName.Contains(item.ContentName)) != null)
                    {
                        req_docs.Add(new ContentTypeModel { ContentType = item.ContentType, ContentName = item.ContentName });
                    }
                }
                // if saving the declined update record validate the documents that are declined only
                if (currentProcess == "enroll_saveDeclinedUpdate")
                {
                    foreach (var item in req_Declined_docs)
                    {
                        //do not show validation messages for already uploaded
                        if ((SessionManager.Get<UploadFile>("ContentType_" + item.Value)) == null)
                        {
                            filevalidationResults.Add(item.Text, Helper.MessageWithDateTimeAdded(item.Text + " is missing"));
                        }
                    }
                }
                else
                {
                    foreach (var item in req_docs)
                    {
                        //do not show validation messages for already uploaded
                        if ((SessionManager.Get<UploadFile>(item.ContentType)) == null && (SessionManager.Get<UploadFile>(item.ContentType + "_AlreadyUploaded")) == null)
                        {
                            filevalidationResults.Add(item.ContentName, Helper.MessageWithDateTimeAdded(item.ContentName + " is missing"));
                        }
                    }
                }
                //Do not save the child again when working on decline when nothing has changed on the update information 
                //as indicated by the hidden form field that stores the count of declines that were of type update information
                if (currentProcess == "enroll_saveDeclinedUpdate" && DeclinedUpdateInfo == "0")
                {
                    foreach (var item in all_docs)
                    {
                        var file = SessionManager.Get<UploadFile>(item.ContentType);
                        // prevent reuploading of file                
                        if (file != null && file.FileBytes.Length > 0 && !item.ContentType.Contains("AlreadyUploaded"))
                        {
                            file.ContentTypeID = int.Parse(item.ContentType.Split('_')[1]);
                            files_uploaded = _svc.UploadAnUpdateFile(file);
                            if (files_uploaded[0] == null || files_uploaded[0].Code == "-2")
                            { validationResults.Add(item.ContentName, Helper.MessageWithDateTimeAdded("The file: " + item.ContentName + " could not be uploaded !")); }
                        }
                    }
                    var data = new { Success = true, validationResult = validationResults, FileValidation = filevalidationResults, Message = String.Empty };
                    return Json(data);
                }
                else
                {
                    var child = _util.AddUpdateChildObject(childEnrollInfo);
                    // if(child != null){
                    var message = _svc.SaveUpdate(child, uInfo.ToString());
                    if (message.Count() >= 1 && message[0].Code == child.ChildID)
                    {
                        foreach (var item in all_docs)
                        {
                            var file = SessionManager.Get<UploadFile>(item.ContentType);
                            // prevent reuploading of file                
                            if (file != null && file.FileBytes.Length > 0 && !item.ContentType.Contains("AlreadyUploaded"))
                            {
                                file.ContentTypeID = int.Parse(item.ContentType.Split('_')[1]);
                                files_uploaded = _svc.UploadAnUpdateFile(file);
                                if (files_uploaded[0] == null || files_uploaded[0].Code == "-2")
                                {
                                    validationResults.Add(item.ContentName, Helper.MessageWithDateTimeAdded("The file: " + item.ContentName + " could not be uploaded !"));
                                }
                                else
                                {
                                    file.FileID = Helper.ParseInt(files_uploaded[0].Code);
                                }
                            }
                            else
                            { continue; }
                        }

                        //get workflow for this item and update the file id

                        int pendingChildID = 0;
                        var QC_List_Dataset = _svc.GetWorkFlows(0, 0, 0, uInfo.ToString(), "");
                        foreach (DataRow row in QC_List_Dataset.Tables[0].Rows)
                        {
                            if (!child.ChildID.Equals(Convert.ToString(row["Child_GUID"]))) continue;
                            pendingChildID = Convert.ToInt32(row["Pending_Child_ID"]);
                            break;
                        }
                        if (pendingChildID != 0)
                        {
                            var contentTypes = (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).ToList();
                            var _chWrkFlowSteps = _svc.GetChildWorkFlow(pendingChildID, 2, uInfo.ToString(), false);
                            foreach (var _step in _chWrkFlowSteps)
                            {
                                var type = _step.Step_Name.Split();
                                var contentDetails = contentTypes.FirstOrDefault(a => a.Content_Type.Contains(type[0]));
                                if (contentDetails != null)
                                {
                                    var file = SessionManager.Get<UploadFile>("ContentType_" + contentDetails.Content_Type_Code_ID);
                                    if (file != null && !string.IsNullOrEmpty(file.FileID.ToString()))
                                    {
                                        _step.File_ID = file.FileID;
                                    }
                                }
                                _svc.SaveWorkFlow(_step, uInfo.ToString());
                            }
                        }

                        Session.RemoveAll();
                        SessionManager.Set<UserInfo>("UserInfo", uInfo);
                        var data = new { Success = true, validationResult = validationResults, FileValidation = filevalidationResults, Message = Helper.MessageWithDateTimeAdded(message[0].Description) };
                        return Json(data);
                    }
                    else
                    {
                        var data = new { Success = false, validationResult = validationResults, FileValidation = filevalidationResults, Message = Helper.MessageWithDateTimeAdded(message[0].Description) };
                        return Json(data);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Debug("Something went wrong in saving the child update !", ex);
                return null;
            }
        }

        // Summary
        // This method saves and submits the child update information
        [HttpPost]
        public ActionResult SubmitUpdate(FormCollection formData)
        {
            try
            {
                var currentProcess = formData["button.id"];
                var DeclinedUpdateInfo = formData["declines.IsDeclinedUpdateInfo"];
                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                UpdateChild child = new UpdateChild();
                var resultSet = validationResult(formData, "Update");
                var childEnrollInfo = resultSet[0];
                var validationResults = resultSet[1];
                var count = 0;
                var declinedDocCount = 0;
                Dictionary<string, string> filevalidationResults = new Dictionary<string, string>();
                var files_uploaded = new ReturnMessage[10];
                // made changes to look for required content types in the code colum rules and not the content type table
                var rules = CacheManager.GetFromCache<List<FieldRuleModel>>("Column_Rules").SelectMany(c => c.ColumnRules).Where(m => (m.validationType == "ReqValidation" || m.validationType == "DocValidation") && m.ModuleName == "Update" && m.isRequired).ToList();
                var all_docs = new List<ContentTypeModel>();
                var docs = (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).ToList();
                var req_Declined_docs = SessionManager.Get<List<SelectListItem>>("DeclinedContentTypes");
                foreach (var c in docs)
                {
                    all_docs.Add(new ContentTypeModel { ContentType = "ContentType_" + c.Content_Type_Code_ID.ToString(), ContentName = c.Description });
                }

                var req_docs = new List<ContentTypeModel>();

                foreach (var item in all_docs)
                {
                    if (rules.FirstOrDefault(a => a.ColumnRuleName.Contains(item.ContentName)) != null)
                    {
                        req_docs.Add(new ContentTypeModel { ContentType = item.ContentType, ContentName = item.ContentName });
                    }
                }

                if (currentProcess == "enroll_submitDeclinedUpdate")
                {
                    foreach (var item in req_Declined_docs)
                    {
                        //do not show validation messages for already uploaded
                        if ((SessionManager.Get<UploadFile>("ContentType_" + item.Value)) == null)
                        {
                            filevalidationResults.Add(item.Text, Helper.MessageWithDateTimeAdded(item.Text + " is missing"));
                        }
                    }
                }
                else
                {
                    foreach (var item in req_docs)
                    {
                        if ((SessionManager.Get<UploadFile>(item.ContentType)) == null && (SessionManager.Get<UploadFile>(item.ContentType + "_AlreadyUploaded")) == null)
                        {
                            filevalidationResults.Add(item.ContentName, Helper.MessageWithDateTimeAdded(item.ContentName + " is missing"));
                        }
                        else
                        {
                            count += 1;
                        }
                    }
                }
                //Do not save the child again when working on decline when nothing has changed on the update information 
                //as indicated by the hidden form field that stores the count of declines that were of type update information
                if (currentProcess == "enroll_submitDeclinedUpdate" && DeclinedUpdateInfo == "0")
                {
                    child.ChildID = formData["child.ChildID"];
                    foreach (var item in all_docs)
                    {
                        var file = SessionManager.Get<UploadFile>(item.ContentType);
                        if (file != null)
                        {
                            // prevent uploading already uploaded files
                            if (!item.ContentType.Contains("AlreadyUploaded"))
                            {
                                //adding content type of the file before saving the file 
                                file.ContentTypeID = int.Parse(item.ContentType.Split('_')[1]);
                                files_uploaded = _svc.UploadAnUpdateFile(file);
                            }
                        }
                        else { continue; }
                        if (files_uploaded[0] != null && files_uploaded[0].Code == "0")
                        {
                            if (req_Declined_docs.FirstOrDefault(a => a.Text == item.ContentName) != null) { declinedDocCount += 1; }
                        }
                        else
                        {
                            filevalidationResults.Add(item.ContentName, Helper.MessageWithDateTimeAdded("The file: " + item.ContentName + " could not be uploaded !"));
                        }
                    }
                }
                else
                {
                    child = _util.AddUpdateChildObject(childEnrollInfo);
                    var message = _svc.SaveUpdate(child, uInfo.ToString());
                    if (message.Count() >= 1 && message[0].Code == child.ChildID)
                    {
                        foreach (var item in all_docs)
                        {
                            UploadFile file = SessionManager.Get<UploadFile>(item.ContentType);
                            if (file != null)
                            {
                                // prevent uploading already uploaded files
                                if (!item.ContentType.Contains("AlreadyUploaded"))
                                {
                                    //adding content type of the file before saving the file 
                                    file.ContentTypeID = int.Parse(item.ContentType.Split('_')[1]);
                                    files_uploaded = _svc.UploadAnUpdateFile(file);
                                    file.FileID = Helper.ParseInt(files_uploaded[0].Code);
                                }
                            }
                            else { continue; }
                            //if (files_uploaded[0] != null && files_uploaded[0].Code == "0")
                            //{
                            //    if (req_docs.FirstOrDefault(a => a.ContentName == item.ContentName) != null) {  }
                            //}
                            //else
                            //{
                            //    filevalidationResults.Add(item.ContentName, Helper.MessageWithDateTimeAdded("The file: " + item.ContentName + " could not be uploaded !"));
                            //}
                        }
                    }
                    else
                    {
                        var data = new { Success = false, validationResult = validationResults, FileValidation = filevalidationResults, Message = Helper.MessageWithDateTimeAdded(message[0].Description) };
                        return Json(data);
                    }
                }
                if ((currentProcess != "enroll_submitUpdate" && count == req_docs.Count()) && validationResults.Count == 0 || (currentProcess == "enroll_submitDeclinedUpdate" && declinedDocCount == req_Declined_docs.Count()) && validationResults.Count == 0)
                {
                    var submit_result = _svc.SubmitUpdate(child.ChildID, uInfo.ToString());
                    if (submit_result.Count() >= 1 && submit_result[0].Message == "Child_Number")
                    {
                        // Need to update the workflow here to be QC_Approve_Action
                        int pendingChildID = 0;
                        var QC_List_Dataset = _svc.GetWorkFlows(0, 0, 0, uInfo.ToString(), "");
                        foreach (DataRow row in QC_List_Dataset.Tables[0].Rows)
                        {
                            if (!child.ChildID.Equals(Convert.ToString(row["Child_GUID"]))) continue;
                            pendingChildID = Convert.ToInt32(row["Pending_Child_ID"]);
                            break;
                        }

                        if (pendingChildID != 0)
                        {
                            var contentTypes = (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).ToList();
                            var _chWrkFlowSteps = _svc.GetChildWorkFlow(pendingChildID, 2, uInfo.ToString(), false);
                            foreach (var _step in _chWrkFlowSteps)
                            {
                                var type = _step.Step_Name.Split();
                                var contentDetails = contentTypes.FirstOrDefault(a => a.Content_Type.Contains(type[0]));
                                if (contentDetails != null)
                                {
                                    var file = SessionManager.Get<UploadFile>("ContentType_" + contentDetails.Content_Type_Code_ID);
                                    if (file != null)
                                    {
                                        _step.File_ID = file.FileID;
                                    }
                                }
                                _step.QCApprover = false;
                                _step.Complete = true;
                                _svc.SaveWorkFlow(_step, uInfo.ToString());
                            }
                        }

                        //Clearing the current session keyvalue collection
                        Session.RemoveAll();
                        SessionManager.Set<UserInfo>("UserInfo", uInfo);
                        var data = new { Success = true, ID = child.ChildID, Number = submit_result[0].Code, Message = Helper.MessageWithDateTimeAdded(submit_result[0].Description), validationResult = filevalidationResults };
                        return Json(data);
                    }
                    else
                    {
                        var data = new { Success = false, validationResult = filevalidationResults, Message = Helper.MessageWithDateTimeAdded("Something went wrong in submitting the enrollment !") };
                        return Json(data);
                    }
                }
                else
                {
                    var data = new { Success = false, validationResult = validationResults, FileValidation = filevalidationResults, Message = Helper.MessageWithDateTimeAdded(" Submission could not be done as the following information is missing !") };
                    return Json(data);
                }
            }
            catch (Exception ex)
            {
                log.Debug("Something went wrong in submitting the child update !", ex);
                return null;
            }
        }

        /// <summary>
        /// This method loads the Quality control landing page
        /// </summary>
        /// <returns></returns>
        public ActionResult QualityControl()
        {
            UserInfo uInfo = SessionManager.Get<UserInfo>("UserInfo");
            if (uInfo == null)
            {
                return RedirectToAction("SessionReset", "Home");
            }
            
            Dictionary<string, object> dct_User_Defaults = Classes.User_Session_Defaults.Get_User_Defaults(Session);

            var QCModel = new CommonVM(User_Session_Defaults.enum_Display_Page.Quality_Control, Session);

            QCModel.currentUser = uInfo;
            QCModel.Declines = new List<DeclineDetails>();
            //Remove all previous user files or data stored in Session
            Session.RemoveAll();
            SessionManager.Set<UserInfo>("UserInfo", QCModel.currentUser);
            
            QCModel.user_Session_Defaults.Add_Dictionary_Items_To_Session(Session, dct_User_Defaults);

            return View(QCModel);
        }

        /// <summary>
        /// This method loads the Translation landing page
        /// </summary>
        /// <returns></returns>
        public ActionResult Translation()
        {
            UserInfo uInfo = SessionManager.Get<UserInfo>("UserInfo");
            if (uInfo == null)
            {
                return RedirectToAction("SessionReset", "Home");
            }


            Dictionary<string, object> dct_User_Defaults = Classes.User_Session_Defaults.Get_User_Defaults(Session);

            var TranslationModel = new CommonVM(User_Session_Defaults.enum_Display_Page.Translation, Session);


            TranslationModel.currentUser = uInfo;
            TranslationModel.Declines = new List<DeclineDetails>();
            //Remove all previous user files or data stored in Session
            Session.RemoveAll();
            SessionManager.Set<UserInfo>("UserInfo", TranslationModel.currentUser);

            TranslationModel.user_Session_Defaults.Add_Dictionary_Items_To_Session(Session, dct_User_Defaults);
            
            return View(TranslationModel);
        }

        //Summary
        //Get the list of child records pending for Quality control
        [HttpPost]
        public string getQClist
            (
            string countryID, 
            string locationID, 
            string workflowID
            )
        {
            try
            {

                // SET USER DEFAULT VALUES
                User_Session_Defaults.Set_User_Preferred_Country_Session_Var(Session, countryID);

                User_Session_Defaults.Set_User_Preferred_Location_Code_ID_Session_Var(Session, locationID);

                User_Session_Defaults.Set_User_Preferred_Default_QualityControl_Action_Type_Session_Var(Session, workflowID);


                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                var QC_List_Dataset = _svc.GetWorkFlows(Helper.ParseInt(workflowID), Helper.ParseInt(countryID), Helper.ParseInt(locationID), uInfo.ToString(), "true");
                //var QC_List_Dataset = _svc.GetWorkFlows(0, 0, 0, uInfo.ToString(), "");

                var QC_List = new List<object>();

                foreach (DataRow row in QC_List_Dataset.Tables[0].Rows)
                {

                    string the_Submittal_Date = "";

                    if (row.Table.Columns.Contains("Action_Date"))
                    {

                        if (row["Action_Date"] != null)
                        {
                            DateTime Time_Stamp = Convert.ToDateTime(row["Action_Date"]);

                            the_Submittal_Date = Time_Stamp.ToString("MM/dd/yyyy");
                        }

                    }                            

                    QC_List.Add(new
                    {
                        Pending_Child_ID = Convert.ToInt32(row["Pending_Child_ID"]),
                        Child_ID = Convert.ToString(row["Child_GUID"]),
                        Child_Number = Convert.ToString(row["Child_Number"]),
                        First_Name = Convert.ToString(row["First_Name"]),
                        Last_Name = Convert.ToString(row["Last_Name"]),
                        Country_Name = Convert.ToString(row["Country_Name"]),
                        Location_Name = Convert.ToString(row["Location_Name"]),
                        Workflow = Convert.ToString(row["Workflow"]),
                        Workflow_Code_ID = Convert.ToString(row["Workflow_Code_ID"]),
                        Child_Information = Convert.ToString(row["Child Information"]),
                        Enrollment_Information = Convert.ToString(row["Enrollment Information"]),
                        Update_Information = Convert.ToString(row["Update Information"]),
                        Consent_Form = Convert.ToString(row["Consent Form"]),
                        Profile_Photo = Convert.ToString(row["Profile Photo"]),
                        Action_Photo = Convert.ToString(row["Action Photo"]),
                        Art = Convert.ToString(row["Art"]),
                        Letter = Convert.ToString(row["Letter"]),
                        Submittal_Date = the_Submittal_Date
                    });                    
                    
                }

                string the_return = JsonConvert.SerializeObject(QC_List);

                return the_return;
            }
            catch (Exception ex)
            {
                log.Debug("Failed to get the Quality Control list !", ex);
                return null;
            }
        }

        //Summary
        //Get the list of child records pending for Quality control
        [HttpPost]
        public string getTranslationList(string countryID, string locationID, string ActionReason)
        {
            try
            {

                // SET USER DEFAULT VALUES
                User_Session_Defaults.Set_User_Preferred_Country_Session_Var(Session, countryID);

                User_Session_Defaults.Set_User_Preferred_Location_Code_ID_Session_Var(Session, locationID);

                User_Session_Defaults.Set_User_Preferred_Translation_Action_Session_Var(Session, ActionReason);


                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                var dataset = _svc.GetTranslationWorkFlows(Helper.ParseInt(ActionReason), Helper.ParseInt(countryID), Helper.ParseInt(locationID), 0, uInfo.ToString());

                //var dataset = _svc.GetWorkFlows(3, Helper.ParseInt(countryID), Helper.ParseInt(locationID), uInfo.ToString(), "");

                var list = new List<object>();

                foreach (DataRow row in dataset.Tables[0].Rows)
                {

                    ;

                    string the_Submittal_Date = "";

                    if (row["Action_Date"] != null)
                    {
                        DateTime Time_Stamp = Convert.ToDateTime(row["Action_Date"]);

                        the_Submittal_Date = Time_Stamp.ToString("MM/dd/yyyy");
                    }

                    ;

                    list.Add(new
                    {
                        Pending_Child_ID = Convert.ToInt32(row["Pending_Child_ID"]),
                        Child_ID = Convert.ToString(row["Child_GUID"]),
                        Child_Number = Convert.ToString(row["Child_Number"]),
                        First_Name = Convert.ToString(row["First_Name"]),
                        Last_Name = Convert.ToString(row["Last_Name"]),
                        Country_Name = Convert.ToString(row["Country_Name"]),
                        Location_Name = Convert.ToString(row["Location_Name"]),
                        Workflow = Convert.ToString(row["Workflow"]),
                        Workflow_Code_ID = Convert.ToString(row["Workflow_Code_ID"]),
                        Letter = Convert.ToString(row["Letter"]),
                        Update_Information = Convert.ToString(row["Update Information"]),
                        Submittal_Date = the_Submittal_Date
                    });
                }
                return JsonConvert.SerializeObject(list);
            }
            catch (Exception ex)
            {
                log.Debug("Failed to get the Translation list !", ex);
                return null;
            }
        }

        /// <summary>
        /// Creates the view for the child's Quality Control
        /// </summary>
        /// <param name="id"></param>
        /// <param name="workflowID"></param>
        /// <param name="pendingChildID"></param>
        /// <returns></returns>
        public ActionResult LoadTranslationView(string id, string actionReason, string pendingChildID, string childNumber)
        {
            try
            {
                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                var model = new CommonVM();
                model.Declines = new List<DeclineDetails>();
                ViewBag.Module = "Translate";
                model = _util.setupTranslationView(id);

                model.child.Child_WorkflowID = actionReason;
                model.child.Pending_ChildID = pendingChildID;
                //Workflow id for translation is 3
                // model.Child_workflowsteps = _svc.GetChildWorkFlow(Helper.ParseInt(pendingChildID), 3, uInfo.ToString());
                model.Child_translationSteps = _svc.GetChildTranslations(id, int.Parse(childNumber), uInfo.ToString()).ToList();
                var ProfilePhoto = new DownLoadFile() { ChildID = id, UserID = uInfo.ToString() };

                var contentID = CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type").FirstOrDefault(a => a.Description.Contains("Profile"));
                ProfilePhoto.ContentTypeID = contentID.Content_Type_Code_ID;

                var child_ProfilePhoto = _svc.GetUpdateFile(ProfilePhoto);
                model.child.ProfilePhoto = new FileModel();
                if (child_ProfilePhoto != null && child_ProfilePhoto.Count() > 0)
                {
                    model.child.ProfilePhoto.FileBytes = child_ProfilePhoto[0].FileBytes;
                    model.child.ProfilePhoto.FileName = child_ProfilePhoto[0].FileName;
                }

                model.Child_workflowteps_ddl = model.Child_translationSteps.Select(x => new SelectListItem { Text = x.Step == "Update Information" ? "Major Life Event" : x.Step, Value = x.StepCodeID.ToString() });
                //add test mle data since it is not getting returned back with the child record that is in pending table
                //model.child.MajorLifeEvent = "Adding some test data to just view how it looks in the UI, will replace this with the actual child MLE once we get the data back from the DB";
                return View("Enrollment", model);
            }
            catch (Exception ex)
            {
                log.Debug("Failed to load the Child's QC View !", ex);
                return null;
            }
        }

        /// <summary>
        /// Creates the view for the child's Quality Control
        /// </summary>
        /// <param name="id"></param>
        /// <param name="workflowID"></param>
        /// <param name="pendingChildID"></param>
        /// <returns></returns>
        public ActionResult LoadQCView(string id, string workflowID, string pendingChildID)
        {
            try
            {
                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                var QCModel = new CommonVM();
                QCModel.Declines = new List<DeclineDetails>();
                if (workflowID == "1")
                {
                    ViewBag.Module = "QCEnrollment";
                    QCModel = _util.setupEnrollmentView(id);
                }
                else if (workflowID == "2")
                {
                    ViewBag.Module = "QCUpdate";
                    QCModel = _util.setupUpdateView(id);
                }

                QCModel.child.Child_WorkflowID = workflowID;
                QCModel.child.Pending_ChildID = pendingChildID;
                QCModel.Child_workflowsteps = _svc.GetChildWorkFlow(Helper.ParseInt(pendingChildID), Helper.ParseInt(workflowID), uInfo.ToString(), true);
                //When all the workflow steps are complete redirect to parent worklist view
                if (QCModel.Child_workflowsteps == null || QCModel.Child_workflowsteps.Count() == 0)
                {
                    return RedirectToAction("QualityControl");
                }

                if (workflowID == "1")
                {
                    var ProfilePhoto = new DownLoadFile() { ChildID = id, UserID = uInfo.ToString() };

                    //Get the child profile photo and store it in model using the  file id, if not present then use the content type id for profile Image
                    var profileWorkflowStep = QCModel.Child_workflowsteps.FirstOrDefault(a => a.Step_Name == "Profile Photo");


                    if (profileWorkflowStep != null)
                    {
                        ProfilePhoto.FileID = profileWorkflowStep.File_ID;
                    }
                    else
                    {
                        var contentID = CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type").FirstOrDefault(a => a.Description.Contains("Profile"));
                        ProfilePhoto.ContentTypeID = contentID.Content_Type_Code_ID;
                    }

                    var child_ProfilePhoto = _svc.GetUpdateFile(ProfilePhoto);
                    if(child_ProfilePhoto.Length > 0)
                    {
                        QCModel.child.ProfilePhoto = new FileModel();
                        QCModel.child.ProfilePhoto.FileBytes = child_ProfilePhoto[0].FileBytes;
                        QCModel.child.ProfilePhoto.FileName = child_ProfilePhoto[0].FileName;
                    }
                }

                QCModel.Child_workflowteps_ddl =
                    QCModel
                    .Child_workflowsteps
                    .Select
                    (
                        x =>
                        new SelectListItem
                        {
                            Text = x.Step_Name,
                            Value = x.Step_Code_ID.ToString()
                        }
                        );

                return View("Enrollment", QCModel);
            }
            catch (Exception ex)
            {
                log.Debug("Failed to load the Child's QC View !", ex);
                return View("QualityControl");
            }
        }

        /// <summary>
        /// This method loads the workflow file from the database
        /// </summary>
        /// <param name="PendingChildID"></param>
        /// <param name="fileID"></param>
        /// <returns></returns>
        [HttpPost]
        public string LoadQCFile(string ChildID, string fileID)
        {
            try
            {
                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                if (!string.IsNullOrEmpty(ChildID) && !string.IsNullOrEmpty(fileID))
                {
                    var file = new DownLoadFile();
                    file.ChildID = ChildID;
                    file.FileID = Helper.ParseInt(fileID);
                    file.UserID = uInfo.ToString();
                    var downloadfile = _svc.GetUpdateFile(file);
                    var data = new { Success = true, Name = downloadfile[0].FileName, File = downloadfile[0] };
                    return JsonConvert.SerializeObject(data);
                }
                else
                {
                    var data = new { Success = false, Message = Helper.MessageWithDateTimeAdded("Child ID or Content Type ID is missing !") };
                    return JsonConvert.SerializeObject(data);
                }
            }
            catch (Exception ex)
            {
                log.Debug("Failed to get the selected workflow document !", ex);
                return null;
            }
        }

        /// <summary>
        /// This method saves the QC workflow status to the database
        /// </summary>
        /// <param name="pendingchildID"></param>
        /// <param name="StepCodeID"></param>
        /// <param name="workflowID"></param>
        /// <param name="action"></param>
        /// <param name="RejectReason"></param>
        /// <returns></returns>
        [HttpPost]
        public string SaveWorkflowStep(string childWorkflowStepID, string pendingchildID, string StepCodeID, string workflowID, string action, string StatusCodeID, string IsTranslationRequired, string fileID)
        {
            try
            {
                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                if (!string.IsNullOrEmpty(pendingchildID) && !string.IsNullOrEmpty(StepCodeID))
                {
                    var wk_step = new WorkFlowStep();
                    wk_step.Step_Code_ID = Helper.ParseInt(StepCodeID);
                    wk_step.Pending_Child_ID = Helper.ParseInt(pendingchildID);
                    wk_step.Workflow_Code_ID = Helper.ParseInt(workflowID);
                    var statusCodes = CacheManager.GetFromCache<List<SelectListItem>>("Status");
                    wk_step.Status_Code_ID = Helper.ParseInt(statusCodes.FirstOrDefault(a => a.Text == StatusCodeID).Value);
                    wk_step.Child_Workflow_Step_ID = Helper.ParseInt(childWorkflowStepID);
                    wk_step.QCApprover = true;
                    if (IsTranslationRequired != null && IsTranslationRequired == "Yes")
                    {
                        //get the Workflowcodeid from the code table code.Workflow 3 for translation
                        SubWorkFlow translationWorkflow = new SubWorkFlow() { Child_Workflow_Step_ID = int.Parse(childWorkflowStepID), Pending_Child_ID = int.Parse(pendingchildID), Workflow_Code_ID = 3, File_ID = int.Parse(fileID) };
                        var Message2 = _svc.AddSubWorkFlow(translationWorkflow, uInfo.ToString());
                    }
                    var Message = _svc.SaveWorkFlow(wk_step, uInfo.ToString());
                    if (Message.Code == "1")
                    {
                        var data = new { Success = true, Message = Helper.MessageWithDateTimeAdded(Message.Message) };
                        return JsonConvert.SerializeObject(data);
                    }
                    else
                    {
                        var data = new { Success = false, Message = Helper.MessageWithDateTimeAdded(Message.Message) };
                        return JsonConvert.SerializeObject(data);
                    }
                }
                else
                {
                    var data = new { Success = false, Message = Helper.MessageWithDateTimeAdded("Pending Child ID or Workflow Step ID is missing !") };
                    return JsonConvert.SerializeObject(data);
                }
            }
            catch (Exception ex)
            {
                log.Debug("Failed to save the workflow !", ex);
                return null;
            }
        }


        public string SubmitTranslations(FormCollection formData)
        {
            try
            {
                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                var id = (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).FirstOrDefault(a => a.Content_Type == "Translation").Content_Type_Code_ID;

                //get all child translation steps
                var workflowsteps = _svc.GetChildTranslations(formData["ChildID"], 0, uInfo.ToString()).ToList();
                var stepInfo = formData["workflowStepsData"].ToString().Split('&');


                //content type for translated letter
                var key = "ContentType_TranslatedLetter";
                var file = SessionManager.Get<UploadFile>(key);
                string TranslatedMLE = formData["TranslatedMLE"];

                if (!string.IsNullOrEmpty(TranslatedMLE))
                {
                    var message = _svc.SaveTranslatedMLE(uInfo.ToString(), formData["ChildID"], 1, formData["TranslatedMLE"], int.Parse(formData["ParentMLEID"]), 0);
                    if (!message.Message.Contains("Success"))
                    {
                        var data = new { Success = false, Message = Helper.MessageWithDateTimeAdded(message.Message) };
                        return JsonConvert.SerializeObject(data);
                    }

                    //saveworkflow for MLE translation step
                    var statusCodes = CacheManager.GetFromCache<List<SelectListItem>>("Status");

                    var childWorkflowStepID = "";
                    var fileID = "";

                    var mleStepCodeID = workflowsteps.FirstOrDefault(a => a.Step == "Update Information").StepCodeID;

                    foreach (var item in stepInfo)
                    {
                        if (item.IndexOf('|' + mleStepCodeID.ToString(), StringComparison.Ordinal) == -1) continue;
                        var childWorkflowStepIDAndFileID = item.Split('|')[0];
                        childWorkflowStepID = childWorkflowStepIDAndFileID.Split('_')[0];
                        fileID = childWorkflowStepIDAndFileID.Split('_')[1];
                    }

                    var wk_step = new WorkFlowStep
                    {
                        Pending_Child_ID = Helper.ParseInt(formData["PendingChildID"]),
                        Workflow_Code_ID = Helper.ParseInt(formData["workflowID"]),
                        Step_Code_ID = mleStepCodeID,
                        Status_Code_ID = Helper.ParseInt(statusCodes.FirstOrDefault(a => a.Text == "Completed").Value),
                        QCApprover = true,
                        Complete = true,
                        Child_Workflow_Step_ID = int.Parse(childWorkflowStepID)
                    };

                    var SaveMessage = _svc.SaveWorkFlow(wk_step, uInfo.ToString());
                    if (file == null)
                    {
                        var data = new { Success = true, Message = Helper.MessageWithDateTimeAdded(message.Message) };
                        return JsonConvert.SerializeObject(data);
                    }

                }
                if (file != null)
                {
                    //get the letter step workflow
                    var letterStep = workflowsteps.FirstOrDefault(a => a.Step == "Letter");

                    //adding content type of the file before saving the file 
                    file.ContentTypeID = id;
                    file.FileStreamID = letterStep.StreamID;
                    var result = _svc.UploadAnUpdateFile(file);
                    //saveworkflow for File translation step
                    var statusCodes = CacheManager.GetFromCache<List<SelectListItem>>("Status");

                    var childWorkflowStepID = "";
                    var fileID = "";

                    foreach (var item in stepInfo)
                    {
                        if (item.IndexOf('|' + letterStep.StepCodeID.ToString(), StringComparison.Ordinal) == -1) continue;
                        var childWorkflowStepIDAndFileID = item.Split('|')[0];
                        childWorkflowStepID = childWorkflowStepIDAndFileID.Split('_')[0];
                        fileID = childWorkflowStepIDAndFileID.Split('_')[1];
                    }

                    var wk_step = new WorkFlowStep
                    {
                        Pending_Child_ID = Helper.ParseInt(formData["PendingChildID"]),
                        Workflow_Code_ID = Helper.ParseInt(formData["workflowID"]),
                        Step_Code_ID = letterStep.StepCodeID,
                        Status_Code_ID = Helper.ParseInt(statusCodes.FirstOrDefault(a => a.Text == "Completed").Value),
                        QCApprover = true,
                        Complete = true,
                        Child_Workflow_Step_ID = int.Parse(childWorkflowStepID)
                    };

                    var SaveMessage = _svc.SaveWorkFlow(wk_step, uInfo.ToString());
                    var data = new { Success = true, Message = Helper.MessageWithDateTimeAdded(result[0].Message) };
                    return JsonConvert.SerializeObject(data);
                }
                else
                {
                    var data = new { Success = false, Message = Helper.MessageWithDateTimeAdded("Failed to submit translation !") };
                    return JsonConvert.SerializeObject(data);
                }
            }
            catch (Exception ex)
            {
                log.Debug("Something went wrong in submitting the child update !", ex);
                return null;
            }
        }

        //Summary 
        // This method loads/brings the child record
        // GET: PanMain/LoadUpdateChild/AC0671A7-36B0-4E96-8929-F64695793926
        // Populate some of the Update Information drop down options in the Update module depending on pre-populated date of birth field.
        public ActionResult LoadChildRecord(string id)
        {
            EnrollUtil _util = new EnrollUtil();
            ViewBag.Module = "ChildRecord";
            var updateModel = _util.SetupChildRecordView(id);
            return View("Enrollment", updateModel);
        }

        public ActionResult SaveChildRecord(FormCollection formData)
        {
            try
            {
                var files_uploaded = new ReturnMessage[10];
                dynamic CreateResponse = UpdateChildRecord(formData);
                if (CreateResponse.Success == true)
                {
                    var data = new { Success = CreateResponse.Success, validationResult = CreateResponse.validationResult, FileValidation = CreateResponse.fileValidationResult, Message = CreateResponse.Message, duplicates = CreateResponse.duplicates };
                    return Json(data);
                }
                else
                {
                    var data = new { Success = CreateResponse.Success, validationResult = CreateResponse.validationResult, FileValidation = CreateResponse.fileValidationResult };
                    return Json(data);
                }
            }
            catch (Exception ex)
            {
                Log.Debug("Something went wrong in saving the child Record.");
                return null;
            }
        }

        //Summary
        //This method is creates a child record in the database when call upon.
        [NonAction]
        private object UpdateChildRecord(FormCollection formData)
        {
            try
            {
                var uInfo = SessionManager.Get<UserInfo>("UserInfo");
                var resultSet = validationResult(formData, "Enrollment");
                var childEnrollInfo = resultSet[0];
                var validationResults = resultSet[1];
                int count = 0;
                var files_uploaded = new ReturnMessage[10];

                Dictionary<string, string> filevalidationResults = new Dictionary<string, string>();
                var req_docs = (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).Where(m => m.Required == true).Select(c => new { ContentType = "ContentType_" + c.Content_Type_Code_ID.ToString(), ContentName = c.Description }).ToList();
                var all_docs = (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).Select(c => new { ContentType = "ContentType_" + c.Content_Type_Code_ID.ToString(), ContentName = c.Description }).ToList();
                foreach (var item in req_docs)
                {
                    // do not add missing file for already uploaded files and files currently saved
                    if ((SessionManager.Get<UploadFile>(item.ContentType)) == null && (SessionManager.Get<UploadFile>(item.ContentType + "_AlreadyUploaded")) == null)
                    {
                        filevalidationResults.Add(item.ContentName, Helper.MessageWithDateTimeAdded(item.ContentName + " is missing"));
                    }
                }

                if (count == 0)
                {
                    var child = _util.AdminChildObject(childEnrollInfo);

                    var message = _svc.SaveChildRecord(child, uInfo.ToString());
                    if (message.Count() >= 1 && message[0].Message == "Child_ID")
                    {
                        var duplicates = new List<ReturnMessage>();
                        string childID = message[0].Code;
                        if (child.ChildID == message[0].Code)
                        {
                            foreach (var item in all_docs)
                            {
                                var file = SessionManager.Get<UploadFile>(item.ContentType);
                                // upload File only when it is not already uploaded and was retrieved from DB when working on incomplete saved enrollments
                                if (file != null && file.FileBytes.Length > 0 && !item.ContentType.Contains("AlreadyUploaded"))
                                {
                                    file.ContentTypeID = int.Parse(item.ContentType.Split('_')[1]);
                                    files_uploaded = _svc.UploadChildRecordFile(file);
                                    if (files_uploaded[0] == null || files_uploaded[0].Code == "-2")
                                    { filevalidationResults.Add(item.ContentName, Helper.MessageWithDateTimeAdded(item.ContentName + " could not be uploaded !")); }
                                }
                                else { continue; }
                            }
                        }
                        for (var i = 1; i <= message.Count() - 1; i++)
                        {
                            duplicates.Add(message[i]);
                        }
                        var data = new { Success = true, validationResult = validationResults, fileValidationResult = filevalidationResults, ID = childID, Message = Helper.MessageWithDateTimeAdded(message[0].Description), duplicates = duplicates };
                        return data;
                    }
                    else
                    {
                        var data = new { Success = false, validationResult = validationResults, fileValidationResult = filevalidationResults, Message = Helper.MessageWithDateTimeAdded("Something went wrong in saving child Information !") };
                        return data;
                    }
                }
                else
                {
                    var data = new { Success = false, validationResult = validationResults, fileValidationResult = filevalidationResults };
                    return data;
                }
            }
            catch (Exception ex)
            {
                log.Debug("Something went wrong in creating the child enrollment !", ex);
                return null;
            }
        }
    }
}
